package modules

import (
	"fmt"

	"go-zero-admin/apps/frame/storage/cmd/rpc/internal/config"
	"go-zero-admin/pkg/tool"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/zeromicro/go-zero/core/logx"
)

func NewMinioServer(c config.Config) *minio.Client {
	// 创建minio链接
	minioConfig := c.StorageCustom.MinioConfig
	minioClient, err := minio.New(minioConfig.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(minioConfig.AccessKey, minioConfig.SecretKey, ""),
		Secure: minioConfig.Secure,
	})
	if err != nil {
		fmt.Printf("【STORAGE-RPC-MINIO-ERR】Failed to start storage's rpc server: minio init failed" + "\n")
		logx.Errorf(tool.GetErrMsgHeaderFormat("STORAGE-RPC-MINIO-ERR", "init minio server"), err)
		return nil
	}

	fmt.Printf("【STORAGE-RPC-MINIO】Starting storage's rpc server: minio init successed" + "\n")
	return minioClient
}
