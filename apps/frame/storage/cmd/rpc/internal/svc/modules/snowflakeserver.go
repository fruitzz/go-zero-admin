package modules

import (
	"fmt"

	"go-zero-admin/apps/frame/storage/cmd/rpc/internal/config"
	"go-zero-admin/pkg/tool"

	"github.com/bwmarrin/snowflake"
	"github.com/zeromicro/go-zero/core/logx"
)

func NewSnowflakeServer(c config.Config) *snowflake.Node {
	node, err := snowflake.NewNode(c.StorageCustom.SnowflakeNode)
	if err != nil {
		fmt.Printf("【STORAGE-RPC-SNOWFLAKE-ERR】Failed to start storage's rpc server: snowflake node init failed" + "\n")
		logx.Errorf(tool.GetErrMsgHeaderFormat("STORAGE-RPC-SNOWFLAKE-ERR", "init snowflake node"), err)
		return nil
	}

	fmt.Printf("【STORAGE-RPC-SNOWFLAKE】Starting storage's rpc server: create snowflake node init successed" + "\n")
	return node
}
