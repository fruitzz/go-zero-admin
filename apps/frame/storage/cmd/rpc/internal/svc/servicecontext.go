package svc

import (
	"go-zero-admin/apps/frame/storage/cmd/rpc/internal/config"
	"go-zero-admin/apps/frame/storage/cmd/rpc/internal/svc/modules"

	"github.com/bwmarrin/snowflake"
	"github.com/minio/minio-go/v7"
)

type ServiceContext struct {
	Config config.Config
	// modules
	MinioClient   *minio.Client
	SnowflakeNode *snowflake.Node
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config: c,
		// modules
		MinioClient:   modules.NewMinioServer(c),
		SnowflakeNode: modules.NewSnowflakeServer(c),
	}
}
