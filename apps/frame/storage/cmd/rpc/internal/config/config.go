package config

import "github.com/zeromicro/go-zero/zrpc"

type Config struct {
	zrpc.RpcServerConf
	// 自定义参数
	StorageCustom struct {
		MinioConfig struct {
			Endpoint   string
			AccessKey  string
			SecretKey  string
			Secure     bool
			BucketName string
		}
		SnowflakeNode int64
	}
}
