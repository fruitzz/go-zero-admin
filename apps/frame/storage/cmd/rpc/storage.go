package main

import (
	"flag"
	"fmt"

	"go-zero-admin/apps/frame/storage/cmd/rpc/internal/config"
	minioServer "go-zero-admin/apps/frame/storage/cmd/rpc/internal/server/minio"
	"go-zero-admin/apps/frame/storage/cmd/rpc/internal/svc"
	"go-zero-admin/apps/frame/storage/cmd/rpc/stopb"
	"go-zero-admin/pkg/interceptor/rpcserver"

	"github.com/zeromicro/go-zero/core/conf"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/service"
	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var configFile = flag.String("f", "etc/storage.yaml", "the config file")

func main() {
	flag.Parse()

	// close log
	logx.DisableStat()

	var c config.Config
	conf.MustLoad(*configFile, &c)
	ctx := svc.NewServiceContext(c)

	s := zrpc.MustNewServer(c.RpcServerConf, func(grpcServer *grpc.Server) {
		stopb.RegisterMinioServer(grpcServer, minioServer.NewMinioServer(ctx))

		if c.Mode == service.DevMode || c.Mode == service.TestMode {
			reflection.Register(grpcServer)
		}
	})

	// rpc log interceptor
	s.AddUnaryInterceptors(rpcserver.LoggerInterceptor)

	defer s.Stop()

	fmt.Printf("【STORAGE-RPC】Starting storage's rpc server at %s...\n", c.ListenOn)
	s.Start()
}
