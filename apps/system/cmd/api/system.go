package main

import (
	"flag"
	"fmt"

	"go-zero-admin/apps/system/cmd/api/internal/config"
	"go-zero-admin/apps/system/cmd/api/internal/handler"
	"go-zero-admin/apps/system/cmd/api/internal/svc"

	"github.com/zeromicro/go-zero/core/conf"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/rest"
)

var configFile = flag.String("f", "etc/system.yaml", "the config file")

func main() {
	flag.Parse()

	// log close
	logx.DisableStat()

	var c config.Config
	conf.MustLoad(*configFile, &c)

	ctx := svc.NewServiceContext(c)
	server := rest.MustNewServer(c.RestConf)
	defer server.Stop()

	handler.RegisterHandlers(server, ctx)

	fmt.Printf("【SYSTEM-API】Starting system's api server at %s:%d...\n", c.Host, c.Port)
	server.Start()
}
