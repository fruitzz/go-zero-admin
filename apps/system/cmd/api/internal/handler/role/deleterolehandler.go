package role

import (
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/logic/role"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func DeleteRoleHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.DeleteRoleReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := role.NewDeleteRoleLogic(r.Context(), svcCtx, r)
		resp, err := l.DeleteRole(&req)
		result.HttpResult(r, w, resp, err)
	}
}
