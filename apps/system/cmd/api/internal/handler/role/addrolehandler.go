package role

import (
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/logic/role"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func AddRoleHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.AddRoleReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := role.NewAddRoleLogic(r.Context(), svcCtx, r)
		resp, err := l.AddRole(&req)
		result.HttpResult(r, w, resp, err)
	}
}
