package menu

import (
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/logic/menu"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func DeleteMenuHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.DeleteMenuReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := menu.NewDeleteMenuLogic(r.Context(), svcCtx, r)
		resp, err := l.DeleteMenu(&req)
		result.HttpResult(r, w, resp, err)
	}
}
