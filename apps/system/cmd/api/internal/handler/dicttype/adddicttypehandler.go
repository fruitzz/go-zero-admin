package dicttype

import (
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/logic/dicttype"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func AddDictTypeHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.AddDictTypeReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := dicttype.NewAddDictTypeLogic(r.Context(), svcCtx, r)
		resp, err := l.AddDictType(&req)
		result.HttpResult(r, w, resp, err)
	}
}
