package dicttype

import (
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/logic/dicttype"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func UpdateDictTypeHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.UpdateDictTypeReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := dicttype.NewUpdateDictTypeLogic(r.Context(), svcCtx, r)
		resp, err := l.UpdateDictType(&req)
		result.HttpResult(r, w, resp, err)
	}
}
