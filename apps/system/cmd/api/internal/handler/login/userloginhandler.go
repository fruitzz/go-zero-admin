package login

import (
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/logic/login"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func UserLoginHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.UserLoginReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := login.NewUserLoginLogic(r.Context(), svcCtx, r)
		resp, err := l.UserLogin(&req)
		result.HttpResult(r, w, resp, err)
	}
}
