package posts

import (
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/logic/posts"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func AddPostHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.AddPostReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := posts.NewAddPostLogic(r.Context(), svcCtx, r)
		resp, err := l.AddPost(&req)
		result.HttpResult(r, w, resp, err)
	}
}
