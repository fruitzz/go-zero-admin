package config

import (
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/logic/config"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func DeleteConfigHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.DeleteConfigReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := config.NewDeleteConfigLogic(r.Context(), svcCtx, r)
		resp, err := l.DeleteConfig(&req)
		result.HttpResult(r, w, resp, err)
	}
}
