package config

import (
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/logic/config"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func AddConfigHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.AddConfigReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := config.NewAddConfigLogic(r.Context(), svcCtx, r)
		resp, err := l.AddConfig(&req)
		result.HttpResult(r, w, resp, err)
	}
}
