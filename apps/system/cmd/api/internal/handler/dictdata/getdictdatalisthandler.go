package dictdata

import (
	"net/http"

	"go-zero-admin/pkg/result"

	"go-zero-admin/apps/system/cmd/api/internal/logic/dictdata"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func GetDictDataListHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.GetDictDataListReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := dictdata.NewGetDictDataListLogic(r.Context(), svcCtx, r)
		resp, err := l.GetDictDataList(&req)
		result.HttpResult(r, w, resp, err)
	}
}
