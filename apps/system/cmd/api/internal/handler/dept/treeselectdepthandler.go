package dept

import (
	"net/http"

	"go-zero-admin/pkg/result"

	"go-zero-admin/apps/system/cmd/api/internal/logic/dept"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func TreeSelectDeptHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.TreeSelectDeptReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := dept.NewTreeSelectDeptLogic(r.Context(), svcCtx, r)
		resp, err := l.TreeSelectDept(&req)
		result.HttpResult(r, w, resp, err)
	}
}
