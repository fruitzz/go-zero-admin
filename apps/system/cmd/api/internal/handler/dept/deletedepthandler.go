package dept

import (
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/logic/dept"
	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
)

func DeleteDeptHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.DeleteDeptReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := dept.NewDeleteDeptLogic(r.Context(), svcCtx, r)
		resp, err := l.DeleteDept(&req)
		result.HttpResult(r, w, resp, err)
	}
}
