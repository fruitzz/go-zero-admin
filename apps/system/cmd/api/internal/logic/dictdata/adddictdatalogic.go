package dictdata

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddDictDataLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddDictDataLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddDictDataLogic {
	return &AddDictDataLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddDictDataLogic) AddDictData(req *types.AddDictDataReq) (resp *types.AddDictDataResp, err error) {
	_, err = l.svcCtx.DictDataRpc.AddDictData(l.ctx, &syspb.AddDictDataReq{
		Params: &syspb.SysDictData{
			DictSort:   req.DictSort,
			DictLabel:  req.DictLabel,
			DictValue:  req.DictValue,
			DictType:   req.DictType,
			IsDefault:  req.IsDefault,
			Status:     req.Status,
			Remark:     req.Remark,
			CreateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}

	return
}
