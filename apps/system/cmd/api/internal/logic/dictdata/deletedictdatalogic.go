package dictdata

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteDictDataLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewDeleteDictDataLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *DeleteDictDataLogic {
	return &DeleteDictDataLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *DeleteDictDataLogic) DeleteDictData(req *types.DeleteDictDataReq) (resp *types.DeleteDictDataResp, err error) {
	_, err = l.svcCtx.DictDataRpc.DelDictData(l.ctx, &syspb.DelDictDataReq{
		IsDel: true,
		Ids:   req.Ids,
	})
	if err != nil {
		return nil, err
	}

	return
}
