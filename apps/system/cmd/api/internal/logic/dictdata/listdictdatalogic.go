package dictdata

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type ListDictDataLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewListDictDataLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *ListDictDataLogic {
	return &ListDictDataLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *ListDictDataLogic) ListDictData(req *types.ListDictDataReq) (resp *types.ListDictDataResp, err error) {
	listResp, err := l.svcCtx.DictDataRpc.GetDictDataList(l.ctx, &syspb.GetDictDataListReq{
		Page: &syspb.PageReq{
			DateRange: req.DateRange,
			PageNum:   req.PageNum,
			PageSize:  req.PageSize,
			IsPage:    true,
		},
		DictLabel:    req.DictLabel,
		Status:       req.Status,
		DictTypeList: []string{req.DictType},
	})
	if err != nil {
		return nil, err
	}

	// 处理数据
	dictDataList := make([]*types.SysDictData, 0)
	for _, v := range listResp.GetList() {
		var dictData types.SysDictData
		_ = copier.Copy(&dictData, v)
		dictDataList = append(dictDataList, &dictData)
	}

	return &types.ListDictDataResp{
		PageResp: &types.PageResp{
			CurrentPage: listResp.GetPage().GetCurrent(),
			Total:       listResp.GetPage().GetTotal(),
		},
		List: dictDataList,
	}, nil
}
