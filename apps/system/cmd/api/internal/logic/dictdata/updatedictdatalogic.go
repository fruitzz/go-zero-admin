package dictdata

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateDictDataLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateDictDataLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateDictDataLogic {
	return &UpdateDictDataLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateDictDataLogic) UpdateDictData(req *types.UpdateDictDataReq) (resp *types.UpdateDictDataResp, err error) {
	_, err = l.svcCtx.DictDataRpc.UpDictData(l.ctx, &syspb.UpDictDataReq{
		Params: &syspb.SysDictData{
			DictCode:   req.DictCode,
			DictSort:   req.DictSort,
			DictLabel:  req.DictLabel,
			DictValue:  req.DictValue,
			DictType:   req.DictType,
			IsDefault:  req.IsDefault,
			Status:     req.Status,
			Remark:     req.Remark,
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}

	return
}
