package dicttype

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddDictTypeLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddDictTypeLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddDictTypeLogic {
	return &AddDictTypeLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddDictTypeLogic) AddDictType(req *types.AddDictTypeReq) (resp *types.AddDictTypeResp, err error) {
	_, err = l.svcCtx.DictTypeRpc.AddDictType(l.ctx, &syspb.AddDictTypeReq{
		Params: &syspb.SysDictType{
			DictName:   req.DictName,
			DictType:   req.DictType,
			Status:     req.Status,
			Remark:     req.Remark,
			CreateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}

	return
}
