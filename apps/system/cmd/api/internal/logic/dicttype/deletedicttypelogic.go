package dicttype

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteDictTypeLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewDeleteDictTypeLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *DeleteDictTypeLogic {
	return &DeleteDictTypeLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *DeleteDictTypeLogic) DeleteDictType(req *types.DeleteDictTypeReq) (resp *types.DeleteDictTypeResp, err error) {
	_, err = l.svcCtx.DictTypeRpc.DelDictType(l.ctx, &syspb.DelDictTypeReq{
		IsDel: true,
		Ids:   req.DictIds,
	})
	if err != nil {
		return nil, err
	}

	return
}
