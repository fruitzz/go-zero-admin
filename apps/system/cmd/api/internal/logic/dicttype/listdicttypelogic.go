package dicttype

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type ListDictTypeLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewListDictTypeLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *ListDictTypeLogic {
	return &ListDictTypeLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *ListDictTypeLogic) ListDictType(req *types.ListDictTypeReq) (resp *types.ListDictTypeResp, err error) {
	listResp, err := l.svcCtx.DictTypeRpc.GetDictTypeList(l.ctx, &syspb.GetDictTypeListReq{
		Page: &syspb.PageReq{
			DateRange: req.DateRange,
			PageNum:   req.PageNum,
			PageSize:  req.PageSize,
			IsPage:    true,
		},
		DictName: req.DictName,
		DictType: req.DictType,
		Status:   req.Status,
	})
	if err != nil {
		return nil, err
	}

	// 处理数据
	dictTypeList := make([]*types.SysDictType, 0)
	for _, v := range listResp.GetList() {
		var dictType types.SysDictType
		_ = copier.Copy(&dictType, v)
		dictTypeList = append(dictTypeList, &dictType)
	}

	return &types.ListDictTypeResp{
		PageResp: &types.PageResp{
			CurrentPage: listResp.GetPage().GetCurrent(),
			Total:       listResp.GetPage().GetTotal(),
		},
		DictTypeList: dictTypeList,
	}, nil
}
