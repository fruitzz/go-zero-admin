package dicttype

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateDictTypeLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateDictTypeLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateDictTypeLogic {
	return &UpdateDictTypeLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateDictTypeLogic) UpdateDictType(req *types.UpdateDictTypeReq) (resp *types.UpdateDictTypeResp, err error) {
	_, err = l.svcCtx.DictTypeRpc.UpDictType(l.ctx, &syspb.UpDictTypeReq{
		Params: &syspb.SysDictType{
			DictId:     req.DictId,
			DictName:   req.DictName,
			DictType:   req.DictType,
			Status:     req.Status,
			Remark:     req.Remark,
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}

	return
}
