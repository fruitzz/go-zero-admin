package posts

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type ListPostLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewListPostLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *ListPostLogic {
	return &ListPostLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *ListPostLogic) ListPost(req *types.ListPostReq) (resp *types.ListPostResp, err error) {
	listResp, err := l.svcCtx.PostRpc.GetPostList(l.ctx, &syspb.GetPostListReq{
		Page: &syspb.PageReq{
			DateRange: req.DateRange,
			PageNum:   req.PageNum,
			PageSize:  req.PageSize,
			IsPage:    true,
		},
		PostCode: req.PostCode,
		PostName: req.PostName,
		Status:   req.Status,
	})
	if err != nil {
		return nil, err
	}

	// 处理数据
	postList := make([]*types.SysPost, 0)
	for _, v := range listResp.GetList() {
		var post types.SysPost
		_ = copier.Copy(&post, v)
		postList = append(postList, &post)
	}

	return &types.ListPostResp{
		PageResp: &types.PageResp{
			CurrentPage: listResp.GetPage().GetCurrent(),
			Total:       listResp.GetPage().GetTotal(),
		},
		PostList: postList,
	}, nil
}
