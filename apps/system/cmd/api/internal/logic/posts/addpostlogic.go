package posts

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddPostLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddPostLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddPostLogic {
	return &AddPostLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddPostLogic) AddPost(req *types.AddPostReq) (resp *types.AddPostResp, err error) {
	_, err = l.svcCtx.PostRpc.AddPost(l.ctx, &syspb.AddPostReq{
		Params: &syspb.SysPost{
			PostCode:   req.PostCode,
			PostName:   req.PostName,
			PostSort:   req.PostSort,
			Status:     req.Status,
			Remark:     req.Remark,
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			CreateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}

	return
}
