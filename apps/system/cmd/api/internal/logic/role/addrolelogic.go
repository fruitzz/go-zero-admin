package role

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddRoleLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddRoleLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddRoleLogic {
	return &AddRoleLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddRoleLogic) AddRole(req *types.AddRoleReq) (resp *types.AddRoleResp, err error) {
	_, err = l.svcCtx.RoleRpc.AddRole(l.ctx, &syspb.AddRoleReq{
		Params: &syspb.SysRole{
			Name:       req.Name,
			ListOrder:  req.ListOrder,
			Status:     req.Status,
			Remark:     req.Remark,
			DataScope:  req.DataScope,
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			CreateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
		MenuIds: req.MenuIds,
	})
	if err != nil {
		return nil, err
	}

	return
}
