package role

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetMenusLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewGetMenusLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *GetMenusLogic {
	return &GetMenusLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *GetMenusLogic) GetMenus(req *types.GetMenusReq) (resp *types.GetMenusResp, err error) {
	// 获取新增、编辑中的菜单树状列表
	listRpc, err := l.svcCtx.MenuRpc.GetMenuList(l.ctx, &syspb.GetMenuListReq{
		IsDataScope:     true,
		DataScopeUserId: ctxdata.GetUidFromCtx(l.ctx),
	})
	if err != nil {
		return nil, err
	}

	// 处理数据
	list := make([]*types.SysMenu, 0)
	for _, menuRpc := range listRpc.GetList() {
		var menu types.SysMenu
		_ = copier.Copy(&menu, menuRpc)
		list = append(list, &menu)
	}

	return &types.GetMenusResp{Menu: list}, nil
}
