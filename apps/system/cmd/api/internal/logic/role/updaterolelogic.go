package role

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateRoleLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateRoleLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateRoleLogic {
	return &UpdateRoleLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateRoleLogic) UpdateRole(req *types.UpdateRoleReq) (resp *types.UpdateRoleResp, err error) {
	_, err = l.svcCtx.RoleRpc.UpRole(l.ctx, &syspb.UpRoleReq{
		Params: &syspb.SysRole{
			Id:         req.Id,
			Name:       req.Name,
			ListOrder:  req.ListOrder,
			Status:     req.Status,
			Remark:     req.Remark,
			DataScope:  req.DataScope,
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
		MenuIds: req.MenuIds,
	})
	if err != nil {
		return nil, err
	}

	return
}
