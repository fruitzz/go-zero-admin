package config

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteConfigLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewDeleteConfigLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *DeleteConfigLogic {
	return &DeleteConfigLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *DeleteConfigLogic) DeleteConfig(req *types.DeleteConfigReq) (resp *types.DeleteConfigResp, err error) {
	_, err = l.svcCtx.ConfigRpc.DelConfig(l.ctx, &syspb.DelConfigReq{
		IsDel: true,
		Ids:   req.Ids,
	})
	if err != nil {
		return nil, err
	}

	return
}
