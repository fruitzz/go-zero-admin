package config

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddConfigLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddConfigLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddConfigLogic {
	return &AddConfigLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddConfigLogic) AddConfig(req *types.AddConfigReq) (resp *types.AddConfigResp, err error) {
	_, err = l.svcCtx.ConfigRpc.AddConfig(l.ctx, &syspb.AddConfigReq{
		Params: &syspb.SysConfig{
			ConfigName:  req.ConfigName,
			ConfigKey:   req.ConfigKey,
			ConfigValue: req.ConfigValue,
			ConfigType:  req.ConfigType,
			Remark:      req.Remark,
			CreateUser:  convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			UpdateUser:  convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}

	return
}
