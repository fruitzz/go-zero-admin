package user

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/fx"
	"github.com/zeromicro/go-zero/core/logx"
)

type ListUserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewListUserLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *ListUserLogic {
	return &ListUserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *ListUserLogic) ListUser(req *types.ListUserReq) (resp *types.ListUserResp, err error) {
	// 查询出用户数据分页
	userListResp, err := l.svcCtx.UserRpc.GetUserList(l.ctx, &syspb.GetUserListReq{
		Page: &syspb.PageReq{
			DateRange: req.DateRange,
			PageNum:   req.PageNum,
			PageSize:  req.PageSize,
			IsPage:    true,
		},
		Mobile:          req.Mobile,
		KeyWords:        req.KeyWords,
		SonIdsByDeptId:  req.DeptId,
		Status:          req.UserStatus,
		IsDataScope:     true,
		DataScopeUserId: ctxdata.GetUidFromCtx(l.ctx),
	})
	if err != nil {
		return nil, err
	}
	if len(userListResp.GetList()) == 0 {
		return &types.ListUserResp{
			PageResp: &types.PageResp{
				CurrentPage: userListResp.GetPage().GetCurrent(),
				Total:       userListResp.GetPage().GetTotal(),
			},
			UserList: []*types.SysUserCombo{},
		}, nil
	}

	// 获取用户组合数据
	userComboResp, err := l.svcCtx.UserRpc.GetUserCombo(l.ctx, &syspb.GetUserComboReq{UserList: userListResp.GetList()})
	if err != nil {
		return nil, err
	}

	// 组合用户数据
	userDataList := make([]*types.SysUserCombo, 0)
	for _, v := range userComboResp.GetUserComboList() {
		userData := new(types.SysUserCombo)
		if v.GetUser() != nil {
			_ = copier.Copy(&userData, v.GetUser())
		}

		// 部门
		dept := new(types.SysDept)
		if v.GetDept() != nil {
			_ = copier.Copy(dept, v.GetDept())
		}
		userData.Dept = dept

		// 角色
		userRoleDataList := make([]*types.SysUserRoleData, 0)
		if len(v.GetRoleList()) != 0 {
			fx.From(func(source chan<- interface{}) {
				for _, v := range v.GetRoleList() {
					source <- v
				}
			}).Map(func(item interface{}) interface{} {
				role := item.(*syspb.SysRole)

				return &types.SysUserRoleData{
					RoleId:   role.GetId(),
					RoleName: role.GetName(),
				}
			}).ForEach(func(item interface{}) {
				userRoleDataList = append(userRoleDataList, item.(*types.SysUserRoleData))
			})
		}
		userData.Role = userRoleDataList

		// 查询出岗位
		userPostList := make([]*types.SysUserPostData, 0)
		if len(v.GetPostList()) != 0 {
			fx.From(func(source chan<- interface{}) {
				for _, v := range v.GetPostList() {
					source <- v
				}
			}).Map(func(item interface{}) interface{} {
				post := item.(*syspb.SysPost)

				return &types.SysUserPostData{
					PostId:   post.GetPostId(),
					PostName: post.GetPostName(),
				}
			}).ForEach(func(item interface{}) {
				userPostList = append(userPostList, item.(*types.SysUserPostData))
			})
		}
		userData.Post = userPostList

		// 组合
		userDataList = append(userDataList, userData)
	}

	return &types.ListUserResp{
		PageResp: &types.PageResp{
			CurrentPage: userListResp.GetPage().GetCurrent(),
			Total:       userListResp.GetPage().GetTotal(),
		},
		UserList: userDataList,
	}, nil
}
