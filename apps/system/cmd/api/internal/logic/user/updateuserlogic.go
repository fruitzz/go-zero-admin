package user

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateUserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateUserLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateUserLogic {
	return &UpdateUserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateUserLogic) UpdateUser(req *types.UpdateUserReq) (resp *types.UpdateUserResp, err error) {
	_, err = l.svcCtx.UserRpc.UpUserData(l.ctx, &syspb.UpUserDataReq{
		Params: &syspb.SysUser{
			Id:           req.Id,
			UserName:     req.UserName,
			Mobile:       req.Mobile,
			UserNickname: req.UserNickname,
			Birthday:     req.Birthday,
			UserStatus:   req.UserStatus,
			UserEmail:    req.UserEmail,
			Sex:          req.Sex,
			Avatar:       req.Avatar,
			DeptId:       req.DeptId,
			Remark:       req.Remark,
			UserType:     req.UserType,
			Address:      req.Address,
			Describe:     req.Describe,
			UpdateUser:   convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
		PostIds: req.PostIds,
		RoleIds: req.RoleIds,
	})
	if err != nil {
		return nil, err
	}

	return
}
