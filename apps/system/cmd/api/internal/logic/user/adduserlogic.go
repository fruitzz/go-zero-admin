package user

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddUserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddUserLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddUserLogic {
	return &AddUserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddUserLogic) AddUser(req *types.AddUserReq) (resp *types.AddUserResp, err error) {
	_, err = l.svcCtx.UserRpc.AddUserData(l.ctx, &syspb.AddUserDataReq{
		Params: &syspb.SysUser{
			UserName:     req.UserName,
			Mobile:       req.Mobile,
			UserNickname: req.UserNickname,
			Birthday:     req.Birthday,
			UserPassword: req.UserPassword,
			UserStatus:   req.UserStatus,
			UserEmail:    req.UserEmail,
			Sex:          req.Sex,
			Avatar:       req.Avatar,
			DeptId:       req.DeptId,
			Remark:       req.Remark,
			UserType:     req.UserType,
			Address:      req.Address,
			Describe:     req.Describe,
			UpdateUser:   convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			CreateUser:   convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
		PostIds: req.PostIds,
		RoleIds: req.RoleIds,
	})
	if err != nil {
		return nil, err
	}

	return
}
