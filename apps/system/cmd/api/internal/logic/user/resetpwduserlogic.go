package user

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type ResetPwdUserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewResetPwdUserLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *ResetPwdUserLogic {
	return &ResetPwdUserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *ResetPwdUserLogic) ResetPwdUser(req *types.ResetPwdUserReq) (resp *types.ResetPwdUserResp, err error) {
	_, err = l.svcCtx.UserRpc.UpUserPwd(l.ctx, &syspb.UpUserPwdReq{
		Id:         req.Id,
		Password:   req.UserPassword,
		UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
	})
	if err != nil {
		return nil, err
	}

	return
}
