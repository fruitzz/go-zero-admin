package user

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type SetStatusUserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewSetStatusUserLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *SetStatusUserLogic {
	return &SetStatusUserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *SetStatusUserLogic) SetStatusUser(req *types.SetStatusUserReq) (resp *types.SetStatusUserResp, err error) {
	_, err = l.svcCtx.UserRpc.UpUserStatus(l.ctx, &syspb.UpUserStatusReq{
		Id:         req.Id,
		UserStatus: req.UserStatus,
		UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
	})
	if err != nil {
		return nil, err
	}

	return
}
