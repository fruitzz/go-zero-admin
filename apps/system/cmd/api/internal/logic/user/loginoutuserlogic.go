package user

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type LoginOutUserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewLoginOutUserLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *LoginOutUserLogic {
	return &LoginOutUserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *LoginOutUserLogic) LoginOutUser(req *types.LoginOutUserReq) (resp *types.LoginOutUserResp, err error) {
	// todo: add your logic here and delete this line

	return
}
