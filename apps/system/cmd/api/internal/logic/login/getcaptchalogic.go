package login

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetCaptchaLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewGetCaptchaLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *GetCaptchaLogic {
	return &GetCaptchaLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *GetCaptchaLogic) GetCaptcha(req *types.GetCaptchaReq) (resp *types.GetCaptchaResp, err error) {
	var idKeyC, base64stringC string

	getResp, err := l.svcCtx.CaptchaRpc.CaptchaForGet(l.ctx, &syspb.CaptchaForGetReq{})
	if err != nil {
		return nil, err
	}
	idKeyC = getResp.GetIdKeyC()
	base64stringC = getResp.GetBase64StringC()

	return &types.GetCaptchaResp{
		Key: idKeyC,
		Img: base64stringC,
	}, nil
}
