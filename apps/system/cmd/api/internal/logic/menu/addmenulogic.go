package menu

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddMenuLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddMenuLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddMenuLogic {
	return &AddMenuLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddMenuLogic) AddMenu(req *types.AddMenuReq) (resp *types.AddMenuResp, err error) {
	_, err = l.svcCtx.MenuRpc.AddMenu(l.ctx, &syspb.AddMenuReq{
		Params: &syspb.SysMenu{
			Pid:         req.Pid,
			Name:        req.Name,
			Title:       req.Title,
			Icon:        req.Icon,
			Condition:   req.Condition,
			Remark:      req.Remark,
			MenuType:    req.MenuType,
			MenuSort:    req.MenuSort,
			IsHide:      req.IsHide,
			Path:        req.Path,
			Component:   req.Component,
			IsLink:      req.IsLink,
			IsIframe:    req.IsIframe,
			IsKeepAlive: req.IsKeepAlive,
			Redirect:    req.Redirect,
			IsAffix:     req.IsAffix,
			LinkUrl:     req.LinkUrl,
			UpdateUser:  convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			CreateUser:  convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
		RoleIds: req.Roles,
	})
	if err != nil {
		return nil, err
	}

	return
}
