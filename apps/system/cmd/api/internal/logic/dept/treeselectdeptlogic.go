package dept

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/pkg/constant"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type TreeSelectDeptLogic struct {
	logx.Logger
	ctx           context.Context
	svcCtx        *svc.ServiceContext
	r             *http.Request
	listDeptLogic *ListDeptLogic
}

func NewTreeSelectDeptLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *TreeSelectDeptLogic {
	return &TreeSelectDeptLogic{
		Logger:        logx.WithContext(ctx),
		ctx:           ctx,
		svcCtx:        svcCtx,
		r:             r,
		listDeptLogic: NewListDeptLogic(ctx, svcCtx, r),
	}
}

func (l *TreeSelectDeptLogic) TreeSelectDept(req *types.TreeSelectDeptReq) (resp *types.TreeSelectDeptResp, err error) {
	listDeptResp, err := l.listDeptLogic.ListDept(&types.ListDeptReq{
		Status: convertor.ToString(constant.DeptStatusOn),
	})
	if err != nil {
		return nil, err
	}

	return &types.TreeSelectDeptResp{DeptTree: listDeptResp.DeptTree}, nil
}
