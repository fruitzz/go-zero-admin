package dept

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddDeptLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddDeptLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddDeptLogic {
	return &AddDeptLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddDeptLogic) AddDept(req *types.AddDeptReq) (resp *types.AddDeptResp, err error) {
	_, err = l.svcCtx.DeptRpc.AddDept(l.ctx, &syspb.AddDeptReq{
		Params: &syspb.SysDept{
			ParentId:   req.ParentId,
			Ancestors:  req.Ancestors,
			DeptName:   req.DeptName,
			OrderNum:   req.OrderNum,
			Leader:     req.Leader,
			Phone:      req.Phone,
			Email:      req.Email,
			Status:     req.Status,
			CreateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}
	return
}
