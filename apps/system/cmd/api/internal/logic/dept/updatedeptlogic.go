package dept

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateDeptLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateDeptLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateDeptLogic {
	return &UpdateDeptLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateDeptLogic) UpdateDept(req *types.UpdateDeptReq) (resp *types.UpdateDeptResp, err error) {
	_, err = l.svcCtx.DeptRpc.UpDept(l.ctx, &syspb.UpDeptReq{
		Params: &syspb.SysDept{
			DeptId:     req.DeptId,
			ParentId:   req.ParentId,
			Ancestors:  req.Ancestors,
			DeptName:   req.DeptName,
			OrderNum:   req.OrderNum,
			Leader:     req.Leader,
			Phone:      req.Phone,
			Email:      req.Email,
			Status:     req.Status,
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}
	return
}
