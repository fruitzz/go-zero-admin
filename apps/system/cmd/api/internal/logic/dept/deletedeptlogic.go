package dept

import (
	"context"
	"net/http"

	"go-zero-admin/apps/system/cmd/api/internal/svc"
	"go-zero-admin/apps/system/cmd/api/internal/types"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteDeptLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewDeleteDeptLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *DeleteDeptLogic {
	return &DeleteDeptLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *DeleteDeptLogic) DeleteDept(req *types.DeleteDeptReq) (resp *types.DeleteDeptResp, err error) {
	_, err = l.svcCtx.DeptRpc.DelDept(l.ctx, &syspb.DelDeptReq{
		IsDel: true,
		Id:    req.Id,
	})
	if err != nil {
		return nil, err
	}
	return
}
