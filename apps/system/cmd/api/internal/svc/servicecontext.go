package svc

import (
	"go-zero-admin/apps/system/cmd/api/internal/config"
	"go-zero-admin/apps/system/cmd/api/internal/middleware"
	captchaClient "go-zero-admin/apps/system/cmd/rpc/client/captcha"
	configClient "go-zero-admin/apps/system/cmd/rpc/client/config"
	deptClient "go-zero-admin/apps/system/cmd/rpc/client/dept"
	dictDataClient "go-zero-admin/apps/system/cmd/rpc/client/dictdata"
	dictTypeClient "go-zero-admin/apps/system/cmd/rpc/client/dicttype"
	loginClient "go-zero-admin/apps/system/cmd/rpc/client/login"
	loginLogClient "go-zero-admin/apps/system/cmd/rpc/client/loginlog"
	menuClient "go-zero-admin/apps/system/cmd/rpc/client/menu"
	operLogClient "go-zero-admin/apps/system/cmd/rpc/client/operlog"
	postClient "go-zero-admin/apps/system/cmd/rpc/client/post"
	roleClient "go-zero-admin/apps/system/cmd/rpc/client/role"
	roleDeptClient "go-zero-admin/apps/system/cmd/rpc/client/roledept"
	userClient "go-zero-admin/apps/system/cmd/rpc/client/user"
	userOnlineClient "go-zero-admin/apps/system/cmd/rpc/client/useronline"
	userPostClient "go-zero-admin/apps/system/cmd/rpc/client/userpost"
	userScopeClient "go-zero-admin/apps/system/cmd/rpc/client/userscope"

	"github.com/zeromicro/go-zero/rest"
	"github.com/zeromicro/go-zero/zrpc"
)

type ServiceContext struct {
	Config config.Config
	// middle
	CheckUrlAuth rest.Middleware
	// rpc
	CaptchaRpc    captchaClient.Captcha
	ConfigRpc     configClient.Config
	DeptRpc       deptClient.Dept
	DictDataRpc   dictDataClient.DictData
	DictTypeRpc   dictTypeClient.DictType
	LoginRpc      loginClient.Login
	LoginLogRpc   loginLogClient.LoginLog
	MenuRpc       menuClient.Menu
	OperLogRpc    operLogClient.OperLog
	PostRpc       postClient.Post
	RoleRpc       roleClient.Role
	RoleDeptRpc   roleDeptClient.RoleDept
	UserRpc       userClient.User
	UserOnlineRpc userOnlineClient.UserOnline
	UserPostRpc   userPostClient.UserPost
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config: c,
		// middle
		CheckUrlAuth: middleware.NewCheckUrlAuthMiddleware(userScopeClient.NewUserScope(zrpc.MustNewClient(c.SystemRpc))).Handle,
		// rpc
		CaptchaRpc:    captchaClient.NewCaptcha(zrpc.MustNewClient(c.SystemRpc)),
		ConfigRpc:     configClient.NewConfig(zrpc.MustNewClient(c.SystemRpc)),
		DeptRpc:       deptClient.NewDept(zrpc.MustNewClient(c.SystemRpc)),
		DictDataRpc:   dictDataClient.NewDictData(zrpc.MustNewClient(c.SystemRpc)),
		DictTypeRpc:   dictTypeClient.NewDictType(zrpc.MustNewClient(c.SystemRpc)),
		LoginRpc:      loginClient.NewLogin(zrpc.MustNewClient(c.SystemRpc)),
		LoginLogRpc:   loginLogClient.NewLoginLog(zrpc.MustNewClient(c.SystemRpc)),
		MenuRpc:       menuClient.NewMenu(zrpc.MustNewClient(c.SystemRpc)),
		OperLogRpc:    operLogClient.NewOperLog(zrpc.MustNewClient(c.SystemRpc)),
		PostRpc:       postClient.NewPost(zrpc.MustNewClient(c.SystemRpc)),
		RoleRpc:       roleClient.NewRole(zrpc.MustNewClient(c.SystemRpc)),
		RoleDeptRpc:   roleDeptClient.NewRoleDept(zrpc.MustNewClient(c.SystemRpc)),
		UserRpc:       userClient.NewUser(zrpc.MustNewClient(c.SystemRpc)),
		UserOnlineRpc: userOnlineClient.NewUserOnline(zrpc.MustNewClient(c.SystemRpc)),
		UserPostRpc:   userPostClient.NewUserPost(zrpc.MustNewClient(c.SystemRpc)),
	}
}
