package main

import (
	"flag"
	"fmt"

	"go-zero-admin/apps/system/cmd/rpc/internal/config"
	captchaServer "go-zero-admin/apps/system/cmd/rpc/internal/server/captcha"
	configServer "go-zero-admin/apps/system/cmd/rpc/internal/server/config"
	deptServer "go-zero-admin/apps/system/cmd/rpc/internal/server/dept"
	dictDataServer "go-zero-admin/apps/system/cmd/rpc/internal/server/dictdata"
	dictTypeServer "go-zero-admin/apps/system/cmd/rpc/internal/server/dicttype"
	loginServer "go-zero-admin/apps/system/cmd/rpc/internal/server/login"
	loginLogServer "go-zero-admin/apps/system/cmd/rpc/internal/server/loginlog"
	menuServer "go-zero-admin/apps/system/cmd/rpc/internal/server/menu"
	operLogServer "go-zero-admin/apps/system/cmd/rpc/internal/server/operlog"
	postServer "go-zero-admin/apps/system/cmd/rpc/internal/server/post"
	roleServer "go-zero-admin/apps/system/cmd/rpc/internal/server/role"
	roleDeptServer "go-zero-admin/apps/system/cmd/rpc/internal/server/roledept"
	userServer "go-zero-admin/apps/system/cmd/rpc/internal/server/user"
	userOnlineServer "go-zero-admin/apps/system/cmd/rpc/internal/server/useronline"
	userPostServer "go-zero-admin/apps/system/cmd/rpc/internal/server/userpost"
	userScopeServer "go-zero-admin/apps/system/cmd/rpc/internal/server/userscope"
	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/interceptor/rpcserver"

	"github.com/zeromicro/go-zero/core/conf"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/service"
	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var configFile = flag.String("f", "etc/system.yaml", "the config file")

func main() {
	flag.Parse()

	// close log
	logx.DisableStat()

	var c config.Config
	conf.MustLoad(*configFile, &c)
	ctx := svc.NewServiceContext(c)

	s := zrpc.MustNewServer(c.RpcServerConf, func(grpcServer *grpc.Server) {
		syspb.RegisterCaptchaServer(grpcServer, captchaServer.NewCaptchaServer(ctx))
		syspb.RegisterConfigServer(grpcServer, configServer.NewConfigServer(ctx))
		syspb.RegisterDeptServer(grpcServer, deptServer.NewDeptServer(ctx))
		syspb.RegisterDictDataServer(grpcServer, dictDataServer.NewDictDataServer(ctx))
		syspb.RegisterDictTypeServer(grpcServer, dictTypeServer.NewDictTypeServer(ctx))
		syspb.RegisterLoginServer(grpcServer, loginServer.NewLoginServer(ctx))
		syspb.RegisterLoginLogServer(grpcServer, loginLogServer.NewLoginLogServer(ctx))
		syspb.RegisterMenuServer(grpcServer, menuServer.NewMenuServer(ctx))
		syspb.RegisterOperLogServer(grpcServer, operLogServer.NewOperLogServer(ctx))
		syspb.RegisterPostServer(grpcServer, postServer.NewPostServer(ctx))
		syspb.RegisterRoleServer(grpcServer, roleServer.NewRoleServer(ctx))
		syspb.RegisterRoleDeptServer(grpcServer, roleDeptServer.NewRoleDeptServer(ctx))
		syspb.RegisterUserServer(grpcServer, userServer.NewUserServer(ctx))
		syspb.RegisterUserOnlineServer(grpcServer, userOnlineServer.NewUserOnlineServer(ctx))
		syspb.RegisterUserPostServer(grpcServer, userPostServer.NewUserPostServer(ctx))
		syspb.RegisterUserScopeServer(grpcServer, userScopeServer.NewUserScopeServer(ctx))

		if c.Mode == service.DevMode || c.Mode == service.TestMode {
			reflection.Register(grpcServer)
		}
	})

	// rpc log interceptor
	s.AddUnaryInterceptors(rpcserver.LoggerInterceptor)

	defer s.Stop()

	fmt.Printf("【SYSTEM-RPC】Starting system's rpc server at %s...\n", c.ListenOn)
	s.Start()
}
