// Code generated by goctl. DO NOT EDIT.
// Source: system.proto

package client

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
)

type (
	AddConfigReq                = syspb.AddConfigReq
	AddConfigResp               = syspb.AddConfigResp
	AddDeptReq                  = syspb.AddDeptReq
	AddDeptResp                 = syspb.AddDeptResp
	AddDictDataReq              = syspb.AddDictDataReq
	AddDictDataResp             = syspb.AddDictDataResp
	AddDictTypeReq              = syspb.AddDictTypeReq
	AddDictTypeResp             = syspb.AddDictTypeResp
	AddLoginLogReq              = syspb.AddLoginLogReq
	AddLoginLogResp             = syspb.AddLoginLogResp
	AddMenuReq                  = syspb.AddMenuReq
	AddMenuResp                 = syspb.AddMenuResp
	AddOperLogReq               = syspb.AddOperLogReq
	AddOperLogResp              = syspb.AddOperLogResp
	AddPostReq                  = syspb.AddPostReq
	AddPostResp                 = syspb.AddPostResp
	AddRoleReq                  = syspb.AddRoleReq
	AddRoleResp                 = syspb.AddRoleResp
	AddUserDataReq              = syspb.AddUserDataReq
	AddUserDataResp             = syspb.AddUserDataResp
	CaptchaForGetReq            = syspb.CaptchaForGetReq
	CaptchaForGetResp           = syspb.CaptchaForGetResp
	CaptchaForVerifyReq         = syspb.CaptchaForVerifyReq
	CaptchaForVerifyResp        = syspb.CaptchaForVerifyResp
	CheckUserRuleAuthReq        = syspb.CheckUserRuleAuthReq
	CheckUserRuleAuthResp       = syspb.CheckUserRuleAuthResp
	DelConfigReq                = syspb.DelConfigReq
	DelConfigResp               = syspb.DelConfigResp
	DelDeptReq                  = syspb.DelDeptReq
	DelDeptResp                 = syspb.DelDeptResp
	DelDictDataReq              = syspb.DelDictDataReq
	DelDictDataResp             = syspb.DelDictDataResp
	DelDictTypeReq              = syspb.DelDictTypeReq
	DelDictTypeResp             = syspb.DelDictTypeResp
	DelLoginLogReq              = syspb.DelLoginLogReq
	DelLoginLogResp             = syspb.DelLoginLogResp
	DelMenuReq                  = syspb.DelMenuReq
	DelMenuResp                 = syspb.DelMenuResp
	DelOperLogReq               = syspb.DelOperLogReq
	DelOperLogResp              = syspb.DelOperLogResp
	DelPostReq                  = syspb.DelPostReq
	DelPostResp                 = syspb.DelPostResp
	DelRoleReq                  = syspb.DelRoleReq
	DelRoleResp                 = syspb.DelRoleResp
	DelUserReq                  = syspb.DelUserReq
	DelUserResp                 = syspb.DelUserResp
	GenerateTokenReq            = syspb.GenerateTokenReq
	GenerateTokenResp           = syspb.GenerateTokenResp
	GetConfigListReq            = syspb.GetConfigListReq
	GetConfigListResp           = syspb.GetConfigListResp
	GetConfigOneReq             = syspb.GetConfigOneReq
	GetConfigOneResp            = syspb.GetConfigOneResp
	GetDeptListReq              = syspb.GetDeptListReq
	GetDeptListResp             = syspb.GetDeptListResp
	GetDeptSonByPidReq          = syspb.GetDeptSonByPidReq
	GetDeptSonByPidResp         = syspb.GetDeptSonByPidResp
	GetDictDataListReq          = syspb.GetDictDataListReq
	GetDictDataListResp         = syspb.GetDictDataListResp
	GetDictDataOneReq           = syspb.GetDictDataOneReq
	GetDictDataOneResp          = syspb.GetDictDataOneResp
	GetDictTypeListReq          = syspb.GetDictTypeListReq
	GetDictTypeListResp         = syspb.GetDictTypeListResp
	GetDictTypeOneReq           = syspb.GetDictTypeOneReq
	GetDictTypeOneResp          = syspb.GetDictTypeOneResp
	GetLoginLogListReq          = syspb.GetLoginLogListReq
	GetLoginLogListResp         = syspb.GetLoginLogListResp
	GetMenuListReq              = syspb.GetMenuListReq
	GetMenuListResp             = syspb.GetMenuListResp
	GetMenuOneReq               = syspb.GetMenuOneReq
	GetMenuOneResp              = syspb.GetMenuOneResp
	GetMenuRoleIdsReq           = syspb.GetMenuRoleIdsReq
	GetMenuRoleIdsResp          = syspb.GetMenuRoleIdsResp
	GetMenuTreeReq              = syspb.GetMenuTreeReq
	GetMenuTreeResp             = syspb.GetMenuTreeResp
	GetOperLogListReq           = syspb.GetOperLogListReq
	GetOperLogListResp          = syspb.GetOperLogListResp
	GetPostListReq              = syspb.GetPostListReq
	GetPostListResp             = syspb.GetPostListResp
	GetRoleByUidReq             = syspb.GetRoleByUidReq
	GetRoleByUidResp            = syspb.GetRoleByUidResp
	GetRoleDeptListReq          = syspb.GetRoleDeptListReq
	GetRoleDeptListResp         = syspb.GetRoleDeptListResp
	GetRoleIdsByUidReq          = syspb.GetRoleIdsByUidReq
	GetRoleIdsByUidResp         = syspb.GetRoleIdsByUidResp
	GetRoleListReq              = syspb.GetRoleListReq
	GetRoleListResp             = syspb.GetRoleListResp
	GetRoleOneReq               = syspb.GetRoleOneReq
	GetRoleOneResp              = syspb.GetRoleOneResp
	GetRoleRuleByRidReq         = syspb.GetRoleRuleByRidReq
	GetRoleRuleByRidResp        = syspb.GetRoleRuleByRidResp
	GetUserByAccountReq         = syspb.GetUserByAccountReq
	GetUserByAccountResp        = syspb.GetUserByAccountResp
	GetUserComboReq             = syspb.GetUserComboReq
	GetUserComboResp            = syspb.GetUserComboResp
	GetUserDataReq              = syspb.GetUserDataReq
	GetUserDataResp             = syspb.GetUserDataResp
	GetUserDataScopeReq         = syspb.GetUserDataScopeReq
	GetUserDataScopeResp        = syspb.GetUserDataScopeResp
	GetUserListReq              = syspb.GetUserListReq
	GetUserListResp             = syspb.GetUserListResp
	GetUserOneReq               = syspb.GetUserOneReq
	GetUserOneResp              = syspb.GetUserOneResp
	GetUserOnlineListReq        = syspb.GetUserOnlineListReq
	GetUserOnlineListResp       = syspb.GetUserOnlineListResp
	GetUserPostListReq          = syspb.GetUserPostListReq
	GetUserPostListResp         = syspb.GetUserPostListResp
	GetUserRulesReq             = syspb.GetUserRulesReq
	GetUserRulesResp            = syspb.GetUserRulesResp
	IsExConfigKeyReq            = syspb.IsExConfigKeyReq
	IsExConfigKeyResp           = syspb.IsExConfigKeyResp
	IsExDeptNameReq             = syspb.IsExDeptNameReq
	IsExDeptNameResp            = syspb.IsExDeptNameResp
	IsExDictTypeReq             = syspb.IsExDictTypeReq
	IsExDictTypeResp            = syspb.IsExDictTypeResp
	IsExMenuNameReq             = syspb.IsExMenuNameReq
	IsExMenuNameResp            = syspb.IsExMenuNameResp
	IsExNameOrMobileOrEmailReq  = syspb.IsExNameOrMobileOrEmailReq
	IsExNameOrMobileOrEmailResp = syspb.IsExNameOrMobileOrEmailResp
	IsExPostNameCodeReq         = syspb.IsExPostNameCodeReq
	IsExPostNameCodeResp        = syspb.IsExPostNameCodeResp
	IsExRoleNameReq             = syspb.IsExRoleNameReq
	IsExRoleNameResp            = syspb.IsExRoleNameResp
	LoginReq                    = syspb.LoginReq
	LoginResp                   = syspb.LoginResp
	UpConfigReq                 = syspb.UpConfigReq
	UpConfigResp                = syspb.UpConfigResp
	UpDeptReq                   = syspb.UpDeptReq
	UpDeptResp                  = syspb.UpDeptResp
	UpDictDataReq               = syspb.UpDictDataReq
	UpDictDataResp              = syspb.UpDictDataResp
	UpDictTypeReq               = syspb.UpDictTypeReq
	UpDictTypeResp              = syspb.UpDictTypeResp
	UpMenuReq                   = syspb.UpMenuReq
	UpMenuResp                  = syspb.UpMenuResp
	UpPostReq                   = syspb.UpPostReq
	UpPostResp                  = syspb.UpPostResp
	UpRoleReq                   = syspb.UpRoleReq
	UpRoleResp                  = syspb.UpRoleResp
	UpUserDataReq               = syspb.UpUserDataReq
	UpUserDataResp              = syspb.UpUserDataResp
	UpUserLoginReq              = syspb.UpUserLoginReq
	UpUserLoginResp             = syspb.UpUserLoginResp
	UpUserPwdReq                = syspb.UpUserPwdReq
	UpUserPwdResp               = syspb.UpUserPwdResp
	UpUserStatusReq             = syspb.UpUserStatusReq
	UpUserStatusResp            = syspb.UpUserStatusResp

	DictData interface {
		GetDictDataOne(ctx context.Context, in *GetDictDataOneReq, opts ...grpc.CallOption) (*GetDictDataOneResp, error)
		GetDictDataList(ctx context.Context, in *GetDictDataListReq, opts ...grpc.CallOption) (*GetDictDataListResp, error)
		AddDictData(ctx context.Context, in *AddDictDataReq, opts ...grpc.CallOption) (*AddDictDataResp, error)
		UpDictData(ctx context.Context, in *UpDictDataReq, opts ...grpc.CallOption) (*UpDictDataResp, error)
		DelDictData(ctx context.Context, in *DelDictDataReq, opts ...grpc.CallOption) (*DelDictDataResp, error)
	}

	defaultDictData struct {
		cli zrpc.Client
	}
)

func NewDictData(cli zrpc.Client) DictData {
	return &defaultDictData{
		cli: cli,
	}
}

func (m *defaultDictData) GetDictDataOne(ctx context.Context, in *GetDictDataOneReq, opts ...grpc.CallOption) (*GetDictDataOneResp, error) {
	client := syspb.NewDictDataClient(m.cli.Conn())
	return client.GetDictDataOne(ctx, in, opts...)
}

func (m *defaultDictData) GetDictDataList(ctx context.Context, in *GetDictDataListReq, opts ...grpc.CallOption) (*GetDictDataListResp, error) {
	client := syspb.NewDictDataClient(m.cli.Conn())
	return client.GetDictDataList(ctx, in, opts...)
}

func (m *defaultDictData) AddDictData(ctx context.Context, in *AddDictDataReq, opts ...grpc.CallOption) (*AddDictDataResp, error) {
	client := syspb.NewDictDataClient(m.cli.Conn())
	return client.AddDictData(ctx, in, opts...)
}

func (m *defaultDictData) UpDictData(ctx context.Context, in *UpDictDataReq, opts ...grpc.CallOption) (*UpDictDataResp, error) {
	client := syspb.NewDictDataClient(m.cli.Conn())
	return client.UpDictData(ctx, in, opts...)
}

func (m *defaultDictData) DelDictData(ctx context.Context, in *DelDictDataReq, opts ...grpc.CallOption) (*DelDictDataResp, error) {
	client := syspb.NewDictDataClient(m.cli.Conn())
	return client.DelDictData(ctx, in, opts...)
}
