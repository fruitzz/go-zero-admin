package svc

import (
	"go-zero-admin/apps/system/cmd/rpc/internal/config"
	"go-zero-admin/apps/system/cmd/rpc/internal/svc/modules"
	"go-zero-admin/apps/system/model"

	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config config.Config
	// modules
	AdapterCasbin *modules.AdapterCasbin
	Captcha       *modules.Captcha
	// model
	SysCasbinRuleModel model.SysCasbinRuleModel
	SysConfigModel     model.SysConfigModel
	SysDeptModel       model.SysDeptModel
	SysDictDataModel   model.SysDictDataModel
	SysDictTypeModel   model.SysDictTypeModel
	SysLoginLogModel   model.SysLoginLogModel
	SysMenuModel       model.SysMenuModel
	SysOperLogModel    model.SysOperLogModel
	SysPostModel       model.SysPostModel
	SysRoleDeptModel   model.SysRoleDeptModel
	SysRoleModel       model.SysRoleModel
	SysUserModel       model.SysUserModel
	SysUserOnlineModel model.SysUserOnlineModel
	SysUserPostModel   model.SysUserPostModel
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewMysql(c.DB.DataSource)
	return &ServiceContext{
		Config: c,
		// modules
		AdapterCasbin: modules.NewCasbinServer(c, conn),
		Captcha:       modules.NewCaptchaServer(),
		// model
		SysCasbinRuleModel: model.NewSysCasbinRuleModel(conn, c.ModelCache),
		SysConfigModel:     model.NewSysConfigModel(conn, c.ModelCache),
		SysDeptModel:       model.NewSysDeptModel(conn, c.ModelCache),
		SysDictDataModel:   model.NewSysDictDataModel(conn, c.ModelCache),
		SysDictTypeModel:   model.NewSysDictTypeModel(conn, c.ModelCache),
		SysLoginLogModel:   model.NewSysLoginLogModel(conn, c.ModelCache),
		SysMenuModel:       model.NewSysMenuModel(conn, c.ModelCache),
		SysOperLogModel:    model.NewSysOperLogModel(conn, c.ModelCache),
		SysPostModel:       model.NewSysPostModel(conn, c.ModelCache),
		SysRoleDeptModel:   model.NewSysRoleDeptModel(conn, c.ModelCache),
		SysRoleModel:       model.NewSysRoleModel(conn, c.ModelCache),
		SysUserModel:       model.NewSysUserModel(conn, c.ModelCache),
		SysUserOnlineModel: model.NewSysUserOnlineModel(conn, c.ModelCache),
		SysUserPostModel:   model.NewSysUserPostModel(conn, c.ModelCache),
	}
}
