package modules

import (
	"github.com/mojocn/base64Captcha"
)

type Captcha struct {
	Driver *base64Captcha.DriverString
	Store  base64Captcha.Store
}

// NewCaptchaServer 创建captcha服务
func NewCaptchaServer() *Captcha {
	return &Captcha{
		Driver: &base64Captcha.DriverString{
			Height:          80,
			Width:           240,
			NoiseCount:      50,
			ShowLineOptions: 20,
			Length:          4,
			Source:          "abcdefghjkmnpqrstuvwxyz23456789",
			Fonts:           []string{"chromohv.ttf"},
		},
		Store: base64Captcha.DefaultMemStore,
	}
}
