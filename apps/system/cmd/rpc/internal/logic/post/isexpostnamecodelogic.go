package postlogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type IsExPostNameCodeLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	getPostListLogic *GetPostListLogic
}

func NewIsExPostNameCodeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *IsExPostNameCodeLogic {
	return &IsExPostNameCodeLogic{
		ctx:              ctx,
		svcCtx:           svcCtx,
		Logger:           logx.WithContext(ctx),
		getPostListLogic: NewGetPostListLogic(ctx, svcCtx),
	}
}

func (l *IsExPostNameCodeLogic) IsExPostNameCode(in *syspb.IsExPostNameCodeReq) (*syspb.IsExPostNameCodeResp, error) {
	if in.GetName() == "" && in.GetCode() == "" {
		return &syspb.IsExPostNameCodeResp{IsExist: true}, nil
	}

	// 查询条件
	params := &syspb.GetPostListReq{
		Page: &syspb.PageReq{
			IsCount: true,
		},
		PostName:       in.GetName(),
		PostCode:       in.GetCode(),
		IsExCodeOrName: true,
	}
	if in.GetIsNotId() != 0 {
		params.IsNotId = in.GetIsNotId()
	}

	// 查询
	listResp, err := l.getPostListLogic.GetPostList(params)
	if err != nil {
		return nil, err
	}

	// 处理数据
	isExist := true
	if listResp.GetPage().GetTotal() == 0 {
		isExist = false
	}

	return &syspb.IsExPostNameCodeResp{IsExist: isExist}, nil
}
