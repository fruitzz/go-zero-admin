package captchalogic

import (
	"context"
	"strings"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/mojocn/base64Captcha"
	"github.com/zeromicro/go-zero/core/logx"
)

type CaptchaForVerifyLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCaptchaForVerifyLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CaptchaForVerifyLogic {
	return &CaptchaForVerifyLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CaptchaForVerifyLogic) CaptchaForVerify(in *syspb.CaptchaForVerifyReq) (*syspb.CaptchaForVerifyResp, error) {
	if in.GetId() == "" || in.GetAnswer() == "" {
		return &syspb.CaptchaForVerifyResp{IsTrue: false}, nil
	}

	// 验证
	c := base64Captcha.NewCaptcha(l.svcCtx.Captcha.Driver, l.svcCtx.Captcha.Store)
	verify := c.Verify(in.GetId(), strings.ToLower(in.GetAnswer()), true)

	return &syspb.CaptchaForVerifyResp{IsTrue: verify}, nil
}
