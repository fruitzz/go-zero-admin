package dictdatalogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetDictDataListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetDictDataListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDictDataListLogic {
	return &GetDictDataListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetDictDataListLogic) GetDictDataList(in *syspb.GetDictDataListReq) (*syspb.GetDictDataListResp, error) {
	listWhereBuilder := l.svcCtx.SysDictDataModel.RowBuilder()
	countWhereBuilder := l.svcCtx.SysDictDataModel.CountBuilder("1")
	if in.GetDictLabel() != "" {
		condition := squirrel.Like{"dict_label": in.GetDictLabel() + "%"}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetStatus() != "" {
		condition := squirrel.Eq{"status": in.GetStatus()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if len(in.GetDictTypeList()) > 0 {
		condition := squirrel.Eq{"dict_type": in.GetDictTypeList()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetDictDataListLogic) getList(in *syspb.GetDictDataListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetDictDataListResp, error) {
	orderBy := "dict_sort,dict_code asc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.SysDictDataModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dictData list"), err, in)
	}

	// 转换数据
	var dataList []*syspb.SysDictData
	for _, v := range list {
		var data syspb.SysDictData
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetDictDataListResp{
		List: dataList,
	}, nil
}

func (l *GetDictDataListLogic) getPage(in *syspb.GetDictDataListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*syspb.GetDictDataListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageSize = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "dict_sort,dict_code asc"
	}

	// 分页查询
	list, err := l.svcCtx.SysDictDataModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dictData list"), err, in)
	}
	count, err := l.svcCtx.SysDictDataModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dictData count"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysDictData
	for _, v := range list {
		var data syspb.SysDictData
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetDictDataListResp{
		List: dataList,
		Page: &syspb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetDictDataListLogic) getCount(in *syspb.GetDictDataListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetDictDataListResp, error) {
	count, err := l.svcCtx.SysDictDataModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dictData count"), err, in)
	}

	return &syspb.GetDictDataListResp{
		Page: &syspb.PageResp{
			Total: count,
		},
	}, nil
}
