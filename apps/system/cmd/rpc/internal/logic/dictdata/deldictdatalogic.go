package dictdatalogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type DelDictDataLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDelDictDataLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DelDictDataLogic {
	return &DelDictDataLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DelDictDataLogic) DelDictData(in *syspb.DelDictDataReq) (*syspb.DelDictDataResp, error) {
	if len(in.GetIds()) == 0 || (!in.GetIsDel() && in.GetUpdateUser() == "") {
		return nil, errors.Wrapf(xerr.NewErrMsg("删除字典数据失败：参数缺失"),
			tool.GetErrMsgFormat("delete dictData"), "params is not existed", in)
	}

	// 删除数据
	err := l.svcCtx.SysDictDataModel.DeleteEx(l.ctx, nil, in.GetIds(), in.GetIsDel(), in.GetUpdateUser())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("delete dictData"), err, in)
	}

	return &syspb.DelDictDataResp{}, nil
}
