package dictdatalogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetDictDataOneLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetDictDataOneLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDictDataOneLogic {
	return &GetDictDataOneLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetDictDataOneLogic) GetDictDataOne(in *syspb.GetDictDataOneReq) (*syspb.GetDictDataOneResp, error) {
	one, err := l.svcCtx.SysDictDataModel.FindOne(l.ctx, in.GetDictCode())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("get dictData one"), err, in)
	}

	// 数据转换
	var data syspb.SysDictData
	_ = copier.Copy(&data, one)

	return &syspb.GetDictDataOneResp{One: &data}, nil
}
