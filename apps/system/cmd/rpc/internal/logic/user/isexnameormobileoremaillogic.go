package userlogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type IsExNameOrMobileOrEmailLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	getUserListLogic *GetUserListLogic
}

func NewIsExNameOrMobileOrEmailLogic(ctx context.Context, svcCtx *svc.ServiceContext) *IsExNameOrMobileOrEmailLogic {
	return &IsExNameOrMobileOrEmailLogic{
		ctx:              ctx,
		svcCtx:           svcCtx,
		Logger:           logx.WithContext(ctx),
		getUserListLogic: NewGetUserListLogic(ctx, svcCtx),
	}
}

func (l *IsExNameOrMobileOrEmailLogic) IsExNameOrMobileOrEmail(in *syspb.IsExNameOrMobileOrEmailReq) (*syspb.IsExNameOrMobileOrEmailResp, error) {
	if in.GetUserName() == "" || in.GetMobile() == "" || in.GetEmail() == "" {
		return &syspb.IsExNameOrMobileOrEmailResp{IsExist: true}, nil
	}

	// 条件
	params := &syspb.GetUserListReq{
		Page: &syspb.PageReq{
			IsCount: true,
		},
		UserName:                in.GetUserName(),
		Mobile:                  in.GetMobile(),
		Email:                   in.GetEmail(),
		IsExNameOrMobileOrEmail: true,
	}
	if in.GetIsNotId() != 0 {
		params.IsNotId = in.GetIsNotId()
	}

	// 查询
	listResp, err := l.getUserListLogic.GetUserList(params)
	if err != nil {
		return nil, err
	}

	// 处理
	isExist := false
	if listResp.GetPage().Total > 0 {
		isExist = true
	}

	return &syspb.IsExNameOrMobileOrEmailResp{IsExist: isExist}, nil
}
