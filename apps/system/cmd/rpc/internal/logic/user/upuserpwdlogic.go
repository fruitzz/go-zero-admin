package userlogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/duke-git/lancet/v2/random"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpUserPwdLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpUserPwdLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpUserPwdLogic {
	return &UpUserPwdLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpUserPwdLogic) UpUserPwd(in *syspb.UpUserPwdReq) (*syspb.UpUserPwdResp, error) {
	if in.GetPassword() == "" || in.GetUpdateUser() == "" {
		return nil, errors.Wrapf(xerr.NewErrMsg("更新用户密码失败：参数缺失"),
			tool.GetErrMsgFormat("update user password"), "params is not existed", in)
	}

	// 重置密码
	salt := random.RandString(10)
	password := tool.EncryptPassword(in.GetPassword(), salt)

	// 查询出用户信息
	one, err := l.svcCtx.SysUserModel.FindOne(l.ctx, in.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update user password"), err, in)
	}
	one.UserPassword = password
	one.UserSalt = salt
	one.UpdateUser = in.GetUpdateUser()

	// 更新
	err = l.svcCtx.SysUserModel.UpdateEx(l.ctx, nil, one)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update user password"), err, in)
	}

	return &syspb.UpUserPwdResp{}, nil
}
