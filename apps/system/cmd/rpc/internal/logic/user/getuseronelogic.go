package userlogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetUserOneLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetUserOneLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetUserOneLogic {
	return &GetUserOneLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetUserOneLogic) GetUserOne(in *syspb.GetUserOneReq) (*syspb.GetUserOneResp, error) {
	sysUser, err := l.svcCtx.SysUserModel.FindOne(l.ctx, in.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("get user one"), err, in)
	}

	if !in.GetWithPwd() {
		sysUser.UserPassword = ""
		sysUser.UserSalt = ""
	}

	var data syspb.SysUser
	_ = copier.Copy(&data, sysUser)

	return &syspb.GetUserOneResp{
		One: &data,
	}, nil
}
