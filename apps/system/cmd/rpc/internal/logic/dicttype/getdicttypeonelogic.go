package dicttypelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetDictTypeOneLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetDictTypeOneLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDictTypeOneLogic {
	return &GetDictTypeOneLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetDictTypeOneLogic) GetDictTypeOne(in *syspb.GetDictTypeOneReq) (*syspb.GetDictTypeOneResp, error) {
	one, err := l.svcCtx.SysDictTypeModel.FindOne(l.ctx, in.GetDictId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("get dictType one"), err, in)
	}

	// 数据转换
	var data syspb.SysDictType
	_ = copier.Copy(&data, one)

	return &syspb.GetDictTypeOneResp{One: &data}, nil
}
