package dicttypelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetDictTypeListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetDictTypeListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDictTypeListLogic {
	return &GetDictTypeListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetDictTypeListLogic) GetDictTypeList(in *syspb.GetDictTypeListReq) (*syspb.GetDictTypeListResp, error) {
	listWhereBuilder := l.svcCtx.SysDictTypeModel.RowBuilder()
	countWhereBuilder := l.svcCtx.SysDictTypeModel.CountBuilder("1")
	if in.GetDictName() != "" {
		condition := squirrel.Like{"dict_name": in.GetDictName() + "%"}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetDictType() != "" {
		condition := squirrel.Eq{"dict_type": in.GetDictType()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetStatus() != "" {
		condition := squirrel.Eq{"status": in.GetStatus()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetIsNotId() != 0 {
		condition := squirrel.NotEq{"dict_id": in.GetIsNotId()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if len(in.GetPage().GetDateRange()) > 0 {
		condition := squirrel.And{squirrel.GtOrEq{"create_time": in.GetPage().GetDateRange()[0]},
			squirrel.LtOrEq{"create_time": in.GetPage().GetDateRange()[1]}}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetDictTypeListLogic) getList(in *syspb.GetDictTypeListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetDictTypeListResp, error) {
	orderBy := "dict_id desc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.SysDictTypeModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dictType list"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysDictType
	for _, v := range list {
		var data syspb.SysDictType
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetDictTypeListResp{
		List: dataList,
	}, nil
}

func (l *GetDictTypeListLogic) getPage(in *syspb.GetDictTypeListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*syspb.GetDictTypeListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageSize = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "dict_id desc"
	}

	// 查询数据
	list, err := l.svcCtx.SysDictTypeModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dictType list"), err, in)
	}

	count, err := l.svcCtx.SysDictTypeModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dictType count"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysDictType
	for _, v := range list {
		var data syspb.SysDictType
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetDictTypeListResp{
		List: dataList,
		Page: &syspb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetDictTypeListLogic) getCount(in *syspb.GetDictTypeListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetDictTypeListResp, error) {
	count, err := l.svcCtx.SysDictTypeModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dictType count"), err, in)
	}

	return &syspb.GetDictTypeListResp{
		Page: &syspb.PageResp{
			Total: count,
		},
	}, nil
}
