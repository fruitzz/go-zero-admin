package dicttypelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/logic/dictdata"
	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type UpDictTypeLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	isExDictTypeLogic    *IsExDictTypeLogic
	getDictDataListLogic *dictdatalogic.GetDictDataListLogic
}

func NewUpDictTypeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpDictTypeLogic {
	return &UpDictTypeLogic{
		ctx:                  ctx,
		svcCtx:               svcCtx,
		Logger:               logx.WithContext(ctx),
		isExDictTypeLogic:    NewIsExDictTypeLogic(ctx, svcCtx),
		getDictDataListLogic: dictdatalogic.NewGetDictDataListLogic(ctx, svcCtx),
	}
}

func (l *UpDictTypeLogic) UpDictType(in *syspb.UpDictTypeReq) (*syspb.UpDictTypeResp, error) {
	dictType := in.GetParams()
	if dictType == nil || dictType.GetDictId() == 0 || dictType.GetDictName() == "" ||
		dictType.GetDictType() == "" || dictType.GetUpdateUser() == "" {
		return nil, errors.Wrapf(xerr.NewErrMsg("更新字典类型失败：参数缺失"),
			tool.GetErrMsgFormat("update dictType"), "params is not existed", in)
	}

	// 校验字典类型是否重复
	resp, err := l.isExDictTypeLogic.IsExDictType(&syspb.IsExDictTypeReq{
		DictType: dictType.GetDictType(),
		IsNotId:  dictType.GetDictId(),
	})
	if err != nil {
		return nil, err
	}
	if resp.GetIsExist() {
		return nil, errors.Wrapf(xerr.NewErrMsg("更新字典类型失败：字典类型已存在"),
			tool.GetErrMsgFormat("update dictType"), "dict type is existed", in)
	}

	// 转换数据
	one, err := l.svcCtx.SysDictTypeModel.FindOne(l.ctx, dictType.GetDictId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update dictType"), err, in)
	}
	oldDictType := one.DictType

	// 处理新数据
	one.DictName = dictType.GetDictName()
	one.DictType = dictType.GetDictType()
	one.Status = dictType.GetStatus()
	one.Remark = dictType.GetRemark()
	one.UpdateUser = dictType.GetUpdateUser()

	// 更新数据
	if err = l.svcCtx.SysDictTypeModel.Trans(l.ctx, func(context context.Context, session sqlx.Session) error {
		e := l.svcCtx.SysDictTypeModel.UpdateEx(l.ctx, session, one)
		if e != nil {
			return e
		}

		// 查询字典数据
		dictDataListResp, e := l.getDictDataListLogic.GetDictDataList(&syspb.GetDictDataListReq{
			DictTypeList: []string{oldDictType},
		})
		if e != nil {
			return e
		}
		if len(dictDataListResp.GetList()) == 0 {
			return nil
		}

		// 修改字典数据
		for _, v := range dictDataListResp.GetList() {
			var data model.SysDictData
			_ = copier.Copy(&data, v)

			data.DictType = dictType.GetDictType()
			data.UpdateUser = dictType.GetUpdateUser()

			e = l.svcCtx.SysDictDataModel.UpdateEx(l.ctx, session, &data)
			if e != nil {
				return e
			}
		}

		return nil
	}); err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update dictType"), err, in)
	}

	return &syspb.UpDictTypeResp{}, nil
}
