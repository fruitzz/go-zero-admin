package dicttypelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type IsExDictTypeLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	getDictTypeListLogic *GetDictTypeListLogic
}

func NewIsExDictTypeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *IsExDictTypeLogic {
	return &IsExDictTypeLogic{
		ctx:                  ctx,
		svcCtx:               svcCtx,
		Logger:               logx.WithContext(ctx),
		getDictTypeListLogic: NewGetDictTypeListLogic(ctx, svcCtx),
	}
}

func (l *IsExDictTypeLogic) IsExDictType(in *syspb.IsExDictTypeReq) (*syspb.IsExDictTypeResp, error) {
	if in.GetDictType() == "" {
		return &syspb.IsExDictTypeResp{IsExist: true}, nil
	}

	// 查询条件
	params := &syspb.GetDictTypeListReq{
		Page: &syspb.PageReq{
			IsCount: true,
		},
		DictType: in.GetDictType(),
	}
	if in.GetIsNotId() != 0 {
		params.IsNotId = in.GetIsNotId()
	}

	// 查询
	listResp, err := l.getDictTypeListLogic.GetDictTypeList(params)
	if err != nil {
		return nil, err
	}

	// 数据转换
	isExist := false
	if listResp.GetPage().Total > 0 {
		isExist = true
	}

	return &syspb.IsExDictTypeResp{IsExist: isExist}, nil
}
