package rolelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetRoleByUidLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	getRoleIdsByUidLogic *GetRoleIdsByUidLogic
	getRoleListLogic     *GetRoleListLogic
}

func NewGetRoleByUidLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetRoleByUidLogic {
	return &GetRoleByUidLogic{
		ctx:                  ctx,
		svcCtx:               svcCtx,
		Logger:               logx.WithContext(ctx),
		getRoleIdsByUidLogic: NewGetRoleIdsByUidLogic(ctx, svcCtx),
		getRoleListLogic:     NewGetRoleListLogic(ctx, svcCtx),
	}
}

func (l *GetRoleByUidLogic) GetRoleByUid(in *syspb.GetRoleByUidReq) (*syspb.GetRoleByUidResp, error) {
	if in.GetUserId() == 0 {
		return &syspb.GetRoleByUidResp{List: nil}, nil
	}

	// 查询数据
	roleIdsResp, err := l.getRoleIdsByUidLogic.GetRoleIdsByUid(&syspb.GetRoleIdsByUidReq{
		UserId: in.GetUserId(),
	})
	if err != nil {
		return nil, err
	}
	roleIds := roleIdsResp.GetRoleIds()

	// 处理数据
	var roles []*syspb.SysRole
	if len(roleIds) > 0 {
		roleListResp, err := l.getRoleListLogic.GetRoleList(&syspb.GetRoleListReq{
			Ids: roleIds,
		})
		if err != nil {
			return nil, err
		}
		roles = roleListResp.GetList()
	}

	return &syspb.GetRoleByUidResp{List: roles}, nil
}
