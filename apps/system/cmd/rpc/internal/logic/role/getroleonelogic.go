package rolelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetRoleOneLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetRoleOneLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetRoleOneLogic {
	return &GetRoleOneLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetRoleOneLogic) GetRoleOne(in *syspb.GetRoleOneReq) (*syspb.GetRoleOneResp, error) {
	one, err := l.svcCtx.SysRoleModel.FindOne(l.ctx, in.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("get role one"), err, in)
	}

	// 转换
	var data syspb.SysRole
	_ = copier.Copy(&data, one)

	return &syspb.GetRoleOneResp{One: &data}, nil
}
