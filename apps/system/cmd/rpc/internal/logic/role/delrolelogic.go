package rolelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/duke-git/lancet/v2/slice"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type DelRoleLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDelRoleLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DelRoleLogic {
	return &DelRoleLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DelRoleLogic) DelRole(in *syspb.DelRoleReq) (*syspb.DelRoleResp, error) {
	if in.GetId() == 0 || (!in.GetIsDel() && in.GetUpdateUser() == "") {
		return nil, errors.Wrapf(xerr.NewErrMsg("删除角色失败：参数缺失"),
			tool.GetErrMsgFormat("delete role"), "params is not existed", in)
	}

	// 校验参数
	enforcer := l.svcCtx.AdapterCasbin.SetCtxSession(l.ctx, nil).Enforcer

	// 判断是否是超级管理员角色
	rids := l.svcCtx.Config.SystemCustom.NotCheckAuthRoleIds
	if slice.Contain(rids, in.GetId()) {
		return nil, errors.Wrapf(xerr.NewErrMsg("删除角色失败：不允许操作超级管理员角色"),
			tool.GetErrMsgFormat("delete role"), "role id is admin", in)
	}

	// 如果已分配，则不允许删除
	policies := enforcer.GetFilteredGroupingPolicy(1, convertor.ToString(in.GetId()))
	var userIds []int64
	for _, policy := range policies {
		val, _ := convertor.ToInt(policy[0])
		userIds = append(userIds, val)
	}
	if len(userIds) > 0 {
		return nil, errors.Wrapf(xerr.NewErrMsg("删除角色失败：该角色已分配，不能删除"),
			tool.GetErrMsgFormat("delete role"), "role is assigned", in)
	}

	// 处理删除
	if err := l.svcCtx.SysRoleModel.Trans(l.ctx, func(context context.Context, session sqlx.Session) error {
		e := l.svcCtx.SysRoleModel.DeleteEx(l.ctx, session, []int64{in.GetId()}, in.GetIsDel(), in.GetUpdateUser())
		if e != nil {
			return e
		}

		// 删除角色与部门关联 自定义角色
		e = l.svcCtx.SysRoleDeptModel.DeleteEx(l.ctx, session, []int64{in.GetId()}, true, "")
		if e != nil {
			return e
		}

		// 删除角色与菜单关联
		enforcer = l.svcCtx.AdapterCasbin.SetCtxSession(l.ctx, session).Enforcer
		_, e = enforcer.RemoveFilteredPolicy(0, convertor.ToString(in.GetId()))
		if e != nil {
			return e
		}

		return nil
	}); err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("delete role"), err, in)
	}

	return &syspb.DelRoleResp{}, nil
}
