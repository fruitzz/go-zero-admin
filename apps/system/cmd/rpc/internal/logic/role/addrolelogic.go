package rolelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type AddRoleLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	isExRoleNameLogic *IsExRoleNameLogic
}

func NewAddRoleLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AddRoleLogic {
	return &AddRoleLogic{
		ctx:               ctx,
		svcCtx:            svcCtx,
		Logger:            logx.WithContext(ctx),
		isExRoleNameLogic: NewIsExRoleNameLogic(ctx, svcCtx),
	}
}

func (l *AddRoleLogic) AddRole(in *syspb.AddRoleReq) (*syspb.AddRoleResp, error) {
	role := in.GetParams()
	if role == nil || role.GetName() == "" || role.GetCreateUser() == "" || role.GetUpdateUser() == "" {
		return nil, errors.Wrapf(xerr.NewErrMsg("新增角色失败：参数缺失"),
			tool.GetErrMsgFormat("insert role"), "params is not existed", in)
	}

	// 校验参数
	isExistResp, err := l.isExRoleNameLogic.IsExRoleName(&syspb.IsExRoleNameReq{
		Name: role.GetName(),
	})
	if err != nil {
		return nil, err
	}
	if isExistResp.GetIsExist() {
		return nil, errors.Wrapf(xerr.NewErrMsg("新增角色："+role.GetName()+" 失败，角色名称已存在"),
			tool.GetErrMsgFormat("insert role"), "role name is existed", role)
	}

	// 数据转换
	var data model.SysRole
	_ = copier.Copy(&data, role)

	// 保存数据
	if err = l.svcCtx.SysRoleModel.Trans(l.ctx, func(context context.Context, session sqlx.Session) error {
		resp, e := l.svcCtx.SysRoleModel.InsertEx(l.ctx, session, []*model.SysRole{&data})
		if e != nil {
			return e
		}
		roleId := resp[0]

		// 保存角色菜单关联
		e = l.HandleRoleRule(roleId, in.GetMenuIds(), false, session)
		if e != nil {
			return e
		}

		return nil
	}); err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("insert role"), err, in)
	}

	return &syspb.AddRoleResp{}, nil
}

// HandleRoleRule 处理角色菜单关联
func (l *AddRoleLogic) HandleRoleRule(roleId int64, ruleIds []int64, isUp bool, session sqlx.Session) error {
	enforcer := l.svcCtx.AdapterCasbin.SetCtxSession(l.ctx, session).Enforcer

	// 更新删除旧的角色和菜单关联
	if isUp {
		_, err := enforcer.RemoveFilteredPolicy(0, convertor.ToString(roleId))
		if err != nil {
			return err
		}
	}

	// 新增角色菜单关联
	if len(ruleIds) > 0 {
		for _, v := range ruleIds {
			_, err := enforcer.AddPolicy(convertor.ToString(roleId), convertor.ToString(v), "All")
			if err != nil {
				return err
			}
		}
	}

	return nil
}
