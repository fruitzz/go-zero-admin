package rolelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/logic/userscope"
	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetRoleListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	getUserDataScopeLogic *userscopelogic.GetUserDataScopeLogic
}

func NewGetRoleListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetRoleListLogic {
	return &GetRoleListLogic{
		ctx:                   ctx,
		svcCtx:                svcCtx,
		Logger:                logx.WithContext(ctx),
		getUserDataScopeLogic: userscopelogic.NewGetUserDataScopeLogic(ctx, svcCtx),
	}
}

func (l *GetRoleListLogic) GetRoleList(in *syspb.GetRoleListReq) (*syspb.GetRoleListResp, error) {
	listWhereBuilder := l.svcCtx.SysRoleModel.RowBuilder()
	countWhereBuilder := l.svcCtx.SysRoleModel.CountBuilder("1")
	if in.GetName() != "" {
		condition := squirrel.Eq{"name": in.GetName()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetStatus() != "" {
		condition := squirrel.Eq{"status": in.GetStatus()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if len(in.GetIds()) > 0 {
		condition := squirrel.Eq{"id": in.GetIds()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetIsNotId() != 0 {
		condition := squirrel.NotEq{"id": in.GetIsNotId()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetIsDataScope() {
		dataScopeList := l.getUserDataScopeLogic.GetRoleDataScopeBuilder(
			in.GetDataScopeUserId(), "id", "create_user")
		for _, v := range dataScopeList {
			listWhereBuilder = listWhereBuilder.Where(v)
			countWhereBuilder = countWhereBuilder.Where(v)
		}
	}

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetRoleListLogic) getList(in *syspb.GetRoleListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetRoleListResp, error) {
	orderBy := "list_order,id asc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.SysRoleModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get role list"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysRole
	for _, v := range list {
		var data syspb.SysRole
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetRoleListResp{
		List: dataList,
	}, nil
}

func (l *GetRoleListLogic) getPage(in *syspb.GetRoleListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*syspb.GetRoleListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageSize = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "list_order,id asc"
	}

	// 分页查询
	list, err := l.svcCtx.SysRoleModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get role list"), err, in)
	}
	count, err := l.svcCtx.SysRoleModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get role count"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysRole
	for _, v := range list {
		var data syspb.SysRole
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetRoleListResp{
		List: dataList,
		Page: &syspb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetRoleListLogic) getCount(in *syspb.GetRoleListReq, whereBuilder squirrel.SelectBuilder) (*syspb.GetRoleListResp, error) {
	count, err := l.svcCtx.SysRoleModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get role count"), err, in)
	}

	return &syspb.GetRoleListResp{
		Page: &syspb.PageResp{
			Total: count,
		},
	}, nil
}
