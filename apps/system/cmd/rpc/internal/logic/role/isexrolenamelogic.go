package rolelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type IsExRoleNameLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	getRoleListLogic *GetRoleListLogic
}

func NewIsExRoleNameLogic(ctx context.Context, svcCtx *svc.ServiceContext) *IsExRoleNameLogic {
	return &IsExRoleNameLogic{
		ctx:              ctx,
		svcCtx:           svcCtx,
		Logger:           logx.WithContext(ctx),
		getRoleListLogic: NewGetRoleListLogic(ctx, svcCtx),
	}
}

func (l *IsExRoleNameLogic) IsExRoleName(in *syspb.IsExRoleNameReq) (*syspb.IsExRoleNameResp, error) {
	if in.GetName() == "" {
		return &syspb.IsExRoleNameResp{IsExist: true}, nil
	}

	// 条件
	params := &syspb.GetRoleListReq{
		Page: &syspb.PageReq{
			IsCount: true,
		},
		Name: in.GetName(),
	}
	if in.GetIsNotId() != 0 {
		params.IsNotId = in.GetIsNotId()
	}

	// 查询
	listResp, err := l.getRoleListLogic.GetRoleList(params)
	if err != nil {
		return nil, err
	}

	// 处理
	isExist := true
	if listResp.GetPage().GetTotal() == 0 {
		isExist = false
	}

	return &syspb.IsExRoleNameResp{IsExist: isExist}, nil
}
