package rolelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/duke-git/lancet/v2/slice"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type UpRoleLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	delRoleLogic      *DelRoleLogic
	addRoleLogic      *AddRoleLogic
	isExRoleNameLogic *IsExRoleNameLogic
}

func NewUpRoleLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpRoleLogic {
	return &UpRoleLogic{
		ctx:               ctx,
		svcCtx:            svcCtx,
		Logger:            logx.WithContext(ctx),
		delRoleLogic:      NewDelRoleLogic(ctx, svcCtx),
		addRoleLogic:      NewAddRoleLogic(ctx, svcCtx),
		isExRoleNameLogic: NewIsExRoleNameLogic(ctx, svcCtx),
	}
}

func (l *UpRoleLogic) UpRole(in *syspb.UpRoleReq) (*syspb.UpRoleResp, error) {
	role := in.GetParams()
	if role == nil || role.GetId() == 0 || role.GetName() == "" || role.GetUpdateUser() == "" {
		return nil, errors.Wrapf(xerr.NewErrMsg("更新角色失败：参数缺失"),
			tool.GetErrMsgFormat("update role"), "params is not existed", in)
	}

	// 校验参数
	enforcer := l.svcCtx.AdapterCasbin.SetCtxSession(l.ctx, nil).Enforcer

	// 判断是否是超级管理员角色
	rids := l.svcCtx.Config.SystemCustom.NotCheckAuthRoleIds
	if slice.Contain(rids, role.GetId()) {
		return nil, errors.Wrapf(xerr.NewErrMsg("更新角色："+role.GetName()+" 失败，不允许操作超级管理员角色"),
			tool.GetErrMsgFormat("update role"), "role id is admin", in)
	}

	// 如果已分配，则不允许禁用
	if role.GetStatus() == constant.RoleStatusOff {
		policies := enforcer.GetFilteredGroupingPolicy(1, convertor.ToString(role.GetId()))
		var userIds []int64
		for _, policy := range policies {
			val, _ := convertor.ToInt(policy[0])
			userIds = append(userIds, val)
		}
		if len(userIds) > 0 {
			return nil, errors.Wrapf(xerr.NewErrMsg("更新角色："+role.GetName()+" 失败，该角色已分配，不能禁用"),
				tool.GetErrMsgFormat("update role"), "role is assigned", in)
		}
	}

	// 校验名称是否重复
	isExistResp, err := l.isExRoleNameLogic.IsExRoleName(&syspb.IsExRoleNameReq{
		Name:    role.GetName(),
		IsNotId: role.GetId(),
	})
	if err != nil {
		return nil, err
	}
	if isExistResp.GetIsExist() {
		return nil, errors.Wrapf(xerr.NewErrMsg("更新角色："+role.GetName()+" 失败，角色名称已存在"),
			tool.GetErrMsgFormat("update role"), "role name is existed", role)
	}

	// 转换数据
	one, err := l.svcCtx.SysRoleModel.FindOne(l.ctx, role.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update role"), err, in)
	}
	one.Id = role.GetId()
	one.Name = role.GetName()
	one.ListOrder = role.GetListOrder()
	one.Status = role.GetStatus()
	one.Remark = role.GetRemark()
	one.DataScope = role.GetDataScope()
	one.UpdateUser = role.GetUpdateUser()

	// 更新
	if err = l.svcCtx.SysRoleModel.Trans(l.ctx, func(context context.Context, session sqlx.Session) error {
		e := l.svcCtx.SysRoleModel.UpdateEx(l.ctx, session, one)
		if e != nil {
			return e
		}

		// 设置用角色菜单关联
		e = l.addRoleLogic.HandleRoleRule(role.GetId(), in.GetMenuIds(), true, session)
		if e != nil {
			return e
		}

		return nil
	}); err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update role"), err, in)
	}

	return &syspb.UpRoleResp{}, nil
}
