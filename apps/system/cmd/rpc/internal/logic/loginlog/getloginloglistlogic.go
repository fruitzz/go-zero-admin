package loginloglogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetLoginLogListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetLoginLogListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetLoginLogListLogic {
	return &GetLoginLogListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetLoginLogListLogic) GetLoginLogList(in *syspb.GetLoginLogListReq) (*syspb.GetLoginLogListResp, error) {
	listWhereBuilder := l.svcCtx.SysLoginLogModel.RowBuilder()
	countWhereBuilder := l.svcCtx.SysLoginLogModel.CountBuilder("1")
	if in.GetLoginName() != "" {
		condition := squirrel.Like{"login_name": in.GetLoginName() + "%"}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetStatus() != "" {
		condition := squirrel.Eq{"status": in.GetStatus()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetIpaddr() != "" {
		condition := squirrel.Like{"ipaddr": in.GetIpaddr() + "%"}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetLoginLocation() != "" {
		condition := squirrel.Like{"login_location": in.GetLoginLocation() + "%"}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if len(in.GetPage().GetDateRange()) > 0 {
		condition := squirrel.And{squirrel.GtOrEq{"login_time": in.GetPage().GetDateRange()[0]},
			squirrel.LtOrEq{"login_time": in.GetPage().GetDateRange()[1]}}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetLoginLogListLogic) getList(in *syspb.GetLoginLogListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetLoginLogListResp, error) {
	orderBy := "info_id desc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.SysLoginLogModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get loginLog list"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysLoginLog
	for _, v := range list {
		var data syspb.SysLoginLog
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetLoginLogListResp{
		List: dataList,
	}, nil
}

func (l *GetLoginLogListLogic) getPage(in *syspb.GetLoginLogListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*syspb.GetLoginLogListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageSize = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "info_id desc"
	}

	// 数据查询
	list, err := l.svcCtx.SysLoginLogModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get loginLog list"), err, in)
	}
	count, err := l.svcCtx.SysLoginLogModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get loginLog count"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysLoginLog
	for _, v := range list {
		var data syspb.SysLoginLog
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetLoginLogListResp{
		List: dataList,
		Page: &syspb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetLoginLogListLogic) getCount(in *syspb.GetLoginLogListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetLoginLogListResp, error) {
	count, err := l.svcCtx.SysLoginLogModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get loginLog count"), err, in)
	}

	return &syspb.GetLoginLogListResp{
		Page: &syspb.PageResp{
			Total: count,
		},
	}, nil
}
