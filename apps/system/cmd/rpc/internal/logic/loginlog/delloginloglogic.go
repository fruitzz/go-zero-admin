package loginloglogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type DelLoginLogLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDelLoginLogLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DelLoginLogLogic {
	return &DelLoginLogLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DelLoginLogLogic) DelLoginLog(in *syspb.DelLoginLogReq) (*syspb.DelLoginLogResp, error) {
	if len(in.GetIds()) == 0 {
		return nil, errors.Wrapf(xerr.NewErrMsg("删除登陆日志失败：参数缺失"),
			tool.GetErrMsgFormat("delete loginLog"), "params is not existed", in)
	}

	// 删除数据
	err := l.svcCtx.SysLoginLogModel.DeleteEx(l.ctx, nil, in.GetIds(), true, "")
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("delete loginLog"), err, in)
	}

	return &syspb.DelLoginLogResp{}, nil
}
