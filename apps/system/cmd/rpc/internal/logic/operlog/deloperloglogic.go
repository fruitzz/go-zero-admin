package operloglogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type DelOperLogLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDelOperLogLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DelOperLogLogic {
	return &DelOperLogLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DelOperLogLogic) DelOperLog(in *syspb.DelOperLogReq) (*syspb.DelOperLogResp, error) {
	if len(in.GetIds()) == 0 {
		return nil, errors.Wrapf(xerr.NewErrMsg("删除操作日志失败：参数缺失"),
			tool.GetErrMsgFormat("delete operLog"), "params is not existed", in)
	}

	// 删除数据
	err := l.svcCtx.SysOperLogModel.DeleteEx(l.ctx, nil, in.GetIds(), true, "")
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("delete operLog"), err, in)
	}

	return &syspb.DelOperLogResp{}, nil
}
