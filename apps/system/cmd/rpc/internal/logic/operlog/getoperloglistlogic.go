package operloglogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetOperLogListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetOperLogListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetOperLogListLogic {
	return &GetOperLogListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetOperLogListLogic) GetOperLogList(in *syspb.GetOperLogListReq) (*syspb.GetOperLogListResp, error) {
	listWhereBuilder := l.svcCtx.SysOperLogModel.RowBuilder()
	countWhereBuilder := l.svcCtx.SysOperLogModel.CountBuilder("1")
	if len(in.GetPage().GetDateRange()) > 0 {
		condition := squirrel.And{squirrel.GtOrEq{"oper_time": in.GetPage().GetDateRange()[0]},
			squirrel.LtOrEq{"oper_time": in.GetPage().GetDateRange()[1]}}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetOperLogListLogic) getList(in *syspb.GetOperLogListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetOperLogListResp, error) {
	orderBy := "oper_id desc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.SysOperLogModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get operLog list"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysOperLog
	for _, v := range list {
		var data syspb.SysOperLog
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetOperLogListResp{
		List: dataList,
	}, nil
}

func (l *GetOperLogListLogic) getPage(in *syspb.GetOperLogListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*syspb.GetOperLogListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageSize = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "oper_id desc"
	}

	// 分页查询
	list, err := l.svcCtx.SysOperLogModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get operLog list"), err, in)
	}
	count, err := l.svcCtx.SysOperLogModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get operLog count"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysOperLog
	for _, v := range list {
		var data syspb.SysOperLog
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetOperLogListResp{
		List: dataList,
		Page: &syspb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetOperLogListLogic) getCount(in *syspb.GetOperLogListReq, whereBuilder squirrel.SelectBuilder) (*syspb.GetOperLogListResp, error) {
	count, err := l.svcCtx.SysOperLogModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get operLog count"), err, in)
	}

	return &syspb.GetOperLogListResp{
		Page: &syspb.PageResp{
			Total: count,
		},
	}, nil
}
