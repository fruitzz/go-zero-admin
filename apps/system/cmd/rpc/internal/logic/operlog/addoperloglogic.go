package operloglogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddOperLogLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAddOperLogLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AddOperLogLogic {
	return &AddOperLogLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *AddOperLogLogic) AddOperLog(in *syspb.AddOperLogReq) (*syspb.AddOperLogResp, error) {
	operLog := in.GetParams()
	if operLog == nil || operLog.GetTitle() == "" || operLog.GetMethod() == "" || operLog.GetRequestMethod() == "" ||
		operLog.GetOperName() == "" || operLog.GetCreateUser() == "" || operLog.GetUpdateUser() == "" {
		return nil, errors.Wrapf(xerr.NewErrMsg("新增操作日志失败：参数缺失"),
			tool.GetErrMsgFormat("insert operLog"), "params is not existed", in)
	}

	// 数据转换
	var data model.SysOperLog
	_ = copier.Copy(&data, in.Params)

	// 保存数据
	_, err := l.svcCtx.SysOperLogModel.InsertEx(l.ctx, nil, []*model.SysOperLog{&data})
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("insert operLog"), err, in)
	}

	return &syspb.AddOperLogResp{}, nil
}
