package roledeptlogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetRoleDeptListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetRoleDeptListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetRoleDeptListLogic {
	return &GetRoleDeptListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetRoleDeptListLogic) GetRoleDeptList(in *syspb.GetRoleDeptListReq) (*syspb.GetRoleDeptListResp, error) {
	listWhereBuilder := l.svcCtx.SysRoleDeptModel.RowBuilder()
	countWhereBuilder := l.svcCtx.SysRoleDeptModel.CountBuilder("1")
	if in.GetRoleId() != 0 {
		condition := squirrel.Eq{"role_id": in.GetRoleId()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetDeptId() != 0 {
		condition := squirrel.Eq{"dept_id": in.GetDeptId()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetRoleDeptListLogic) getList(in *syspb.GetRoleDeptListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetRoleDeptListResp, error) {
	orderBy := "id desc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.SysRoleDeptModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get roleDept list"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysRoleDept
	for _, v := range list {
		var data syspb.SysRoleDept
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetRoleDeptListResp{
		List: dataList,
	}, nil
}

func (l *GetRoleDeptListLogic) getPage(in *syspb.GetRoleDeptListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*syspb.GetRoleDeptListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageSize = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "id desc"
	}

	// 分页查询
	list, err := l.svcCtx.SysRoleDeptModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get roleDept list"), err, in)
	}
	count, err := l.svcCtx.SysRoleDeptModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get roleDept count"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysRoleDept
	for _, v := range list {
		var data syspb.SysRoleDept
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetRoleDeptListResp{
		List: dataList,
		Page: &syspb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetRoleDeptListLogic) getCount(in *syspb.GetRoleDeptListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetRoleDeptListResp, error) {
	count, err := l.svcCtx.SysRoleDeptModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get roleDept count"), err, in)
	}

	return &syspb.GetRoleDeptListResp{
		Page: &syspb.PageResp{
			Total: count,
		},
	}, nil
}
