package deptlogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/logic/userscope"
	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetDeptListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	getUserDataScopeLogic *userscopelogic.GetUserDataScopeLogic
}

func NewGetDeptListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetDeptListLogic {
	return &GetDeptListLogic{
		ctx:                   ctx,
		svcCtx:                svcCtx,
		Logger:                logx.WithContext(ctx),
		getUserDataScopeLogic: userscopelogic.NewGetUserDataScopeLogic(ctx, svcCtx),
	}
}

func (l *GetDeptListLogic) GetDeptList(in *syspb.GetDeptListReq) (*syspb.GetDeptListResp, error) {
	listWhereBuilder := l.svcCtx.SysDeptModel.RowBuilder()
	countWhereBuilder := l.svcCtx.SysDeptModel.CountBuilder("1")
	if in.GetDeptName() != "" {
		condition := squirrel.Eq{"dept_name": in.GetDeptName()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetParentId() != "" {
		condition := squirrel.Eq{"parent_id": in.GetParentId()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetStatus() != "" {
		condition := squirrel.Eq{"status": in.GetStatus()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetIsNotId() != 0 {
		condition := squirrel.NotEq{"dept_id": in.GetIsNotId()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetIsDataScope() {
		dataScopeList := l.getUserDataScopeLogic.GetDataScopeBuilder(in.GetDataScopeUserId(),
			"dept_id", "create_user")
		for _, v := range dataScopeList {
			listWhereBuilder = listWhereBuilder.Where(v)
			countWhereBuilder = countWhereBuilder.Where(v)
		}
	}

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetDeptListLogic) getList(in *syspb.GetDeptListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetDeptListResp, error) {
	orderBy := "order_num,dept_id asc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.SysDeptModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dept list"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysDept
	for _, v := range list {
		var data syspb.SysDept
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetDeptListResp{
		List: dataList,
	}, nil
}

func (l *GetDeptListLogic) getPage(in *syspb.GetDeptListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*syspb.GetDeptListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageSize = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "order_num,dept_id asc"
	}

	// 分页查询
	list, err := l.svcCtx.SysDeptModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dept list"), err, in)
	}
	count, err := l.svcCtx.SysDeptModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dept count"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysDept
	for _, v := range list {
		var data syspb.SysDept
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetDeptListResp{
		List: dataList,
		Page: &syspb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetDeptListLogic) getCount(in *syspb.GetDeptListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetDeptListResp, error) {
	count, err := l.svcCtx.SysDeptModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get dept count"), err, in)
	}

	return &syspb.GetDeptListResp{
		Page: &syspb.PageResp{
			Total: count,
		},
	}, nil
}
