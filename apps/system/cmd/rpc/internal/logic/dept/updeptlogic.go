package deptlogic

import (
	"context"
	"strings"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/duke-git/lancet/v2/convertor"
	"github.com/duke-git/lancet/v2/strutil"
	"github.com/duke-git/lancet/v2/validator"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/fx"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type UpDeptLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	isExDeptNameLogic    *IsExDeptNameLogic
	getDeptListLogic     *GetDeptListLogic
	getDeptSonByPidLogic *GetDeptSonByPidLogic
}

func NewUpDeptLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpDeptLogic {
	return &UpDeptLogic{
		ctx:                  ctx,
		svcCtx:               svcCtx,
		Logger:               logx.WithContext(ctx),
		isExDeptNameLogic:    NewIsExDeptNameLogic(ctx, svcCtx),
		getDeptListLogic:     NewGetDeptListLogic(ctx, svcCtx),
		getDeptSonByPidLogic: NewGetDeptSonByPidLogic(ctx, svcCtx),
	}
}

func (l *UpDeptLogic) UpDept(in *syspb.UpDeptReq) (*syspb.UpDeptResp, error) {
	// 校验参数
	if err := l.validParams(in); err != nil {
		return nil, err
	}

	// 转换数据
	newDept, oldDept, err := l.convertData(in)
	if err != nil {
		return nil, err
	}

	// 修改部门信息
	// 判断用户是否修改了该部门的父类信息
	var deptChildren []*model.SysDept
	if oldDept.ParentId != newDept.ParentId {
		// 可忽略err，直接判断是否存在值
		var newAncestors string
		newParentDept, _ := l.svcCtx.SysDeptModel.FindOne(l.ctx, newDept.ParentId)
		if newParentDept != nil {
			newAncestors = convertor.ToString(newParentDept.Ancestors) + "," + convertor.ToString(newParentDept.DeptId)
		} else {
			newAncestors = convertor.ToString(constant.DeptFirstPid)
		}

		newDept.Ancestors = newAncestors
		// 更新子类的ancestors
		deptChildren, err = l.updateDeptChildren(newDept.DeptId, newAncestors, oldDept.Ancestors, newDept.UpdateUser)
		if err != nil {
			return nil, err
		}
	}
	deptChildren = append(deptChildren, newDept)

	// 更新数据
	if err = l.svcCtx.SysDeptModel.Trans(l.ctx, func(context context.Context, session sqlx.Session) error {
		for _, v := range deptChildren {
			e := l.svcCtx.SysDeptModel.UpdateEx(l.ctx, session, v)
			if e != nil {
				return e
			}
		}

		// 如果该部门是启用状态，则启用该部门的所有上级部门
		if newDept.Status == constant.DeptStatusOn && newDept.Ancestors != "" &&
			newDept.Ancestors != convertor.ToString(constant.DeptFirstPid) {
			// 没有上级部门
			if strings.Index(newDept.Ancestors, ",") == -1 {
				return nil
			}

			// 处理上级部门id
			deptParentIds := strutil.SplitEx(newDept.Ancestors, ",", true)
			if len(deptParentIds) == 0 {
				return nil
			}

			for _, v := range deptParentIds {
				deptParentId, _ := convertor.ToInt(v)
				parentDept, _ := l.svcCtx.SysDeptModel.FindOne(l.ctx, deptParentId)
				if parentDept == nil {
					continue
				}
				parentDept.Status = constant.DeptStatusOn
				e := l.svcCtx.SysDeptModel.UpdateEx(l.ctx, session, parentDept)
				if e != nil {
					return e
				}
			}
		}

		return nil
	}); err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update dept"), err, in)
	}

	return &syspb.UpDeptResp{}, nil
}

// 更新子类的数据
func (l *UpDeptLogic) updateDeptChildren(deptId int64, newAncestors string, oldAncestors string,
	updateUser string) ([]*model.SysDept, error) {
	listResp, err := l.getDeptListLogic.GetDeptList(&syspb.GetDeptListReq{})
	if err != nil {
		return nil, err
	}
	if len(listResp.GetList()) == 0 {
		return nil, nil
	}

	// 根据父类id获取其下所有数据
	deptSonsResp, err := l.getDeptSonByPidLogic.GetDeptSonByPid(&syspb.GetDeptSonByPidReq{
		Pid:  deptId,
		List: listResp.GetList(),
	})
	if err != nil {
		return nil, err
	}
	deptSons := deptSonsResp.GetList()
	if len(deptSons) == 0 {
		return nil, nil
	}

	// 处理数据
	var newDeptSons []*model.SysDept
	fx.From(func(source chan<- interface{}) {
		for _, v := range deptSons {
			source <- v
		}
	}).Map(func(item interface{}) interface{} {
		deptSon := new(model.SysDept)
		_ = copier.Copy(deptSon, item.(*syspb.SysDept))
		deptSon.Ancestors = strings.Replace(deptSon.Ancestors, oldAncestors, newAncestors, 1)
		deptSon.UpdateUser = updateUser
		return deptSon
	}).ForEach(func(item interface{}) {
		newDeptSons = append(newDeptSons, item.(*model.SysDept))
	})

	return newDeptSons, nil
}

// 校验参数
func (l *UpDeptLogic) validParams(in *syspb.UpDeptReq) error {
	dept := in.GetParams()
	if dept == nil || dept.GetDeptId() == 0 || dept.GetDeptName() == "" || dept.GetUpdateUser() == "" {
		return errors.Wrapf(xerr.NewErrMsg("更新部门失败：参数缺失"),
			tool.GetErrMsgFormat("update dept"), "params is not existed", in)
	}
	if dept.GetPhone() != "" && !validator.IsChineseMobile(dept.GetPhone()) {
		return errors.Wrapf(xerr.NewErrMsg("手机号数据错误"),
			tool.GetErrMsgFormat("insert dept"), "phone params error", in)
	}
	if dept.GetEmail() != "" && !validator.IsEmail(dept.GetEmail()) {
		return errors.Wrapf(xerr.NewErrMsg("邮箱数据错误"),
			tool.GetErrMsgFormat("insert dept"), "email params error", in)
	}

	// 判断部门自己不能成为自己的上级部门
	if dept.GetDeptId() == dept.GetParentId() {
		return errors.Wrapf(xerr.NewErrMsg("更新部门："+dept.GetDeptName()+" 失败，上级部门不能是自己"),
			tool.GetErrMsgFormat("update dept"), "dept parentId is myself", in)
	}

	// 判断用户同部门下是否有相同的名称
	isExist, err := l.isExDeptNameLogic.IsExDeptName(&syspb.IsExDeptNameReq{
		DeptName: dept.GetDeptName(),
		ParentId: dept.GetParentId(),
		IsNotId:  dept.GetDeptId(),
	})
	if err != nil {
		return err
	}
	if isExist.GetIsExist() {
		return errors.Wrapf(xerr.NewErrMsg("更新部门："+dept.GetDeptName()+" 失败，部门名称已存在"),
			tool.GetErrMsgFormat("update dept"), "dept name existed", in)
	}

	// 如果部门处于开启状态，则不用判断状态
	if dept.Status == constant.DeptStatusOn {
		return nil
	}

	// 部门存在用户，不允许禁用
	// 不能直接调用user包里的逻辑，避免循环依赖
	count, err := l.svcCtx.SysDeptModel.FindCount(l.ctx, l.svcCtx.SysUserModel.CountBuilder("1").
		Where(squirrel.Eq{"dept_id": dept.GetDeptId()}))
	if err != nil || count > 0 {
		return errors.Wrapf(xerr.NewErrMsg("更新部门："+dept.GetDeptName()+" 失败，部门存在用户，不允许禁用"),
			tool.GetErrMsgFormat("update dept"), "dept user is existed", in)
	}

	// 判断如果修改该部门为停用状态，则其子部门也需要修改为停用状态
	deptStatusListResp, err := l.getDeptListLogic.GetDeptList(&syspb.GetDeptListReq{
		Page: &syspb.PageReq{
			IsCount: true,
		},
		Status:   convertor.ToString(constant.DeptStatusOn),
		ParentId: convertor.ToString(dept.GetDeptId()),
	})
	if err != nil {
		return err
	}
	if deptStatusListResp.GetPage().GetTotal() > 0 {
		return errors.Wrapf(xerr.NewErrMsg("更新部门："+dept.GetDeptName()+" 失败，该部门包含未停用的子部门"),
			tool.GetErrMsgFormat("update dept"), "dept children is not stop", in)
	}

	return nil
}

// 转换数据
func (l *UpDeptLogic) convertData(in *syspb.UpDeptReq) (*model.SysDept, *model.SysDept, error) {
	dept := in.GetParams()
	one, err := l.svcCtx.SysDeptModel.FindOne(l.ctx, dept.GetDeptId())
	if err != nil {
		return nil, nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update dept"), err, in)
	}

	// 复制老数据
	oldData := new(model.SysDept)
	_ = copier.Copy(oldData, one)

	// 处理新数据
	one.ParentId = dept.GetParentId()
	one.DeptName = dept.GetDeptName()
	one.OrderNum = dept.GetOrderNum()
	one.Leader = dept.GetLeader()
	one.Phone = dept.GetPhone()
	one.Email = dept.GetEmail()
	one.Status = dept.GetStatus()
	one.UpdateUser = dept.GetUpdateUser()

	return one, oldData, nil
}
