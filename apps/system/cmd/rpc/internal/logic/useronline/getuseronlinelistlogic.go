package useronlinelogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetUserOnlineListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetUserOnlineListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetUserOnlineListLogic {
	return &GetUserOnlineListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetUserOnlineListLogic) GetUserOnlineList(in *syspb.GetUserOnlineListReq) (*syspb.GetUserOnlineListResp, error) {
	// 查询条件
	listWhereBuilder := l.svcCtx.SysUserOnlineModel.RowBuilder()
	countWhereBuilder := l.svcCtx.SysUserOnlineModel.CountBuilder("1")

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetUserOnlineListLogic) getList(in *syspb.GetUserOnlineListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetUserOnlineListResp, error) {
	orderBy := "id desc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.SysUserOnlineModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get userOnline list"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysUserOnline
	for _, v := range list {
		var data syspb.SysUserOnline
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetUserOnlineListResp{
		List: dataList,
	}, nil
}

func (l *GetUserOnlineListLogic) getPage(in *syspb.GetUserOnlineListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*syspb.GetUserOnlineListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageNum = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "id desc"
	}

	// 分页查询
	list, err := l.svcCtx.SysUserOnlineModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get userOnline list"), err, in)
	}
	count, err := l.svcCtx.SysUserOnlineModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get userOnline count"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysUserOnline
	for _, v := range list {
		var data syspb.SysUserOnline
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetUserOnlineListResp{
		List: dataList,
		Page: &syspb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetUserOnlineListLogic) getCount(in *syspb.GetUserOnlineListReq, whereBuilder squirrel.SelectBuilder) (*syspb.GetUserOnlineListResp, error) {
	count, err := l.svcCtx.SysUserOnlineModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get userOnline count"), err, in)
	}

	return &syspb.GetUserOnlineListResp{
		Page: &syspb.PageResp{
			Total: count,
		},
	}, nil
}
