package configlogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type IsExConfigKeyLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	configList *GetConfigListLogic
}

func NewIsExConfigKeyLogic(ctx context.Context, svcCtx *svc.ServiceContext) *IsExConfigKeyLogic {
	return &IsExConfigKeyLogic{
		ctx:        ctx,
		svcCtx:     svcCtx,
		Logger:     logx.WithContext(ctx),
		configList: NewGetConfigListLogic(ctx, svcCtx),
	}
}

func (l *IsExConfigKeyLogic) IsExConfigKey(in *syspb.IsExConfigKeyReq) (*syspb.IsExConfigKeyResp, error) {
	if in.GetConfigKey() == "" {
		return &syspb.IsExConfigKeyResp{IsExist: true}, nil
	}

	// 查询条件
	params := &syspb.GetConfigListReq{
		Page: &syspb.PageReq{
			IsCount: true,
		},
		ConfigKey: in.GetConfigKey(),
	}
	if in.GetIsNotId() != 0 {
		params.IsNotId = in.GetIsNotId()
	}

	// 查询数据
	listResp, err := l.configList.GetConfigList(params)
	if err != nil {
		return nil, err
	}

	// 处理数据
	isExist := false
	if listResp.GetPage().Total > 0 {
		isExist = true
	}

	return &syspb.IsExConfigKeyResp{IsExist: isExist}, nil
}
