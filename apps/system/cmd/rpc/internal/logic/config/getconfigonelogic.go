package configlogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetConfigOneLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetConfigOneLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetConfigOneLogic {
	return &GetConfigOneLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetConfigOneLogic) GetConfigOne(in *syspb.GetConfigOneReq) (*syspb.GetConfigOneResp, error) {
	one, err := l.svcCtx.SysConfigModel.FindOne(l.ctx, in.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("get config one"), err, in)
	}

	// 转换数据
	var data syspb.SysConfig
	_ = copier.Copy(&data, one)

	return &syspb.GetConfigOneResp{One: &data}, nil
}
