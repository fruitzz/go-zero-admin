package configlogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddConfigLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	isExConfigKeyLogic *IsExConfigKeyLogic
}

func NewAddConfigLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AddConfigLogic {
	return &AddConfigLogic{
		ctx:                ctx,
		svcCtx:             svcCtx,
		Logger:             logx.WithContext(ctx),
		isExConfigKeyLogic: NewIsExConfigKeyLogic(ctx, svcCtx),
	}
}

func (l *AddConfigLogic) AddConfig(in *syspb.AddConfigReq) (*syspb.AddConfigResp, error) {
	config := in.GetParams()
	if config == nil || config.GetConfigName() == "" || config.GetConfigKey() == "" ||
		config.GetConfigValue() == "" || config.GetCreateUser() == "" || config.GetUpdateUser() == "" {
		return nil, errors.Wrapf(xerr.NewErrMsg("新增参数配置失败：参数缺失"),
			tool.GetErrMsgFormat("insert config"), "params is not existed", in)
	}

	// 校验参数键名重复
	resp, err := l.isExConfigKeyLogic.IsExConfigKey(&syspb.IsExConfigKeyReq{
		ConfigKey: config.GetConfigKey(),
	})
	if err != nil {
		return nil, err
	}
	if resp.GetIsExist() {
		return nil, errors.Wrapf(xerr.NewErrMsg("新增参数配置失败：参数键名重复"),
			tool.GetErrMsgFormat("insert config"), "config key is existed", in)
	}

	// 转换数据
	var data model.SysConfig
	_ = copier.Copy(&data, config)

	// 保存数据
	_, err = l.svcCtx.SysConfigModel.InsertEx(l.ctx, nil, []*model.SysConfig{&data})
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("insert config"), err, in)
	}

	return &syspb.AddConfigResp{}, nil
}
