package configlogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetConfigListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetConfigListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetConfigListLogic {
	return &GetConfigListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetConfigListLogic) GetConfigList(in *syspb.GetConfigListReq) (*syspb.GetConfigListResp, error) {
	listWhereBuilder := l.svcCtx.SysConfigModel.RowBuilder()
	countWhereBuilder := l.svcCtx.SysConfigModel.CountBuilder("1")
	if in.GetConfigName() != "" {
		condition := squirrel.Like{"config_name": in.GetConfigName() + "%"}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetConfigType() != "" {
		condition := squirrel.Eq{"config_type": in.GetConfigType()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetConfigKey() != "" {
		condition := squirrel.Eq{"config_key": in.GetConfigKey()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if len(in.GetPage().GetDateRange()) > 0 {
		condition := squirrel.And{squirrel.GtOrEq{"create_time": in.GetPage().GetDateRange()[0]},
			squirrel.LtOrEq{"create_time": in.GetPage().GetDateRange()[1]}}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetIsNotId() != 0 {
		condition := squirrel.NotEq{"config_id": in.GetIsNotId()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetConfigListLogic) getList(in *syspb.GetConfigListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetConfigListResp, error) {
	orderBy := "config_id desc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.SysConfigModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get config list"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysConfig
	for _, v := range list {
		var data syspb.SysConfig
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetConfigListResp{
		List: dataList,
	}, nil
}

func (l *GetConfigListLogic) getPage(in *syspb.GetConfigListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*syspb.GetConfigListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageSize = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "config_id desc"
	}

	// 分页查询
	list, err := l.svcCtx.SysConfigModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get config list"), err, in)
	}
	count, err := l.svcCtx.SysConfigModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get config count"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysConfig
	for _, v := range list {
		var data syspb.SysConfig
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetConfigListResp{
		List: dataList,
		Page: &syspb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetConfigListLogic) getCount(in *syspb.GetConfigListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetConfigListResp, error) {
	count, err := l.svcCtx.SysConfigModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get config count"), err, in)
	}

	return &syspb.GetConfigListResp{
		Page: &syspb.PageResp{
			Total: count,
		},
	}, nil
}
