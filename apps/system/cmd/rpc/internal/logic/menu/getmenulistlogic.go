package menulogic

import (
	"context"
	"strings"

	"go-zero-admin/apps/system/cmd/rpc/internal/logic/userscope"
	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetMenuListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	getUserDataScopeLogic *userscopelogic.GetUserDataScopeLogic
}

func NewGetMenuListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetMenuListLogic {
	return &GetMenuListLogic{
		ctx:                   ctx,
		svcCtx:                svcCtx,
		Logger:                logx.WithContext(ctx),
		getUserDataScopeLogic: userscopelogic.NewGetUserDataScopeLogic(ctx, svcCtx),
	}
}
func (l *GetMenuListLogic) GetMenuList(in *syspb.GetMenuListReq) (*syspb.GetMenuListResp, error) {
	listWhereBuilder := l.svcCtx.SysMenuModel.RowBuilder()
	countWhereBuilder := l.svcCtx.SysMenuModel.CountBuilder("1")
	if in.GetName() != "" {
		condition := squirrel.Eq{"name": in.GetName()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetTitle() != "" {
		condition := squirrel.Like{"title": in.GetTitle() + "%"}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetComponent() != "" {
		condition := squirrel.Like{"component": in.GetComponent() + "%"}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetMenuType() != "" {
		condition := squirrel.Eq{"menu_type": strings.Split(in.GetMenuType(), ",")}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetIsNotId() != 0 {
		condition := squirrel.NotEq{"id": in.GetIsNotId()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetIsDataScope() {
		dataScopeList := l.getUserDataScopeLogic.GetMenuDataScopeBuilder(in.GetDataScopeUserId(),
			"id", "create_user")
		for _, v := range dataScopeList {
			listWhereBuilder = listWhereBuilder.Where(v)
			countWhereBuilder = countWhereBuilder.Where(v)
		}
	}

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetMenuListLogic) getList(in *syspb.GetMenuListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetMenuListResp, error) {
	orderBy := "menu_sort,id asc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.SysMenuModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get menu list"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysMenu
	for _, v := range list {
		var data syspb.SysMenu
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetMenuListResp{
		List: dataList,
	}, nil
}

func (l *GetMenuListLogic) getPage(in *syspb.GetMenuListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*syspb.GetMenuListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageSize = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "menu_sort,id asc"
	}

	// 分页查询
	list, err := l.svcCtx.SysMenuModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get menu list"), err, in)
	}
	count, err := l.svcCtx.SysMenuModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get menu count"), err, in)
	}

	// 数据转换
	var dataList []*syspb.SysMenu
	for _, v := range list {
		var data syspb.SysMenu
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &syspb.GetMenuListResp{
		List: dataList,
		Page: &syspb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetMenuListLogic) getCount(in *syspb.GetMenuListReq,
	whereBuilder squirrel.SelectBuilder) (*syspb.GetMenuListResp, error) {
	count, err := l.svcCtx.SysMenuModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get menu count"), err, in)
	}

	return &syspb.GetMenuListResp{
		Page: &syspb.PageResp{
			Total: count,
		},
	}, nil
}
