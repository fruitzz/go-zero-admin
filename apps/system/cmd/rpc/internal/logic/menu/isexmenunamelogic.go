package menulogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"

	"github.com/zeromicro/go-zero/core/logx"
)

type IsExMenuNameLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	getMenuListLogic *GetMenuListLogic
}

func NewIsExMenuNameLogic(ctx context.Context, svcCtx *svc.ServiceContext) *IsExMenuNameLogic {
	return &IsExMenuNameLogic{
		ctx:              ctx,
		svcCtx:           svcCtx,
		Logger:           logx.WithContext(ctx),
		getMenuListLogic: NewGetMenuListLogic(ctx, svcCtx),
	}
}

func (l *IsExMenuNameLogic) IsExMenuName(in *syspb.IsExMenuNameReq) (*syspb.IsExMenuNameResp, error) {
	if in.GetName() == "" {
		return &syspb.IsExMenuNameResp{IsExist: true}, nil
	}

	// 查询条件
	params := &syspb.GetMenuListReq{
		Page: &syspb.PageReq{
			IsCount: true,
		},
		Name: in.GetName(),
	}
	if in.GetIsNotId() != 0 {
		params.IsNotId = in.GetIsNotId()
	}

	// 查询数据
	listResp, err := l.getMenuListLogic.GetMenuList(params)
	if err != nil {
		return nil, err
	}

	// 处理数据
	isExist := true
	if listResp.GetPage().GetTotal() == 0 {
		isExist = false
	}

	return &syspb.IsExMenuNameResp{IsExist: isExist}, nil
}
