package menulogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/duke-git/lancet/v2/validator"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type UpMenuLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	isExMenuNameLogic *IsExMenuNameLogic
	addMenuLogic      *AddMenuLogic
}

func NewUpMenuLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpMenuLogic {
	return &UpMenuLogic{
		ctx:               ctx,
		svcCtx:            svcCtx,
		Logger:            logx.WithContext(ctx),
		isExMenuNameLogic: NewIsExMenuNameLogic(ctx, svcCtx),
		addMenuLogic:      NewAddMenuLogic(ctx, svcCtx),
	}
}

func (l *UpMenuLogic) UpMenu(in *syspb.UpMenuReq) (*syspb.UpMenuResp, error) {
	// 校验参数
	if err := l.validParams(in); err != nil {
		return nil, err
	}

	// 转换数据
	newMenu, err := l.convertData(in)
	if err != nil {
		return nil, err
	}

	// 更新数据
	if err = l.svcCtx.SysMenuModel.Trans(l.ctx, func(context context.Context, session sqlx.Session) error {
		e := l.svcCtx.SysMenuModel.UpdateEx(l.ctx, session, newMenu)
		if e != nil {
			return e
		}

		// 处理菜单角色的关系
		e = l.addMenuLogic.HandleRoleRule(newMenu.Id, in.GetRoleIds(), true, session)
		if e != nil {
			return e
		}

		return nil
	}); err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update menu"), err, in)
	}

	return &syspb.UpMenuResp{}, nil
}

// 校验参数
func (l *UpMenuLogic) validParams(in *syspb.UpMenuReq) error {
	menu := in.GetParams()
	if menu == nil || menu.GetId() == 0 || menu.GetName() == "" || menu.GetTitle() == "" ||
		((menu.GetMenuType() == constant.MenuTypeList || menu.GetMenuType() == constant.MenuTypeMenu) &&
			(menu.GetPath() == "" || menu.GetComponent() == "")) || menu.GetUpdateUser() == "" {
		return errors.Wrapf(xerr.NewErrMsg("更新菜单失败：参数缺失"),
			tool.GetErrMsgFormat("update menu"), "params is not existed", in)
	}

	// 查询数据
	isExistResp, err := l.isExMenuNameLogic.IsExMenuName(&syspb.IsExMenuNameReq{
		Name:    menu.GetName(),
		IsNotId: menu.GetId(),
	})
	if err != nil {
		return err
	}

	// 判断是否存在
	if isExistResp.GetIsExist() {
		return errors.Wrapf(xerr.NewErrMsg("更新菜单："+menu.GetTitle()+" 失败，接口规则已经存在"),
			tool.GetErrMsgFormat("update menu"), "menu name is existed", menu)
	}

	// 如果该菜单是iframe而且是链接的话，判断链接地址合法性
	if (menu.GetIsIframe() == 1 || menu.GetIsLink() == 1) && !validator.IsUrl(menu.GetLinkUrl()) {
		return errors.Wrapf(xerr.NewErrMsg("更新菜单："+menu.GetTitle()+" 失败，链接地址非法"),
			tool.GetErrMsgFormat("update menu"), "menu link url is illegal", menu)
	}

	// 判断菜单自己不能成为自己的上级部门
	if menu.GetId() == menu.GetPid() {
		return errors.Wrapf(xerr.NewErrMsg("更新菜单："+menu.GetTitle()+" 失败，上级菜单不能是自己"),
			tool.GetErrMsgFormat("update menu"), "dept parentId is myself", menu)
	}

	return nil
}

// 转换数据
func (l *UpMenuLogic) convertData(in *syspb.UpMenuReq) (*model.SysMenu, error) {
	menu := in.GetParams()
	one, err := l.svcCtx.SysMenuModel.FindOne(l.ctx, menu.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update menu"), err, in)
	}

	// 处理新数据
	one.Id = menu.GetId()
	one.Pid = menu.GetPid()
	one.Name = menu.GetName()
	one.Title = menu.GetTitle()
	one.Icon = menu.GetIcon()
	one.Condition = menu.GetCondition()
	one.Remark = menu.GetRemark()
	one.MenuType = menu.GetMenuType()
	one.MenuSort = menu.GetMenuSort()
	one.IsHide = menu.GetIsHide()
	one.Path = menu.GetPath()
	one.Component = menu.GetComponent()
	one.IsLink = menu.GetIsLink()
	one.IsIframe = menu.GetIsIframe()
	one.IsKeepAlive = menu.GetIsKeepAlive()
	one.Redirect = menu.GetRedirect()
	one.IsAffix = menu.GetIsAffix()
	one.LinkUrl = menu.GetLinkUrl()
	one.UpdateUser = menu.GetUpdateUser()

	return one, nil
}
