package menulogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetMenuOneLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetMenuOneLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetMenuOneLogic {
	return &GetMenuOneLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetMenuOneLogic) GetMenuOne(in *syspb.GetMenuOneReq) (*syspb.GetMenuOneResp, error) {
	one, err := l.svcCtx.SysMenuModel.FindOne(l.ctx, in.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("get menu one"), err, in)
	}

	// 数据转换
	var data syspb.SysMenu
	_ = copier.Copy(&data, one)

	return &syspb.GetMenuOneResp{One: &data}, nil
}
