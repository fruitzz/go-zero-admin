package menulogic

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/apps/system/model"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/duke-git/lancet/v2/validator"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type AddMenuLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
	isExMenuNameLogic *IsExMenuNameLogic
}

func NewAddMenuLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AddMenuLogic {
	return &AddMenuLogic{
		ctx:               ctx,
		svcCtx:            svcCtx,
		Logger:            logx.WithContext(ctx),
		isExMenuNameLogic: NewIsExMenuNameLogic(ctx, svcCtx),
	}
}

func (l *AddMenuLogic) AddMenu(in *syspb.AddMenuReq) (*syspb.AddMenuResp, error) {
	menu := in.GetParams()
	if menu == nil || menu.GetName() == "" || menu.GetTitle() == "" ||
		((menu.GetMenuType() == constant.MenuTypeList || menu.GetMenuType() == constant.MenuTypeMenu) &&
			(menu.GetPath() == "" || menu.GetComponent() == "")) ||
		menu.GetCreateUser() == "" || menu.GetUpdateUser() == "" {
		return nil, errors.Wrapf(xerr.NewErrMsg("新增菜单失败：参数缺失"),
			tool.GetErrMsgFormat("insert menu"), "params is not existed", in)
	}

	// 校验参数
	isExistResp, err := l.isExMenuNameLogic.IsExMenuName(&syspb.IsExMenuNameReq{
		Name: menu.GetName(),
	})
	if err != nil {
		return nil, err
	}
	if isExistResp.GetIsExist() {
		return nil, errors.Wrapf(xerr.NewErrMsg("新增菜单："+menu.GetTitle()+" 失败，接口规则已经存在"),
			tool.GetErrMsgFormat("insert menu"), "menu name is existed", menu)
	}
	// 如果该菜单是iframe而且是链接的话，判断链接地址合法性
	if (menu.GetIsIframe() == 1 || menu.GetIsLink() == 1) && !validator.IsUrl(menu.GetLinkUrl()) {
		return nil, errors.Wrapf(xerr.NewErrMsg("新增菜单："+menu.GetTitle()+" 失败，链接地址非法"),
			tool.GetErrMsgFormat("insert menu"), "menu link url is illegal", menu)
	}

	// 数据转换
	var entities model.SysMenu
	_ = copier.Copy(&entities, menu)

	// 保存数据
	if err = l.svcCtx.SysMenuModel.Trans(l.ctx, func(context context.Context, session sqlx.Session) error {
		r, e := l.svcCtx.SysMenuModel.InsertEx(l.ctx, session, []*model.SysMenu{&entities})
		if e != nil {
			return e
		}
		menuId := r[0]

		// 处理菜单角色权限
		e = l.HandleRoleRule(menuId, in.GetRoleIds(), false, session)
		if e != nil {
			return e
		}

		return nil
	}); err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("insert menu"), err, in)
	}

	return &syspb.AddMenuResp{}, nil
}

// HandleRoleRule 处理菜单角色权限
func (l *AddMenuLogic) HandleRoleRule(ruleId int64, roleIds []int64, isUp bool, session sqlx.Session) (err error) {
	enforcer := l.svcCtx.AdapterCasbin.SetCtxSession(l.ctx, session).Enforcer

	// 更新删除旧的菜单和角色关联
	if isUp {
		_, err = enforcer.RemoveFilteredPolicy(1, convertor.ToString(ruleId))
		if err != nil {
			return err
		}
	}

	// 新增菜单角色关联
	if len(roleIds) > 0 {
		for _, roleId := range roleIds {
			_, err = enforcer.AddPolicy(convertor.ToString(roleId), convertor.ToString(ruleId), "All")
			if err != nil {
				return err
			}
		}
	}

	return
}
