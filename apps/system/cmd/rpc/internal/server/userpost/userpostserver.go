// Code generated by goctl. DO NOT EDIT.
// Source: system.proto

package server

import (
	"context"

	"go-zero-admin/apps/system/cmd/rpc/internal/logic/userpost"
	"go-zero-admin/apps/system/cmd/rpc/internal/svc"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
)

type UserPostServer struct {
	svcCtx *svc.ServiceContext
	syspb.UnimplementedUserPostServer
}

func NewUserPostServer(svcCtx *svc.ServiceContext) *UserPostServer {
	return &UserPostServer{
		svcCtx: svcCtx,
	}
}

func (s *UserPostServer) GetUserPostList(ctx context.Context, in *syspb.GetUserPostListReq) (*syspb.GetUserPostListResp, error) {
	l := userpostlogic.NewGetUserPostListLogic(ctx, s.svcCtx)
	return l.GetUserPostList(in)
}
