package config

import (
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/zrpc"
)

type Config struct {
	zrpc.RpcServerConf
	JwtAuth struct {
		AccessSecret string
		AccessExpire int64
	}
	DB struct {
		DataSource string
	}
	ModelCache   cache.CacheConf
	SystemCustom struct {
		NotCheckAuthUserIds []int64
		NotCheckAuthRoleIds []int64
		CasBinFile          struct {
			ModelFile  string
			PolicyFile string
		}
	}
}
