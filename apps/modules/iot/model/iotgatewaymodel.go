package model

import (
	"context"
	"database/sql"

	"go-zero-admin/pkg/constant"

	"github.com/Masterminds/squirrel"
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/sqlc"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

var _ IotGatewayModel = (*customIotGatewayModel)(nil)

type (
	// IotGatewayModel is an interface to be customized, add more methods here,
	// and implement the added methods in customIotGatewayModel.
	IotGatewayModel interface {
		iotGatewayModel
		Trans(ctx context.Context, fn func(context context.Context, session sqlx.Session) error) error
		RowBuilder() squirrel.SelectBuilder
		CountBuilder(field string) squirrel.SelectBuilder
		SumBuilder(field string) squirrel.SelectBuilder
		FindOneByQuery(ctx context.Context, rowBuilder squirrel.SelectBuilder) (*IotGateway, error)
		FindSum(ctx context.Context, sumBuilder squirrel.SelectBuilder) (float64, error)
		FindCount(ctx context.Context, countBuilder squirrel.SelectBuilder) (int64, error)
		FindAll(ctx context.Context, rowBuilder squirrel.SelectBuilder, orderBy string) ([]*IotGateway, error)
		FindPageListByPage(ctx context.Context, rowBuilder squirrel.SelectBuilder, page, pageSize int64, orderBy string) ([]*IotGateway, error)
		InsertEx(ctx context.Context, session sqlx.Session, dataList []*IotGateway) ([]int64, error)
		DeleteEx(ctx context.Context, session sqlx.Session, ids []int64, isDel bool, userId string) error
		UpdateEx(ctx context.Context, session sqlx.Session, data *IotGateway) error
	}

	customIotGatewayModel struct {
		*defaultIotGatewayModel
	}
)

// NewIotGatewayModel returns a model for the database table.
func NewIotGatewayModel(conn sqlx.SqlConn, c cache.CacheConf) IotGatewayModel {
	return &customIotGatewayModel{
		defaultIotGatewayModel: newIotGatewayModel(conn, c),
	}
}

func (m *defaultIotGatewayModel) Trans(ctx context.Context, fn func(ctx context.Context, session sqlx.Session) error) error {
	return m.TransactCtx(ctx, func(ctx context.Context, session sqlx.Session) error {
		return fn(ctx, session)
	})
}

func (m *defaultIotGatewayModel) RowBuilder() squirrel.SelectBuilder {
	return squirrel.Select(iotGatewayRows).From(m.table)
}

func (m *defaultIotGatewayModel) CountBuilder(field string) squirrel.SelectBuilder {
	return squirrel.Select("COUNT(" + field + ")").From(m.table)
}

func (m *defaultIotGatewayModel) SumBuilder(field string) squirrel.SelectBuilder {
	return squirrel.Select("IFNULL(SUM(" + field + "), 0)").From(m.table)
}

func (m *defaultIotGatewayModel) FindOneByQuery(ctx context.Context, rowBuilder squirrel.SelectBuilder) (*IotGateway, error) {
	query, values, err := rowBuilder.Where(squirrel.Eq{"del_state": constant.DelStateNo}).ToSql()
	if err != nil {
		return nil, err
	}

	var resp IotGateway
	err = m.QueryRowNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return &resp, nil
	case sqlc.ErrNotFound:
		return nil, ErrNotFound
	default:
		return nil, err
	}
}

func (m *defaultIotGatewayModel) FindSum(ctx context.Context, sumBuilder squirrel.SelectBuilder) (float64, error) {
	query, values, err := sumBuilder.Where(squirrel.Eq{"del_state": constant.DelStateNo}).ToSql()
	if err != nil {
		return 0, err
	}

	var resp float64
	err = m.QueryRowNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return 0, err
	}
}

func (m *defaultIotGatewayModel) FindCount(ctx context.Context, countBuilder squirrel.SelectBuilder) (int64, error) {
	query, values, err := countBuilder.Where(squirrel.Eq{"del_state": constant.DelStateNo}).ToSql()
	if err != nil {
		return 0, err
	}

	var resp int64
	err = m.QueryRowNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return 0, err
	}
}

func (m *defaultIotGatewayModel) FindAll(ctx context.Context, rowBuilder squirrel.SelectBuilder, orderBy string) ([]*IotGateway, error) {
	if orderBy == "" {
		rowBuilder = rowBuilder.OrderBy(m.primaryField() + " DESC")
	} else {
		rowBuilder = rowBuilder.OrderBy(orderBy)
	}

	query, values, err := rowBuilder.Where(squirrel.Eq{"del_state": constant.DelStateNo}).ToSql()
	if err != nil {
		return nil, err
	}

	var resp []*IotGateway
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}

func (m *defaultIotGatewayModel) FindPageListByPage(ctx context.Context, rowBuilder squirrel.SelectBuilder, page, pageSize int64, orderBy string) ([]*IotGateway, error) {
	if orderBy == "" {
		rowBuilder = rowBuilder.OrderBy(m.primaryField() + " DESC")
	} else {
		rowBuilder = rowBuilder.OrderBy(orderBy)
	}

	if page < 1 {
		page = 1
	}
	if pageSize < 1 {
		pageSize = 10
	}
	offset := (page - 1) * pageSize

	query, values, err := rowBuilder.Where(squirrel.Eq{"del_state": constant.DelStateNo}).Offset(uint64(offset)).Limit(uint64(pageSize)).ToSql()
	if err != nil {
		return nil, err
	}

	var resp []*IotGateway
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}

func (m *defaultIotGatewayModel) InsertEx(ctx context.Context, session sqlx.Session, dataList []*IotGateway) ([]int64, error) {
	// 判断外部是否有事务，如果没事务，自己开启事务，方便数据库插入和搜索引擎插入的同步和回滚
	if session != nil {
		return m.insertLogic(ctx, session, dataList)
	}

	// 外部无事务
	var rtIds []int64
	if err := m.Trans(ctx, func(c context.Context, si sqlx.Session) error {
		inIds, err := m.insertLogic(ctx, si, dataList)
		if err != nil {
			return err
		}
		rtIds = inIds
		return nil
	}); err != nil {
		return []int64{}, err
	}
	return rtIds, nil
}

func (m *defaultIotGatewayModel) DeleteEx(ctx context.Context, session sqlx.Session, ids []int64, isDel bool, userId string) error {
	// 判断外部是否有事务，如果没事务，自己开启事务，方便数据库删除和搜索引擎删除的同步和回滚
	if session != nil {
		return m.deleteLogic(ctx, session, ids, isDel, userId)
	}

	// 外部无事务
	if err := m.Trans(ctx, func(c context.Context, si sqlx.Session) error {
		return m.deleteLogic(ctx, si, ids, isDel, userId)
	}); err != nil {
		return err
	}
	return nil
}

func (m *defaultIotGatewayModel) UpdateEx(ctx context.Context, session sqlx.Session, data *IotGateway) error {
	// 判断外部是否有事务，如果没事务，自己开启事务，方便数据库更新和搜索引擎更新的同步和回滚
	if session != nil {
		return m.updateLogic(ctx, session, data)
	}

	// 外部无事务
	if err := m.Trans(ctx, func(c context.Context, si sqlx.Session) error {
		return m.updateLogic(ctx, si, data)
	}); err != nil {
		return err
	}
	return nil
}

// 新增逻辑
func (m *defaultIotGatewayModel) insertLogic(ctx context.Context, session sqlx.Session, dataList []*IotGateway) ([]int64, error) {
	var err error
	var resp sql.Result
	var rtIds []int64
	if len(dataList) == 1 {
		resp, err = m.insert(ctx, session, dataList[0])
		if err == nil {
			rtId, _ := resp.LastInsertId()
			rtIds = append(rtIds, rtId)
		}
	} else {
		resp, err = m.inserts(ctx, session, dataList)
		if err == nil {
			rtId, _ := resp.LastInsertId()
			rtIds = append(rtIds, rtId)
			for i := 1; i < len(dataList); i++ {
				rtIds = append(rtIds, rtId+int64(i))
			}
		}
	}
	if err != nil {
		return []int64{}, err
	}

	//TODO 扩展搜索引擎

	return rtIds, nil
}

// 删除逻辑
func (m *defaultIotGatewayModel) deleteLogic(ctx context.Context, session sqlx.Session, ids []int64, isDel bool, userId string) error {
	var err error
	if len(ids) == 1 {
		if isDel {
			err = m.delete(ctx, session, ids[0])
		} else {
			err = m.deleteSoft(ctx, session, ids[0], userId)
		}
	} else {
		if isDel {
			err = m.deletes(ctx, session, ids)
		} else {
			err = m.deletesSoft(ctx, session, ids, userId)
		}
	}
	if err != nil {
		return err
	}

	//TODO 扩展搜索引擎

	return nil
}

// 更新逻辑
func (m *defaultIotGatewayModel) updateLogic(ctx context.Context, session sqlx.Session, data *IotGateway) error {
	var err error
	err = m.update(ctx, session, data)
	if err != nil {
		return err
	}

	//TODO 扩展搜索引擎

	return nil
}
