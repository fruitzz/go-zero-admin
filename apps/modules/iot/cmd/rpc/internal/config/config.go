package config

import (
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/zrpc"
)

type Config struct {
	zrpc.RpcServerConf
	DB struct {
		DataSource string
	}
	ModelCache cache.CacheConf
	SystemRpc  zrpc.RpcClientConf
	// 自定义参数
	IotCustom struct {
		NatsConfig struct {
			Servers string
			IsRetry bool
		}
	}
}
