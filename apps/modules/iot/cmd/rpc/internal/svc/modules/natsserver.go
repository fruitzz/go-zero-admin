package modules

import (
	natsjs "go-zero-admin/apps/modules/iot/cmd/nats"
	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/config"

	natsio "github.com/nats-io/nats.go"
)

// NewNatsServer 创建nats服务
func NewNatsServer(c config.Config) (*natsio.Conn, natsio.JetStreamContext, *natsio.EncodedConn) {
	natsConfig := c.IotCustom.NatsConfig
	return natsjs.InitNatsConn(&natsjs.NatsConnectProperties{
		Servers: natsConfig.Servers,
		IsRetry: natsConfig.IsRetry,
		EncType: natsio.JSON_ENCODER,
	})
}
