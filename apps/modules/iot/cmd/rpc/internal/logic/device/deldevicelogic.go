package devicelogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/apps/modules/iot/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type DelDeviceLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDelDeviceLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DelDeviceLogic {
	return &DelDeviceLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DelDeviceLogic) DelDevice(in *iotpb.DelDeviceReq) (*iotpb.DelDeviceResp, error) {
	if len(in.GetIds()) == 0 || (!in.GetIsDel() && in.GetUpdateUser() == "") {
		return nil, errors.Wrapf(xerr.NewErrMsg("删除设备失败：参数缺失"),
			tool.GetErrMsgFormat("delete device"), "params is not existed", in)
	}

	// 删除数据
	err := l.svcCtx.IotDeviceModel.DeleteEx(l.ctx, nil, in.GetIds(), in.GetIsDel(), in.GetUpdateUser())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("delete device"), err, in)
	}

	return &iotpb.DelDeviceResp{}, nil
}
