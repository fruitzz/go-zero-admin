package devicelogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/apps/modules/iot/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpDeviceTslLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpDeviceTslLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpDeviceTslLogic {
	return &UpDeviceTslLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpDeviceTslLogic) UpDeviceTsl(in *iotpb.UpDeviceTslReq) (*iotpb.UpDeviceTslResp, error) {
	one, err := l.svcCtx.IotDeviceModel.FindOne(l.ctx, in.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update device state"), err, in)
	}

	one.Metadata = in.GetMetadata()
	one.UpdateUser = in.GetUpdateUser()

	// 更新
	err = l.svcCtx.IotDeviceModel.UpdateEx(l.ctx, nil, one)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update device"), err, in)
	}

	return &iotpb.UpDeviceTslResp{}, nil
}
