package networklogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/link/network/constant"
	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/apps/modules/iot/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpNetWorkLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpNetWorkLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpNetWorkLogic {
	return &UpNetWorkLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpNetWorkLogic) UpNetWork(in *iotpb.UpNetWorkReq) (*iotpb.UpNetWorkResp, error) {
	network := in.GetParams()
	if network == nil || network.GetId() == 0 || network.GetDeptId() == 0 || network.GetName() == "" ||
		network.GetConfiguration() == "" || network.GetType() == "" || network.GetUpdateUser() == "" {
		return nil, errors.Wrapf(xerr.NewErrMsg("更新网络组件失败：参数缺失"),
			tool.GetErrMsgFormat("update network"), "params is not existed", in)
	}

	// 获取数据
	one, err := l.svcCtx.IotNetworkModel.FindOne(l.ctx, network.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update network"), err, in)
	}

	// 判断是否已经启动 启动的不让更新
	if one.State == constant.NetworkStateOpen {
		return nil, errors.Wrapf(xerr.NewErrMsg("更新网络组件失败：ID："+
			convertor.ToString(one.Id)+"，名称："+one.Name+"，已启动，请关闭后再更新"),
			tool.GetErrMsgFormat("update network"),
			"network id: "+convertor.ToString(one.Id)+", name: "+one.Name+", is opened", one)
	}

	one.DeptId = network.GetDeptId()
	one.Name = network.GetName()
	one.Configuration = network.GetConfiguration()
	one.State = network.GetState()
	one.Type = network.GetType()
	one.Description = network.GetDescription()
	one.UpdateUser = network.GetUpdateUser()

	// 更新
	err = l.svcCtx.IotNetworkModel.UpdateEx(l.ctx, nil, one)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update network"), err, in)
	}

	return &iotpb.UpNetWorkResp{}, nil
}
