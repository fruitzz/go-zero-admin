package networklogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/link/network/constant"
	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetNetWorkTypeListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetNetWorkTypeListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetNetWorkTypeListLogic {
	return &GetNetWorkTypeListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetNetWorkTypeListLogic) GetNetWorkTypeList(in *iotpb.GetNetWorkTypeListReq) (*iotpb.GetNetWorkTypeListResp, error) {
	var networkTypeList []*iotpb.NetWorkType
	for k, v := range constant.NetworkTypeMap {
		netWorkType := new(iotpb.NetWorkType)
		netWorkType.Id = k
		netWorkType.Name = v
		networkTypeList = append(networkTypeList, netWorkType)
	}

	return &iotpb.GetNetWorkTypeListResp{List: networkTypeList}, nil
}
