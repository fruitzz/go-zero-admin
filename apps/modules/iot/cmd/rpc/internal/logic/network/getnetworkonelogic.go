package networklogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/apps/modules/iot/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetNetWorkOneLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetNetWorkOneLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetNetWorkOneLogic {
	return &GetNetWorkOneLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetNetWorkOneLogic) GetNetWorkOne(in *iotpb.GetNetWorkOneReq) (*iotpb.GetNetWorkOneResp, error) {
	one, err := l.svcCtx.IotNetworkModel.FindOne(l.ctx, in.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("get network one"), err, in)
	}

	// 转换数据
	var data iotpb.IotNetwork
	_ = copier.Copy(&data, one)

	return &iotpb.GetNetWorkOneResp{One: &data}, nil
}
