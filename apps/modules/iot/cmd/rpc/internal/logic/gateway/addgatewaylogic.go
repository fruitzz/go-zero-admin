package gatewaylogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/apps/modules/iot/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddGatewayLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAddGatewayLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AddGatewayLogic {
	return &AddGatewayLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *AddGatewayLogic) AddGateway(in *iotpb.AddGatewayReq) (*iotpb.AddGatewayResp, error) {
	gateway := in.GetParams()
	if gateway == nil || gateway.GetDeptId() == 0 || gateway.GetName() == "" ||
		gateway.GetConfiguration() == "" || gateway.GetProvider() == "" ||
		gateway.GetCreateUser() == "" || gateway.GetUpdateUser() == "" {
		return nil, errors.Wrapf(xerr.NewErrMsg("新增设备网关失败：参数缺失"),
			tool.GetErrMsgFormat("insert network"), "params is not existed", in)
	}

	var data model.IotGateway
	_ = copier.Copy(&data, in.GetParams())

	// 保存数据
	_, err := l.svcCtx.IotGatewayModel.InsertEx(l.ctx, nil, []*model.IotGateway{&data})
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("insert gateway"), err, in)

	}

	return &iotpb.AddGatewayResp{}, nil
}
