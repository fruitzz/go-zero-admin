package gatewaylogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/link/gateway"
	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetGatewayTypeListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetGatewayTypeListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetGatewayTypeListLogic {
	return &GetGatewayTypeListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetGatewayTypeListLogic) GetGatewayTypeList(in *iotpb.GetGatewayTypeReq) (*iotpb.GetGatewayTypeResp, error) {
	var gatewayTypeList []*iotpb.GatewayType
	for _, v := range gateway.Providers {
		gatewayType := new(iotpb.GatewayType)
		gatewayType.Id = v.GetId()
		gatewayType.Name = v.GetName()
		gatewayTypeList = append(gatewayTypeList, gatewayType)
	}

	return &iotpb.GetGatewayTypeResp{}, nil
}
