package gatewaylogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/apps/modules/iot/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetGatewayOneLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetGatewayOneLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetGatewayOneLogic {
	return &GetGatewayOneLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetGatewayOneLogic) GetGatewayOne(in *iotpb.GetGatewayOneReq) (*iotpb.GetGatewayOneResp, error) {
	one, err := l.svcCtx.IotGatewayModel.FindOne(l.ctx, in.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("get gateway one"), err, in)
	}

	// 转换数据
	var data iotpb.IotGateway
	_ = copier.Copy(&data, one)

	return &iotpb.GetGatewayOneResp{One: &data}, nil
}
