package productlogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/apps/modules/iot/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddProductLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAddProductLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AddProductLogic {
	return &AddProductLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *AddProductLogic) AddProduct(in *iotpb.AddProductReq) (*iotpb.AddProductResp, error) {
	product := in.GetParams()
	if product == nil || product.GetDeptId() == 0 || product.GetName() == "" ||
		product.GetTransportProtocol() == "" || product.GetMessageProtocol() == "" ||
		product.GetStorePolicy() == "" || product.GetDeviceType() == "" ||
		product.GetCreateUser() == "" || product.GetUpdateUser() == "" ||
		in.GetUserId() == 0 {
		return nil, errors.Wrapf(xerr.NewErrMsg("新增产品失败：参数缺失"),
			tool.GetErrMsgFormat("insert product"), "params is not existed", in)
	}

	var data model.IotProduct
	_ = copier.Copy(&data, in.GetParams())

	// 保存数据
	_, err := l.svcCtx.IotProductModel.InsertEx(l.ctx, nil, []*model.IotProduct{&data})
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("insert product"), err, in)

	}

	return &iotpb.AddProductResp{}, nil
}
