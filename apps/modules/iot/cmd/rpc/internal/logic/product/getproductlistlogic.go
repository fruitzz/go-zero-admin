package productlogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/apps/system/cmd/rpc/syspb"
	"go-zero-admin/pkg/constant"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/Masterminds/squirrel"
	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductListLogic {
	return &GetProductListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductListLogic) GetProductList(in *iotpb.GetProductListReq) (*iotpb.GetProductListResp, error) {
	listWhereBuilder := l.svcCtx.IotProductModel.RowBuilder()
	countWhereBuilder := l.svcCtx.IotProductModel.CountBuilder("1")
	if in.GetDeptId() != "" {
		condition := squirrel.Eq{"dept_id": in.GetDeptId()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetName() != "" {
		condition := squirrel.Like{"name": in.GetName() + "%"}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetState() != "" {
		condition := squirrel.Eq{"state": in.GetState()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetDeviceType() != "" {
		condition := squirrel.Eq{"device_type": in.GetDeviceType()}
		listWhereBuilder = listWhereBuilder.Where(condition)
		countWhereBuilder = countWhereBuilder.Where(condition)
	}
	if in.GetIsDataScope() {
		dataScopeResp, _ := l.svcCtx.UserScopeRpc.GetUserDataScope(l.ctx,
			&syspb.GetUserDataScopeReq{UserId: in.GetDataScopeUserId()})
		var conditionList []any
		if len(dataScopeResp.GetDeptIds()) > 0 {
			condition := squirrel.Eq{"dept_id": dataScopeResp.GetDeptIds()}
			conditionList = append(conditionList, condition)
		}
		if dataScopeResp.GetUserId() != 0 {
			condition := squirrel.Eq{"create_user": dataScopeResp.GetUserId()}
			conditionList = append(conditionList, condition)
		}
		for _, v := range conditionList {
			listWhereBuilder = listWhereBuilder.Where(v)
			countWhereBuilder = countWhereBuilder.Where(v)
		}
	}

	// 查询
	if in.GetPage() != nil && in.GetPage().GetIsPage() {
		return l.getPage(in, listWhereBuilder, countWhereBuilder)
	} else if in.GetPage() != nil && in.GetPage().GetIsCount() {
		return l.getCount(in, countWhereBuilder)
	}
	return l.getList(in, listWhereBuilder)
}

func (l *GetProductListLogic) getList(in *iotpb.GetProductListReq,
	whereBuilder squirrel.SelectBuilder) (*iotpb.GetProductListResp, error) {
	orderBy := "id desc"
	if in.GetPage() != nil {
		orderBy = in.GetPage().GetOrderBy()
	}
	list, err := l.svcCtx.IotProductModel.FindAll(l.ctx, whereBuilder, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get product list"), err, in)
	}

	// 数据转换
	var dataList []*iotpb.IotProduct
	for _, v := range list {
		var data iotpb.IotProduct
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &iotpb.GetProductListResp{
		List: dataList,
	}, nil
}

func (l *GetProductListLogic) getPage(in *iotpb.GetProductListReq, listWhereBuilder squirrel.SelectBuilder,
	countWhereBuilder squirrel.SelectBuilder) (*iotpb.GetProductListResp, error) {
	// 分页条件
	pageNum := in.GetPage().GetPageNum()
	if pageNum == 0 {
		pageNum = 1
	}
	pageSize := in.GetPage().GetPageSize()
	if pageSize == 0 {
		pageSize = constant.PageSize
	}
	orderBy := in.GetPage().GetOrderBy()
	if orderBy == "" {
		orderBy = "id desc"
	}

	// 分页查询
	list, err := l.svcCtx.IotProductModel.FindPageListByPage(l.ctx, listWhereBuilder, pageNum, pageSize, orderBy)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get product list"), err, in)
	}
	count, err := l.svcCtx.IotProductModel.FindCount(l.ctx, countWhereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get product count"), err, in)
	}

	// 转换数据
	var dataList []*iotpb.IotProduct
	for _, v := range list {
		var data iotpb.IotProduct
		_ = copier.Copy(&data, v)

		data.UpdateTime = v.UpdateTime.Unix()
		data.CreateTime = v.CreateTime.Unix()

		dataList = append(dataList, &data)
	}

	return &iotpb.GetProductListResp{
		List: dataList,
		Page: &iotpb.PageResp{
			Current: pageNum,
			Total:   count,
		},
	}, nil
}

func (l *GetProductListLogic) getCount(in *iotpb.GetProductListReq,
	whereBuilder squirrel.SelectBuilder) (*iotpb.GetProductListResp, error) {
	count, err := l.svcCtx.IotProductModel.FindCount(l.ctx, whereBuilder)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrCode(xerr.DbError),
			tool.GetErrMsgFormat("get product count"), err, in)
	}

	return &iotpb.GetProductListResp{
		Page: &iotpb.PageResp{
			Total: count,
		},
	}, nil
}
