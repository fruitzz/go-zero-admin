package productlogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/apps/modules/iot/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpProductTslLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpProductTslLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpProductTslLogic {
	return &UpProductTslLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpProductTslLogic) UpProductTsl(in *iotpb.UpProductTslReq) (*iotpb.UpProductTslResp, error) {
	one, err := l.svcCtx.IotProductModel.FindOne(l.ctx, in.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update product state"), err, in)
	}

	one.Metadata = in.GetMetadata()
	one.UpdateUser = in.GetUpdateUser()

	// 更新
	err = l.svcCtx.IotProductModel.UpdateEx(l.ctx, nil, one)
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("update product"), err, in)
	}

	return &iotpb.UpProductTslResp{}, nil
}
