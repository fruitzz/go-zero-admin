package productlogic

import (
	"context"

	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/apps/modules/iot/model"
	"go-zero-admin/pkg/tool"
	"go-zero-admin/pkg/xerr"

	"github.com/jinzhu/copier"
	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductOneLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductOneLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductOneLogic {
	return &GetProductOneLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductOneLogic) GetProductOne(in *iotpb.GetProductOneReq) (*iotpb.GetProductOneResp, error) {
	one, err := l.svcCtx.IotProductModel.FindOne(l.ctx, in.GetId())
	if err != nil {
		return nil, errors.Wrapf(xerr.NewErrFindOneCode(err, model.ErrNotFound),
			tool.GetErrMsgFormat("get product one"), err, in)
	}

	// 转换数据
	var data iotpb.IotProduct
	_ = copier.Copy(&data, one)

	return &iotpb.GetProductOneResp{One: &data}, nil
}
