package main

import (
	"flag"
	"fmt"

	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/config"
	deviceServer "go-zero-admin/apps/modules/iot/cmd/rpc/internal/server/device"
	gatewayServer "go-zero-admin/apps/modules/iot/cmd/rpc/internal/server/gateway"
	netWorkServer "go-zero-admin/apps/modules/iot/cmd/rpc/internal/server/network"
	productServer "go-zero-admin/apps/modules/iot/cmd/rpc/internal/server/product"
	"go-zero-admin/apps/modules/iot/cmd/rpc/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/interceptor/rpcserver"

	"github.com/zeromicro/go-zero/core/conf"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/service"
	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var configFile = flag.String("f", "etc/iot.yaml", "the config file")

func main() {
	flag.Parse()

	// close log
	logx.DisableStat()

	var c config.Config
	conf.MustLoad(*configFile, &c)
	ctx := svc.NewServiceContext(c)

	s := zrpc.MustNewServer(c.RpcServerConf, func(grpcServer *grpc.Server) {
		iotpb.RegisterNetWorkServer(grpcServer, netWorkServer.NewNetWorkServer(ctx))
		iotpb.RegisterGatewayServer(grpcServer, gatewayServer.NewGatewayServer(ctx))
		iotpb.RegisterProductServer(grpcServer, productServer.NewProductServer(ctx))
		iotpb.RegisterDeviceServer(grpcServer, deviceServer.NewDeviceServer(ctx))

		if c.Mode == service.DevMode || c.Mode == service.TestMode {
			reflection.Register(grpcServer)
		}
	})

	// rpc log interceptor
	s.AddUnaryInterceptors(rpcserver.LoggerInterceptor)

	defer s.Stop()

	fmt.Printf("【IOT-RPC】Starting iot's rpc server at %s...\n", c.ListenOn)
	s.Start()
}
