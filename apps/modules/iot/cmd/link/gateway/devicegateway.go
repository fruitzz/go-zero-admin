package gateway

import "go-zero-admin/apps/modules/iot/cmd/link/network"

// IDeviceGateway 设备网关，用于统一管理设备连接，状态以及消息收发
type IDeviceGateway interface {
	// GetId 获取网关ID
	GetId() int64
	// GetTransport 获取传输协议
	GetTransport() *Transport
	// GetNetworkType 获取网络类型
	GetNetworkType() *network.NetworkType
	// OnMessage 订阅来自设备到消息，关闭网关时不会结束流.
	OnMessage()
	// Startup 启动网关
	Startup() error
	// Pause 暂停网关，暂停后停止处理设备消息
	Pause()
	// Shutdown 关闭网关
	Shutdown()
	// IsAlive 是否存活
	IsAlive() bool
	// GetTarget 获取目标引用
	GetTarget() any
}
