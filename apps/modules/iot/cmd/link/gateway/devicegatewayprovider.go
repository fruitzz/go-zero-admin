package gateway

// IDeviceGatewayProvider 设备网关提供者
type IDeviceGatewayProvider interface {
	// GetId 获取唯一标识
	GetId() string
	// GetName 获取名称
	GetName() string
	// GetDescription 获取接入说明
	GetDescription() string
	// GetChannel 接入通道,如: network,modbus
	GetChannel() string
	// GetOrder 排序。0从小到大排序  1从大到小排序
	GetOrder() int64
	// GetTransport 传输协议
	GetTransport() ITransport
	// CreateDeviceGateway 使用配置信息创建设备网关
	CreateDeviceGateway(properties *DeviceGatewayProperties) (IDeviceGateway, error)
	// ReloadDeviceGateway 重新加载网关
	ReloadDeviceGateway(deviceGateway IDeviceGateway, properties *DeviceGatewayProperties)
}
