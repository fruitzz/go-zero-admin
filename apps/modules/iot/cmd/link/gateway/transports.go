package gateway

var _ iTransports = (*transports)(nil)

var (
	ITransports = new(transports)
	All         = make(map[string]*Transport)
)

type (
	iTransports interface {
		Registers(transports []*Transport) func()
		Register(transport *Transport) func()
		Get() []*Transport
		Lookup(id string) *Transport
	}

	transports struct {
	}
)

func (t *transports) Registers(transports []*Transport) func() {
	for i := 0; i < len(transports); i++ {
		t.Register(transports[i])
	}
	return func() {
		for i := 0; i < len(transports); i++ {
			delete(All, transports[i].GetId())
		}
	}
}

func (t *transports) Register(transport *Transport) func() {
	All[transport.GetId()] = transport
	return func() {
		delete(All, transport.GetId())
	}
}

func (t *transports) Get() []*Transport {
	var transport []*Transport
	for _, v := range All {
		transport = append(transport, v)
	}
	return transport
}

func (t *transports) Lookup(id string) *Transport {
	return All[id]
}
