package gateway

import "go-zero-admin/apps/modules/iot/cmd/link/network"

// DeviceGatewayProperties 网关属性
type DeviceGatewayProperties struct {
	Id            int64
	ChannelId     int64
	ProtocolId    int64
	Name          string
	Channel       string
	Transport     string
	Provider      string
	Configuration map[string]any
	State         int64
	Description   string
	NetworkConfig *network.NetworkProperties
}
