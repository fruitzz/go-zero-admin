package constant

// Mqtt 网关类型
const (
	Mqtt = "MQTT"
)

// DefaultTransport 网关类型
var DefaultTransport = map[string]string{
	Mqtt: "MQTT",
}

// 设备网关状态
const (
	// GatewayStateClose 已停止
	GatewayStateClose = 0
	// GatewayStateOpen 已开启
	GatewayStateOpen = 1
	// GatewayStateSuspend 已暂停
	GatewayStateSuspend = 2
)
