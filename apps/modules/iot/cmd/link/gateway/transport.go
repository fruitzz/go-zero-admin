package gateway

import "go-zero-admin/apps/modules/iot/cmd/link/gateway/constant"

var _ ITransport = (*Transport)(nil)

type (
	ITransport interface {
		GetId() string
		GetName() string
		IsSame(transport *Transport) bool
		IsSameById(ids string) bool
		Of(id string) *Transport
		Lookup(id string) *Transport
		GetAll() []*Transport
	}

	Transport struct {
		Id   string `json:"id"`
		Name string `json:"name"`
	}
)

func (t *Transport) GetId() string {
	return t.Id
}

func (t *Transport) GetName() string {
	return t.Name
}

func (t *Transport) IsSame(transport *Transport) bool {
	return t == transport || t.Id == transport.Id
}

func (t *Transport) IsSameById(transportId string) bool {
	return t.Id == transportId
}

func (t *Transport) Of(id string) *Transport {
	name := constant.DefaultTransport[id]
	return &Transport{id, name}
}

func (t *Transport) Lookup(id string) *Transport {
	return ITransports.Lookup(id)
}

func (t *Transport) GetAll() []*Transport {
	return ITransports.Get()
}
