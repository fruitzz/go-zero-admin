package link

import (
	"go-zero-admin/apps/modules/iot/cmd/link/gateway"
	gatewaymqttclient "go-zero-admin/apps/modules/iot/cmd/link/gateway/mqttclient"
	"go-zero-admin/apps/modules/iot/cmd/link/network"
	networkmqttclient "go-zero-admin/apps/modules/iot/cmd/link/network/mqttclient"
)

// InitIot 初始化IOT组件库
func InitIot(networkConfigList []*network.NetworkProperties) {
	initNetworkType()
	initNetworkProvider()
	initNetwork(networkConfigList)
}

// 初始化设备网关提供者
func initGatewayProvider() {
	gateway.Providers[gatewaymqttclient.IMqttClientDeviceGatewayProvider.GetId()] = gatewaymqttclient.
		IMqttClientDeviceGatewayProvider
}

// 初始化网络组件
func initNetwork(networkConfigList []*network.NetworkProperties) {
	network.InitNetwork(networkConfigList)
}

// 初始化网络组件提供者
func initNetworkProvider() {
	// 初始化mqtt客户端提供者
	network.Register(networkmqttclient.IMqttClientProvider)
	// 初始化其他客户端 。。。
}

// 初始化网络类型
func initNetworkType() {
	iNetworkType := new(network.NetworkType)
	network.INetworkTypes.Registers(iNetworkType.GetAll())
}
