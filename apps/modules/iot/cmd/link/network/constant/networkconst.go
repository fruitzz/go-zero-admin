package constant

// MqttClient 网络组件类型枚举
const (
	MqttClient = "MQTT_CLIENT"
)

// NetworkTypeMap 网络组件类型map
var NetworkTypeMap = map[string]string{
	MqttClient: "MQTT客户端",
}

// 网络组件状态
const (
	// NetworkStateOpen 已启动
	NetworkStateOpen = 1
	// NetworkStateClose 已停止
	NetworkStateClose = 0
)

// mqtt client
const (
	// MqttClientQos0 mqtt client's qos
	MqttClientQos0 = 0
	MqttClientQos1 = 1
	MqttClientQos2 = 2

	// MqttClientMsgNatsQue nats topic
	MqttClientMsgNatsQue = "MQTT_CLIENT_MSG_NATS_QUE_"
)
