package network

// INetworkConfig 网络组件配置
type INetworkConfig interface {
	// GetId 获取配置ID
	GetId() int64
	// GetTransport 网络协议类型 TCP or UDP
	GetTransport() string
	// GetSchema 传输模式，如：http、mqtt、ws
	GetSchema() string
	// IsSecure 是否使用安全加密(TLS、DTLS)
	IsSecure() bool
	// GetCertId 安全证书ID，当IsSecure为true时，不能为空
	GetCertId() int64
	// Validate // 验证配置，通过返回true
	Validate() bool
}
