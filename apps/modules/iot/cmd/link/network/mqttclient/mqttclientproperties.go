package mqttclient

import (
	"strconv"

	"go-zero-admin/apps/modules/iot/cmd/link/network"
)

var _ IMqttClientProperties = (*MqttClientProperties)(nil)

type (
	IMqttClientProperties interface {
		network.IClientNetworkConfig
		GetMaxMessageSize() int64
	}

	MqttClientProperties struct {
		network.ClientNetworkConfig
		ClientId       string `json:"clientId"`
		Username       string `json:"username"`
		Password       string `json:"password"`
		MaxMessageSize int64  `json:"maxMessageSize"`
		TopicPrefix    string `json:"topicPrefix"`
	}
)

func (m *MqttClientProperties) GetId() int64 {
	return m.Id
}

func (m *MqttClientProperties) GetTransport() string {
	return "TCP"
}

func (m *MqttClientProperties) GetSchema() string {
	var schema string
	if m.IsSecure() {
		schema = "mqtts"
	} else {
		schema = "mqtt"
	}
	return schema
}

func (m *MqttClientProperties) IsSecure() bool {
	return m.Secure
}

func (m *MqttClientProperties) GetCertId() int64 {
	return m.CertId
}

func (m *MqttClientProperties) Validate() bool {
	if m.IsSecure() && m.GetCertId() == 0 {
		return false
	}
	return true
}

func (m *MqttClientProperties) GetRemoteHost() string {
	return m.RemoteHost
}

func (m *MqttClientProperties) GetRemotePort() int64 {
	return m.RemotePort
}

func (m *MqttClientProperties) GetRemoteAddress() string {
	return m.GetSchema() + "://" + m.GetRemoteHost() + ":" + strconv.FormatInt(m.GetRemotePort(), 10)
}

func (m *MqttClientProperties) GetMaxMessageSize() int64 {
	if m.MaxMessageSize == 0 {
		return 1024 * 1024
	}
	return m.MaxMessageSize
}
