package network

// NetworkProperties 网络组件属性
type NetworkProperties struct {
	Id             int64          `json:"id"`
	Name           string         `json:"name"`
	Enabled        bool           `json:"enabled"`
	Type           string         `json:"type"`
	Configurations map[string]any `json:"configurations"`
}
