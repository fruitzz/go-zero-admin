package network

// 客户端网络组件配置
type (
	IClientNetworkConfig interface {
		INetworkConfig
		GetRemoteHost() string
		GetRemotePort() int64
		GetRemoteAddress() string
	}

	ClientNetworkConfig struct {
		Id         int64  `json:"id"`
		Secure     bool   `json:"secure"`
		CertId     int64  `json:"certId"`
		RemoteHost string `json:"remoteHost"`
		RemotePort int64  `json:"remotePort"`
	}
)
