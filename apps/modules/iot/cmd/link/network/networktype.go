package network

import (
	"go-zero-admin/apps/modules/iot/cmd/link/network/constant"
)

var _ INetworkType = (*NetworkType)(nil)

type (
	INetworkType interface {
		// GetId 类型唯一标识
		GetId() string
		// GetName 类型名称
		GetName() string
		// Of 使用指定的ID创建一个NetworkType
		Of(id string) *NetworkType
		// GetAll 获取所有支持的网络组件类型
		GetAll() []*NetworkType
		// Lookup 根据网络组件类型ID获取类型指针
		Lookup(id string) *NetworkType
	}

	NetworkType struct {
		Id   string `json:"id"`
		Name string `json:"name"`
	}
)

func (n *NetworkType) GetId() string {
	return n.Id
}

func (n *NetworkType) GetName() string {
	return n.Name
}

func (n *NetworkType) Of(id string) *NetworkType {
	name := constant.NetworkTypeMap[id]
	return &NetworkType{id, name}
}

func (n *NetworkType) GetAll() []*NetworkType {
	var networkTypeList []*NetworkType
	for k, v := range constant.NetworkTypeMap {
		networkTypeList = append(networkTypeList, &NetworkType{k, v})
	}
	return networkTypeList
}

func (n *NetworkType) Lookup(id string) *NetworkType {
	return INetworkTypes.Lookup(id)
}
