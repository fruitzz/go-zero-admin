package network

var _ iNetworkTypes = (*networkTypes)(nil)

var (
	INetworkTypes = new(networkTypes)
	All           = make(map[string]*NetworkType)
)

type (
	iNetworkTypes interface {
		// Registers 注册所有网络组件类型
		Registers(networkType []*NetworkType)
		// Register 注册网络组件类型
		Register(networkType *NetworkType)
		// Get 获取网络组件类型列表
		Get() []*NetworkType
		// Lookup 根据ID获取网络组件类型
		Lookup(id string) *NetworkType
	}

	networkTypes struct {
	}
)

func (n *networkTypes) Registers(networkType []*NetworkType) {
	for i := 0; i < len(networkType); i++ {
		n.Register(networkType[i])
	}
}

func (n *networkTypes) Register(networkType *NetworkType) {
	All[networkType.GetId()] = networkType
}

func (n *networkTypes) Get() []*NetworkType {
	var network []*NetworkType
	for _, v := range All {
		network = append(network, v)
	}
	return network
}

func (n *networkTypes) Lookup(id string) *NetworkType {
	return All[id]
}
