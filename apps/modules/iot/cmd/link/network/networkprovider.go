package network

// INetworkProvider 网络组件服务提供者
type INetworkProvider interface {
	// GetType 获取网络组件类型
	GetType() *NetworkType
	// CreateNetwork 使用配置创建一个网络组件
	CreateNetwork(properties any) (INetwork, error)
	// Reload 重新加载网络组件
	Reload(network any, properties any) (INetwork, error)
	// CreateConfig 根据可序列化的配置信息创建网络组件配置
	CreateConfig(properties *NetworkProperties) (any, error)
	// IsReusable 返回网络组件是否可复用，网络组件不能复用时，在设备接入等操作时将无法选择已经被使用的网络组件
	IsReusable() bool
}
