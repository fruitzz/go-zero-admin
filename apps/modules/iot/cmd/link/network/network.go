package network

// INetwork 网络组件接口
type INetwork interface {
	// GetId 获取ID唯一标识
	GetId() int64
	// GetType 获取网络类型
	GetType() *NetworkType
	// Shutdown 关闭网络组件
	Shutdown()
	// IsAlive 是否存活
	IsAlive() bool
	// IsAutoReload 是否重新加载
	IsAutoReload() bool
	// GetTarget 获取目标引用
	GetTarget() any
}
