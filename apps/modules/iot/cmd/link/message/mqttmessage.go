package message

// IMqttMessage mqtt消息体
type IMqttMessage interface {
	IEncodedMessage
	GetTopic() string
	GetClientId() string
	GetMessageId() int64
	IsWill() bool
	GetQosLevel() int64
	IsDup() bool
	IsRetain() bool
	Print() string
}
