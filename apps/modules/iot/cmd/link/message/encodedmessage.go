package message

// IEncodedMessage 消息体编码
type IEncodedMessage interface {
	GetPayload() string
}
