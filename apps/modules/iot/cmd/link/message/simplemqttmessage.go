package message

var _ ISimpleMqttMessage = (*SimpleMqttMessage)(nil)

type (
	ISimpleMqttMessage interface {
		IMqttMessage
		BodyStringBuild(payload string) *SimpleMqttMessage
		BodyByteBuild(payload []byte) *SimpleMqttMessage
		PayloadStringBuild(payload string) *SimpleMqttMessage
		PayloadByteBuild(payload []byte) *SimpleMqttMessage
		TopicBuild(topic string) *SimpleMqttMessage
		ClientIdBuild(clientId string) *SimpleMqttMessage
		QosLevelBuild(qosLevel int64) *SimpleMqttMessage
		PayloadBuild(payload string) *SimpleMqttMessage
		MessageIdBuild(messageId int64) *SimpleMqttMessage
		WillBuild(will bool) *SimpleMqttMessage
		DupBuild(dup bool) *SimpleMqttMessage
		RetainBuild(retain bool) *SimpleMqttMessage
	}

	SimpleMqttMessage struct {
		Topic     string `json:"topic"`
		ClientId  string `json:"clientId"`
		QosLevel  int64  `json:"qosLevel"`
		Payload   string `json:"payload"`
		MessageId int64  `json:"messageId"`
		Will      bool   `json:"will"`
		Dup       bool   `json:"dup"`
		Retain    bool   `json:"retain"`
	}
)

func (s *SimpleMqttMessage) GetPayload() string {
	return s.Payload
}

func (s *SimpleMqttMessage) GetTopic() string {
	return s.Topic
}

func (s *SimpleMqttMessage) GetClientId() string {
	return s.ClientId
}

func (s *SimpleMqttMessage) GetMessageId() int64 {
	return s.MessageId
}

func (s *SimpleMqttMessage) IsWill() bool {
	return s.Will
}

func (s *SimpleMqttMessage) GetQosLevel() int64 {
	return s.QosLevel
}

func (s *SimpleMqttMessage) IsDup() bool {
	return s.Dup
}

func (s *SimpleMqttMessage) IsRetain() bool {
	return s.Retain
}

func (s *SimpleMqttMessage) Print() string {
	return ""
}

func Build() *SimpleMqttMessage {
	return new(SimpleMqttMessage)
}

func (s *SimpleMqttMessage) BodyStringBuild(payload string) *SimpleMqttMessage {
	s.Payload = payload
	return s
}

func (s *SimpleMqttMessage) BodyByteBuild(payload []byte) *SimpleMqttMessage {
	s.Payload = string(payload)
	return s
}

func (s *SimpleMqttMessage) PayloadStringBuild(payload string) *SimpleMqttMessage {
	s.Payload = payload
	return s
}

func (s *SimpleMqttMessage) PayloadByteBuild(payload []byte) *SimpleMqttMessage {
	s.Payload = string(payload)
	return s
}

func (s *SimpleMqttMessage) TopicBuild(topic string) *SimpleMqttMessage {
	s.Topic = topic
	return s
}

func (s *SimpleMqttMessage) ClientIdBuild(clientId string) *SimpleMqttMessage {
	s.ClientId = clientId
	return s
}

func (s *SimpleMqttMessage) QosLevelBuild(qosLevel int64) *SimpleMqttMessage {
	s.QosLevel = qosLevel
	return s
}

func (s *SimpleMqttMessage) PayloadBuild(payload string) *SimpleMqttMessage {
	s.Payload = payload
	return s
}

func (s *SimpleMqttMessage) MessageIdBuild(messageId int64) *SimpleMqttMessage {
	s.MessageId = messageId
	return s
}

func (s *SimpleMqttMessage) WillBuild(will bool) *SimpleMqttMessage {
	s.Will = will
	return s
}

func (s *SimpleMqttMessage) DupBuild(dup bool) *SimpleMqttMessage {
	s.Dup = dup
	return s
}

func (s *SimpleMqttMessage) RetainBuild(retain bool) *SimpleMqttMessage {
	s.Retain = retain
	return s
}
