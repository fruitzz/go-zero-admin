package product

import (
	"net/http"

	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/logic/product"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
)

func UpdateProductHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.UpdateProductReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := product.NewUpdateProductLogic(r.Context(), svcCtx, r)
		resp, err := l.UpdateProduct(&req)
		result.HttpResult(r, w, resp, err)
	}
}
