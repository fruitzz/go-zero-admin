package network

import (
	"net/http"

	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/logic/network"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
)

func AddNetworkHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.AddNetworkReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := network.NewAddNetworkLogic(r.Context(), svcCtx, r)
		resp, err := l.AddNetwork(&req)
		result.HttpResult(r, w, resp, err)
	}
}
