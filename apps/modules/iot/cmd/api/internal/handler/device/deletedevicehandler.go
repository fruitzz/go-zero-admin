package device

import (
	"net/http"

	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/logic/device"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
)

func DeleteDeviceHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.DeleteDeviceReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := device.NewDeleteDeviceLogic(r.Context(), svcCtx, r)
		resp, err := l.DeleteDevice(&req)
		result.HttpResult(r, w, resp, err)
	}
}
