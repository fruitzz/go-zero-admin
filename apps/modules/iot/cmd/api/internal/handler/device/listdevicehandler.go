package device

import (
	"net/http"

	"go-zero-admin/pkg/result"

	"github.com/zeromicro/go-zero/rest/httpx"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/logic/device"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
)

func ListDeviceHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.ListDeviceReq
		if err := httpx.Parse(r, &req); err != nil {
			result.ParamErrorResult(r, w, err)
			return
		}

		l := device.NewListDeviceLogic(r.Context(), svcCtx, r)
		resp, err := l.ListDevice(&req)
		result.HttpResult(r, w, resp, err)
	}
}
