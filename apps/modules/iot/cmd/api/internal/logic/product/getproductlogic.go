package product

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProductLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewGetProductLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *GetProductLogic {
	return &GetProductLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *GetProductLogic) GetProduct(req *types.GetProductReq) (resp *types.GetProductResp, err error) {
	oneResp, err := l.svcCtx.ProductRpc.GetProductOne(l.ctx, &iotpb.GetProductOneReq{Id: req.Id})
	if err != nil {
		return nil, err
	}

	var product types.IotProduct
	_ = copier.Copy(&product, oneResp.GetOne())

	return &types.GetProductResp{Data: &product}, nil
}
