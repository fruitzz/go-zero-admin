package product

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateProductStateLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateProductStateLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateProductStateLogic {
	return &UpdateProductStateLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateProductStateLogic) UpdateProductState(req *types.UpdateProductStateReq) (resp *types.UpdateProductStateResp, err error) {
	_, err = l.svcCtx.ProductRpc.UpProductState(l.ctx, &iotpb.UpProductStateReq{
		Id:         req.Id,
		State:      req.State,
		UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
	})
	if err != nil {
		return nil, err
	}

	return
}
