package product

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateProductTslLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateProductTslLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateProductTslLogic {
	return &UpdateProductTslLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateProductTslLogic) UpdateProductTsl(req *types.UpdateProductTslReq) (resp *types.UpdateProductTslResp, err error) {
	_, err = l.svcCtx.ProductRpc.UpProductTsl(l.ctx, &iotpb.UpProductTslReq{
		Id:         req.Id,
		Metadata:   req.Metadata,
		UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
	})
	if err != nil {
		return nil, err
	}

	return
}
