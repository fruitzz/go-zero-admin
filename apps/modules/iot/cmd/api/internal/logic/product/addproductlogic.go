package product

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddProductLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddProductLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddProductLogic {
	return &AddProductLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddProductLogic) AddProduct(req *types.AddProductReq) (resp *types.AddProductResp, err error) {
	_, err = l.svcCtx.ProductRpc.AddProduct(l.ctx, &iotpb.AddProductReq{
		Params: &iotpb.IotProduct{
			DeptId:            req.DeptId,
			Name:              req.Name,
			Photo:             req.Photo,
			TransportProtocol: req.TransportProtocol,
			MessageProtocol:   req.MessageProtocol,
			StorePolicy:       req.StorePolicy,
			DeviceType:        req.DeviceType,
			Describe:          req.Describe,
			CreateUser:        convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			UpdateUser:        convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
		UserId: ctxdata.GetUidFromCtx(l.ctx),
	})
	if err != nil {
		return nil, err
	}

	return
}
