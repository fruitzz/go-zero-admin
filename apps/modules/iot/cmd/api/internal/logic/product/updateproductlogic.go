package product

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateProductLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateProductLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateProductLogic {
	return &UpdateProductLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateProductLogic) UpdateProduct(req *types.UpdateProductReq) (resp *types.UpdateProductResp, err error) {
	_, err = l.svcCtx.ProductRpc.UpProduct(l.ctx, &iotpb.UpProductReq{
		Params: &iotpb.IotProduct{
			Id:                req.Id,
			DeptId:            req.DeptId,
			Name:              req.Name,
			Photo:             req.Photo,
			TransportProtocol: req.TransportProtocol,
			MessageProtocol:   req.MessageProtocol,
			StorePolicy:       req.StorePolicy,
			DeviceType:        req.DeviceType,
			Describe:          req.Describe,
			UpdateUser:        convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}

	return
}
