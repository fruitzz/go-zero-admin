package network

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateNetworkStateLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateNetworkStateLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateNetworkStateLogic {
	return &UpdateNetworkStateLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateNetworkStateLogic) UpdateNetworkState(req *types.UpdateNetworkStateReq) (resp *types.UpdateNetworkStateResp, err error) {
	_, err = l.svcCtx.NetworkRpc.UpNetWorkState(l.ctx, &iotpb.UpNetWorkStateReq{
		Id:         req.Id,
		State:      req.State,
		UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
	})
	if err != nil {
		return nil, err
	}

	return
}
