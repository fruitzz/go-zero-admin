package network

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type ListNetworkLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewListNetworkLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *ListNetworkLogic {
	return &ListNetworkLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *ListNetworkLogic) ListNetwork(req *types.ListNetworkReq) (resp *types.ListNetworkResp, err error) {
	networkListResp, err := l.svcCtx.NetworkRpc.GetNetWorkList(l.ctx, &iotpb.GetNetWorkListReq{
		Page: &iotpb.PageReq{
			DateRange: req.DateRange,
			PageNum:   req.PageNum,
			PageSize:  req.PageSize,
			IsPage:    true,
		},
		DeptId:          req.DeptId,
		Name:            req.Name,
		Type:            req.Type,
		State:           req.State,
		IsDataScope:     true,
		DataScopeUserId: ctxdata.GetUidFromCtx(l.ctx),
	})
	if err != nil {
		return nil, err
	}

	// 处理数据
	networkList := make([]*types.IotNetwork, 0)
	for _, v := range networkListResp.GetList() {
		var network types.IotNetwork
		_ = copier.Copy(&network, v)
		networkList = append(networkList, &network)
	}

	return &types.ListNetworkResp{
		PageResp: &types.PageResp{
			CurrentPage: networkListResp.GetPage().GetCurrent(),
			Total:       networkListResp.GetPage().GetTotal(),
		},
		List: networkList,
	}, nil
}
