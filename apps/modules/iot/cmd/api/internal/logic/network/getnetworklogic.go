package network

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetNetworkLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewGetNetworkLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *GetNetworkLogic {
	return &GetNetworkLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *GetNetworkLogic) GetNetwork(req *types.GetNetworkReq) (resp *types.GetNetworkResp, err error) {
	oneResp, err := l.svcCtx.NetworkRpc.GetNetWorkOne(l.ctx, &iotpb.GetNetWorkOneReq{Id: req.Id})
	if err != nil {
		return nil, err
	}

	var network types.IotNetwork
	_ = copier.Copy(&network, oneResp.GetOne())

	return &types.GetNetworkResp{
		Data: &network,
	}, nil
}
