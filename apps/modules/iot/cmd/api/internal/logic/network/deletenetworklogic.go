package network

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteNetworkLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewDeleteNetworkLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *DeleteNetworkLogic {
	return &DeleteNetworkLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *DeleteNetworkLogic) DeleteNetwork(req *types.DeleteNetworkReq) (resp *types.DeleteNetworkResp, err error) {
	_, err = l.svcCtx.NetworkRpc.DelNetWork(l.ctx, &iotpb.DelNetWorkReq{
		IsDel: true,
		Ids:   req.Ids,
	})
	if err != nil {
		return nil, err
	}

	return
}
