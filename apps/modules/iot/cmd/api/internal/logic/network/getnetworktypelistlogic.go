package network

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetNetworkTypeListLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewGetNetworkTypeListLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *GetNetworkTypeListLogic {
	return &GetNetworkTypeListLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *GetNetworkTypeListLogic) GetNetworkTypeList(req *types.GetNetworkTypeListReq) (resp *types.GetNetworkTypeListResp, err error) {
	listResp, err := l.svcCtx.NetworkRpc.GetNetWorkTypeList(l.ctx, &iotpb.GetNetWorkTypeListReq{})
	if err != nil {
		return nil, err
	}

	// 处理数据
	networkTypeList := make([]*types.NetworkType, 0)
	for _, v := range listResp.GetList() {
		var networkType types.NetworkType
		_ = copier.Copy(&networkType, v)
		networkTypeList = append(networkTypeList, &networkType)
	}

	return &types.GetNetworkTypeListResp{List: networkTypeList}, nil
}
