package network

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddNetworkLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddNetworkLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddNetworkLogic {
	return &AddNetworkLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddNetworkLogic) AddNetwork(req *types.AddNetworkReq) (resp *types.AddNetworkResp, err error) {
	_, err = l.svcCtx.NetworkRpc.AddNetWork(l.ctx, &iotpb.AddNetWorkReq{
		Params: &iotpb.IotNetwork{
			DeptId:        req.DeptId,
			Name:          req.Name,
			Configuration: req.Configuration,
			State:         req.State,
			Type:          req.Type,
			Description:   req.Description,
			CreateUser:    convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			UpdateUser:    convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}

	return
}
