package network

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateNetworkLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateNetworkLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateNetworkLogic {
	return &UpdateNetworkLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateNetworkLogic) UpdateNetwork(req *types.UpdateNetworkReq) (resp *types.UpdateNetworkResp, err error) {
	_, err = l.svcCtx.NetworkRpc.UpNetWork(l.ctx, &iotpb.UpNetWorkReq{
		Params: &iotpb.IotNetwork{
			Id:            req.Id,
			DeptId:        req.DeptId,
			Name:          req.Name,
			Configuration: req.Configuration,
			State:         req.State,
			Type:          req.Type,
			Description:   req.Description,
			UpdateUser:    convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}

	return
}
