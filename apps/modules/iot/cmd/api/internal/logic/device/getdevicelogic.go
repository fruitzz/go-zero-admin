package device

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetDeviceLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewGetDeviceLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *GetDeviceLogic {
	return &GetDeviceLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *GetDeviceLogic) GetDevice(req *types.GetDeviceReq) (resp *types.GetDeviceResp, err error) {
	oneResp, err := l.svcCtx.DeviceRpc.GetDeviceOne(l.ctx, &iotpb.GetDeviceOneReq{Id: req.Id})
	if err != nil {
		return nil, err
	}

	var device types.IotDevice
	_ = copier.Copy(&device, oneResp.GetOne())

	return &types.GetDeviceResp{
		Data: &device,
	}, nil
}
