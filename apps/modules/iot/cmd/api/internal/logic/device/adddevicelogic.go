package device

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type AddDeviceLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewAddDeviceLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *AddDeviceLogic {
	return &AddDeviceLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *AddDeviceLogic) AddDevice(req *types.AddDeviceReq) (resp *types.AddDeviceResp, err error) {
	_, err = l.svcCtx.DeviceRpc.AddDevice(l.ctx, &iotpb.AddDeviceReq{
		Params: &iotpb.IotDevice{
			DeptId:     req.DeptId,
			ProductId:  req.ProductId,
			Name:       req.Name,
			Describe:   req.Describe,
			CreateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
		UserId: ctxdata.GetUidFromCtx(l.ctx),
	})
	if err != nil {
		return nil, err
	}

	return
}
