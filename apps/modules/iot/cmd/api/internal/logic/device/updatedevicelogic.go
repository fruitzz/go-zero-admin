package device

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateDeviceLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateDeviceLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateDeviceLogic {
	return &UpdateDeviceLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateDeviceLogic) UpdateDevice(req *types.UpdateDeviceReq) (resp *types.UpdateDeviceResp, err error) {
	_, err = l.svcCtx.DeviceRpc.UpDevice(l.ctx, &iotpb.UpDeviceReq{
		Params: &iotpb.IotDevice{
			Id:         req.Id,
			DeptId:     req.DeptId,
			ProductId:  req.ProductId,
			Name:       req.Name,
			Describe:   req.Describe,
			UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
		},
	})
	if err != nil {
		return nil, err
	}

	return
}
