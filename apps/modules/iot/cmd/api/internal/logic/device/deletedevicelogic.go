package device

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteDeviceLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewDeleteDeviceLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *DeleteDeviceLogic {
	return &DeleteDeviceLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *DeleteDeviceLogic) DeleteDevice(req *types.DeleteDeviceReq) (resp *types.DeleteDeviceResp, err error) {
	_, err = l.svcCtx.DeviceRpc.DelDevice(l.ctx, &iotpb.DelDeviceReq{
		IsDel: true,
		Ids:   req.Ids,
	})
	if err != nil {
		return nil, err
	}

	return
}
