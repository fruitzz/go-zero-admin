package device

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type ListDeviceProductLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewListDeviceProductLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *ListDeviceProductLogic {
	return &ListDeviceProductLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *ListDeviceProductLogic) ListDeviceProduct(req *types.ListDeviceProductReq) (resp *types.ListDeviceProductResp, err error) {
	productListResp, err := l.svcCtx.ProductRpc.GetProductList(l.ctx, &iotpb.GetProductListReq{
		Name:            req.Name,
		IsDataScope:     true,
		DataScopeUserId: ctxdata.GetUidFromCtx(l.ctx),
	})
	if err != nil {
		return nil, err
	}

	// 处理数据
	productList := make([]*types.IotProduct, 0)
	for _, v := range productListResp.GetList() {
		var product types.IotProduct
		_ = copier.Copy(&product, v)
		productList = append(productList, &product)
	}

	return &types.ListDeviceProductResp{
		List: productList,
	}, nil
}
