package device

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetDeviceStateCensusLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewGetDeviceStateCensusLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *GetDeviceStateCensusLogic {
	return &GetDeviceStateCensusLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *GetDeviceStateCensusLogic) GetDeviceStateCensus(req *types.GetDeviceStateCensusReq) (resp *types.GetDeviceStateCensusResp, err error) {
	censusResp, err := l.svcCtx.DeviceRpc.GetDeviceStateCensus(l.ctx, &iotpb.GetDeviceStateCensusReq{
		IsDataScope:     true,
		DataScopeUserId: ctxdata.GetUidFromCtx(l.ctx),
	})
	if err != nil {
		return nil, err
	}

	return &types.GetDeviceStateCensusResp{
		Devices:       censusResp.GetDevices(),
		DeviceOnline:  censusResp.GetDeviceOnline(),
		DeviceOffline: censusResp.GetDeviceOffline(),
	}, nil
}
