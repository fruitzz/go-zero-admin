package device

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateDeviceStateLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateDeviceStateLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateDeviceStateLogic {
	return &UpdateDeviceStateLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateDeviceStateLogic) UpdateDeviceState(req *types.UpdateDeviceStateReq) (resp *types.UpdateDeviceStateResp, err error) {
	_, err = l.svcCtx.DeviceRpc.UpDeviceState(l.ctx, &iotpb.UpDeviceStateReq{
		Id:         req.Id,
		State:      req.State,
		UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
	})
	if err != nil {
		return nil, err
	}

	return
}
