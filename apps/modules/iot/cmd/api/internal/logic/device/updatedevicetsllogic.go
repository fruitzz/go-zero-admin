package device

import (
	"context"
	"net/http"

	"go-zero-admin/apps/modules/iot/cmd/api/internal/svc"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/types"
	"go-zero-admin/apps/modules/iot/cmd/rpc/iotpb"
	"go-zero-admin/pkg/ctxdata"

	"github.com/duke-git/lancet/v2/convertor"
	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateDeviceTslLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
	r      *http.Request
}

func NewUpdateDeviceTslLogic(ctx context.Context, svcCtx *svc.ServiceContext, r *http.Request) *UpdateDeviceTslLogic {
	return &UpdateDeviceTslLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
		r:      r,
	}
}

func (l *UpdateDeviceTslLogic) UpdateDeviceTsl(req *types.UpdateDeviceTslReq) (resp *types.UpdateDeviceTslResp, err error) {
	_, err = l.svcCtx.DeviceRpc.UpDeviceTsl(l.ctx, &iotpb.UpDeviceTslReq{
		Id:         req.Id,
		Metadata:   req.Metadata,
		UpdateUser: convertor.ToString(ctxdata.GetUidFromCtx(l.ctx)),
	})
	if err != nil {
		return nil, err
	}

	return
}
