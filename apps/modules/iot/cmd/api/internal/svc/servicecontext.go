package svc

import (
	"go-zero-admin/apps/modules/iot/cmd/api/internal/config"
	"go-zero-admin/apps/modules/iot/cmd/api/internal/middleware"
	deviceClient "go-zero-admin/apps/modules/iot/cmd/rpc/client/device"
	networkClient "go-zero-admin/apps/modules/iot/cmd/rpc/client/network"
	productClient "go-zero-admin/apps/modules/iot/cmd/rpc/client/product"
	userScopeClient "go-zero-admin/apps/system/cmd/rpc/client/userscope"

	"github.com/zeromicro/go-zero/rest"
	"github.com/zeromicro/go-zero/zrpc"
)

type ServiceContext struct {
	Config config.Config
	// middle
	CheckUrlAuth rest.Middleware
	// rpc
	NetworkRpc networkClient.NetWork
	ProductRpc productClient.Product
	DeviceRpc  deviceClient.Device
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config: c,
		// middle
		CheckUrlAuth: middleware.NewCheckUrlAuthMiddleware(userScopeClient.NewUserScope(zrpc.MustNewClient(c.SystemRpc))).Handle,
		// rpc
		NetworkRpc: networkClient.NewNetWork(zrpc.MustNewClient(c.IotRpc)),
		ProductRpc: productClient.NewProduct(zrpc.MustNewClient(c.IotRpc)),
		DeviceRpc:  deviceClient.NewDevice(zrpc.MustNewClient(c.IotRpc)),
	}
}
