package rpcserver

import (
	"context"

	"go-zero-admin/pkg/xerr"

	"github.com/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// LoggerInterceptor rpc apps logger interceptor
func LoggerInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	resp, err = handler(ctx, req)
	if err != nil {
		// err type
		causeErr := errors.Cause(err)
		// custom error type
		if e, ok := causeErr.(*xerr.CodeError); ok {
			logx.WithContext(ctx).Errorf("【RPC-SRV-FUNC】%s,【RPC-SRV-ERR】%+v", info.FullMethod, err)
			// transform grpc err
			err = status.Error(codes.Code(e.GetErrCode()), e.GetErrMsg())
		} else {
			logx.WithContext(ctx).Errorf("【RPC-SRV-FUNC】%s,【RPC-SRV-ERR】%+v", info.FullMethod, err)
		}
	}

	return resp, err
}
