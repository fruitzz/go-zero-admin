package xerr

// OK 成功返回
const OK uint32 = 0

// 全局错误码
// 前3位代表业务, 后三位代表具体功能
const (
	ServerCommonError         uint32 = 100001 //服务器开小差
	RequestParamError         uint32 = 100002 //请求参数错误
	TokenExpireError          uint32 = 100003 //token过期
	TokenGenerateError        uint32 = 100004 //生成token失败
	DbError                   uint32 = 100005 //数据库繁忙,请稍后再试
	DbUpdateAffectedZeroError uint32 = 100006 //更新数据影响行数为0
	DataNoExistError          uint32 = 100007 //数据不存在
)
