package constant

// soft delete
const (
	DelStateNo  int64 = 0 // not deleted
	DelStateYes int64 = 1 // deleted
)

// PageSize page
const (
	PageSize int64 = 10 // page size
)
