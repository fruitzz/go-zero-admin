package constant

// 设备
const (
	DeviceStateOn  int64 = 1 // 设备状态开启
	DeviceStateOff int64 = 0 // 设备状态关闭
)
