package constant

// 数据权限
const (
	DataScopeAll        int64 = 1 // 全部权限
	DataScopeCustomize  int64 = 2 // 自定数据权限
	DataScopeDept       int64 = 3 // 本部门数据权限
	DataScopeDeptAndSon int64 = 4 // 本部门及以下数据权限
	DataScopeOwn        int64 = 5 // 仅本人数据权限
)

// 部门
const (
	DeptStatusOn  int64 = 1 // 部门状态开启
	DeptStatusOff int64 = 0 // 部门状态关闭
	DeptFirstPid  int64 = 0 // 顶级部门ID
)

// 字典数据
const (
	DictDataStatusOn  int64 = 1 // 字典数据状态开启
	DictDataStatusOff int64 = 0 // 字典数据状态关闭
)

// 登录日志
const (
	LoginLogStatusSuc  int64  = 1 // 登录日志状态成功
	LoginLogStatusFail int64  = 0 // 登录日志状态失败
	LoginLogModuleSys  string = "系统后台"
	LoginLogMsg        string = "登录成功"
)

// 菜单
const (
	MenuTypeList int64 = 0 // 目录
	MenuTypeMenu int64 = 1 // 菜单
	MenuTypeBtn  int64 = 2 // 按钮
	MenuFirstPid int64 = 0 // 顶级菜单ID
)

// 角色
const (
	RoleStatusOn  int64 = 1 // 角色状态开启
	RoleStatusOff int64 = 0 // 角色状态关闭
)

// 岗位
const (
	PostStatusOn  int64 = 1 // 岗位状态开启
	PostStatusOff int64 = 0 // 岗位状态关闭
)

// 用户
const (
	UserStatusDis    int64 = 0 // 禁用
	UserStatusEn     int64 = 1 // 正常
	UserStatusNoVail int64 = 2 // 未验证
)
