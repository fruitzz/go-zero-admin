package tool

import (
	"bytes"
	"encoding/json"

	"github.com/klauspost/compress/gzip"
)

// MarshalToJsonWithGzip json数据压缩
func MarshalToJsonWithGzip(jsonData any) []byte {
	dataAfterMarshal, _ := json.Marshal(jsonData)
	dataAfterGzip, err := Encode(dataAfterMarshal)
	if err != nil {
		return nil
	}
	return dataAfterGzip
}

// UnmarshalDataFromJsonWithGzip json数据解压
func UnmarshalDataFromJsonWithGzip(msg []byte, data any) (any, error) {
	dataAfterDecode, err := Decode(msg)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(dataAfterDecode, data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// Encode 压缩
func Encode(input []byte) ([]byte, error) {
	// 创建一个新的 byte 输出流
	var buf bytes.Buffer
	// 创建一个新的 gzip 输出流
	gzipWriter := gzip.NewWriter(&buf)
	// 将 input byte 数组写入到此输出流中
	_, err := gzipWriter.Write(input)
	if err != nil {
		_ = gzipWriter.Close()
		return nil, err
	}
	if err = gzipWriter.Close(); err != nil {
		return nil, err
	}
	// 返回压缩后的 bytes 数组
	return buf.Bytes(), nil
}

// Decode 解压
func Decode(input []byte) ([]byte, error) {
	// 创建一个新的 gzip.Reader
	bytesReader := bytes.NewReader(input)
	gzipReader, err := gzip.NewReader(bytesReader)
	if err != nil {
		return nil, err
	}
	defer func() {
		// defer 中关闭 gzipReader
		_ = gzipReader.Close()
	}()
	buf := new(bytes.Buffer)
	// 从 Reader 中读取出数据
	if _, err = buf.ReadFrom(gzipReader); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
