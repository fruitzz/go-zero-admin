package tool

// GetErrMsgFormat 错误信息日志格式
func GetErrMsgFormat(msg string) string {
	return "Failed to " + msg + "; Err: %+v; In: %+v;"
}

// GetErrMsgHeaderFormat 错误信息日志格式
func GetErrMsgHeaderFormat(hearer, msg string) string {
	return "【" + hearer + "】 Failed to " + msg + "; Err: %+v; In: %+v;"
}
