package tool

import (
	"github.com/duke-git/lancet/v2/cryptor"
)

// EncryptPassword 密码加密
func EncryptPassword(password, salt string) string {
	return cryptor.Md5String(cryptor.Md5String(password) + cryptor.Md5String(salt))
}
