import request from '/@/utils/request';

export function getMenuParams() {
	return request({
		url: '/api/v1/system/menu/getParams',
		method: 'get',
	});
}

export function getMenuList(query: Object) {
	return request({
		url: '/api/v1/system/menu/list',
		method: 'get',
		params: query,
	});
}

export function getMenu(id: number) {
	return request({
		url: '/api/v1/system/menu/get',
		method: 'get',
		params: { id },
	});
}

export function addMenu(data: Object) {
	return request({
		url: '/api/v1/system/menu/add',
		method: 'post',
		data: data,
	});
}

export function editMenu(data: Object) {
	return request({
		url: '/api/v1/system/menu/edit',
		method: 'put',
		data: data,
	});
}

export function delMenu(menuId: number) {
	return request({
		url: '/api/v1/system/menu/delete',
		method: 'delete',
		data: { id: menuId },
	});
}
