import request from '/@/utils/request';
import { ref, toRefs, ToRefs } from 'vue';

/**
 * 获取字典数据
 */
export function useDict(...args: string[]): ToRefs<any> {
	const res: any = ref({});
	args.forEach((d: string) => {
		res.value[d] = [];
		request({
			url: '/api/v1/system/dict/data/getDictDataList',
			method: 'get',
			params: { dictType: d },
		}).then((resp) => {
			res.value[d] = resp.data.list.map((p: any) => ({
				label: p.dictLabel,
				value: p.dictValue,
				isDefault: p.isDefault,
			}));
		});
	});
	return toRefs(res.value);
}

export function getDataList(query: Object) {
	return request({
		url: '/api/v1/system/dict/data/list',
		method: 'get',
		params: query,
	});
}

export function addData(data: any) {
	return request({
		url: '/api/v1/system/dict/data/add',
		method: 'post',
		data: data,
	});
}

export function editData(data: any) {
	return request({
		url: '/api/v1/system/dict/data/edit',
		method: 'put',
		data: data,
	});
}

export function deleteData(ids: number[]) {
	return request({
		url: '/api/v1/system/dict/data/delete',
		method: 'delete',
		data: { ids },
	});
}
