import request from '/@/utils/request';

export function getUserMenus() {
	return request({
		url: '/api/v1/system/user/getUserMenus',
		method: 'get',
	});
}

export function getUserParams() {
	return request({
		url: '/api/v1/system/user/getParams',
		method: 'get',
	});
}

export function changeUserStatus(id: number, userStatus: number) {
	return request({
		url: '/api/v1/system/user/setStatus',
		method: 'put',
		data: { id, userStatus },
	});
}

export function resetUserPwd(id: number, userPassword: string) {
	return request({
		url: '/api/v1/system/user/resetPwd',
		method: 'put',
		data: { id, userPassword },
	});
}

export function getUserData(id: number) {
	return request({
		url: '/api/v1/system/user/getData',
		method: 'get',
		params: { id },
	});
}

export function getUserList(query: Object) {
	return request({
		url: '/api/v1/system/user/list',
		method: 'get',
		params: query,
	});
}

export function addUser(data: object) {
	return request({
		url: '/api/v1/system/user/add',
		method: 'post',
		data: data,
	});
}

export function editUser(data: object) {
	return request({
		url: '/api/v1/system/user/edit',
		method: 'put',
		data: data,
	});
}

export function deleteUser(ids: number[]) {
	return request({
		url: '/api/v1/system/user/delete',
		method: 'delete',
		data: { ids },
	});
}
