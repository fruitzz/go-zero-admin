import request from '/@/utils/request';

export function editProductTsl(id: number, metadata: string) {
	return request({
		url: '/api/v1/iot/product/edit/tsl',
		method: 'put',
		data: { id: id, metadata: metadata },
	});
}

export function editProductState(id: number, state: number) {
	return request({
		url: '/api/v1/iot/product/edit/state',
		method: 'put',
		data: { id: id, state: state },
	});
}

export function getProduct(id: number) {
	return request({
		url: '/api/v1/iot/product/get',
		method: 'get',
		params: { id },
	});
}

export function getProductList(query: Object) {
	return request({
		url: '/api/v1/iot/product/list',
		method: 'get',
		params: query,
	});
}

export function addProduct(data: any) {
	return request({
		url: '/api/v1/iot/product/add',
		method: 'post',
		data: data,
	});
}

export function editProduct(data: any) {
	return request({
		url: '/api/v1/iot/product/edit',
		method: 'put',
		data: data,
	});
}

export function deleteProduct(ids: number[]) {
	return request({
		url: '/api/v1/iot/product/delete',
		method: 'delete',
		data: { ids },
	});
}
