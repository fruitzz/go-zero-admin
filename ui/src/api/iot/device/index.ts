import request from '/@/utils/request';

export function getDeviceCensus() {
	return request({
		url: '/api/v1/iot/device/census/state',
		method: 'get',
	});
}

export function getDeviceProductList(query: Object) {
	return request({
		url: '/api/v1/iot/device/product/list',
		method: 'get',
		params: query,
	});
}

export function editDeviceTsl(id: number, metadata: string) {
	return request({
		url: '/api/v1/iot/device/edit/tsl',
		method: 'put',
		data: { id: id, metadata: metadata },
	});
}

export function editDeviceState(id: number, state: number) {
	return request({
		url: '/api/v1/iot/device/edit/state',
		method: 'put',
		data: { id: id, state: state },
	});
}

export function getDevice(id: number) {
	return request({
		url: '/api/v1/iot/device/get',
		method: 'get',
		params: { id },
	});
}

export function getDeviceList(query: Object) {
	return request({
		url: '/api/v1/iot/device/list',
		method: 'get',
		params: query,
	});
}

export function addDevice(data: any) {
	return request({
		url: '/api/v1/iot/device/add',
		method: 'post',
		data: data,
	});
}

export function editDevice(data: any) {
	return request({
		url: '/api/v1/iot/device/edit',
		method: 'put',
		data: data,
	});
}

export function deleteDevice(ids: number[]) {
	return request({
		url: '/api/v1/iot/device/delete',
		method: 'delete',
		data: { ids },
	});
}
