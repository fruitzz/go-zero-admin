import request from '/@/utils/request';

export function getNetWorkTypeList() {
	return request({
		url: '/api/v1/iot/network/types',
		method: 'get',
	});
}

export function editNetWorkStates(id: number, state: number) {
	return request({
		url: '/api/v1/iot/network/edit/state',
		method: 'put',
		data: { id: id, state: state },
	});
}

export function getNetWork(id: number) {
	return request({
		url: '/api/v1/iot/network/get',
		method: 'get',
		params: { id },
	});
}

export function getNetWorkList(query: Object) {
	return request({
		url: '/api/v1/iot/network/list',
		method: 'get',
		params: query,
	});
}

export function addNetWork(data: any) {
	return request({
		url: '/api/v1/iot/network/add',
		method: 'post',
		data: data,
	});
}

export function editNetWork(data: any) {
	return request({
		url: '/api/v1/iot/network/edit',
		method: 'put',
		data: data,
	});
}

export function deleteNetWork(ids: number[]) {
	return request({
		url: '/api/v1/iot/network/delete',
		method: 'delete',
		data: { ids },
	});
}
