import { defineStore } from 'pinia';
import { Session } from '/@/utils/storage';

/**
 * 用户信息
 * @methods setUserInfos 设置用户信息
 */
export const useUserInfo = defineStore('userInfo', {
	state: (): UserInfosState => ({
		userInfos: {
			userName: '',
			photo: '',
			time: 0,
			roles: [],
			authBtnList: [],
			// 自定义添加
			id: 0,
			userNickname: '',
		},
	}),
	actions: {
		async setUserInfos() {
			// 存储用户信息到浏览器缓存
			if (Session.get('userInfo')) {
				this.userInfos = Session.get('userInfo');
			}
		},

		async setPermissions() {
			// 存储权限按钮数据到浏览器缓存
			if (Session.get('permissions')) {
				this.userInfos.authBtnList = Session.get('permissions');
			}
		},
	},
});
