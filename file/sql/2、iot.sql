-- Create syntax for TABLE 'iot_network'
CREATE TABLE `iot_network`
(
    `id`            bigint                                                         NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `dept_id`       bigint                                                         NOT NULL DEFAULT '0' COMMENT '部门ID',
    `name`          varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '名称',
    `configuration` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '配置',
    `state`         tinyint                                                        NOT NULL DEFAULT '0' COMMENT '状态（1启动 0停止）',
    `type`          varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '类型',
    `description`   varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci  NOT NULL DEFAULT '' COMMENT '描述',
    `create_user`   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '创建人',
    `update_user`   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '更新人',
    `create_time`   timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `del_state`     tinyint                                                        NOT NULL DEFAULT '0' COMMENT '删除状态',
    PRIMARY KEY (`id`),
    KEY `idx_id_deptId` (`dept_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='网络组件表';

-- Create syntax for TABLE 'iot_gateway'
CREATE TABLE `iot_gateway`
(
    `id`            bigint                                                         NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `dept_id`       bigint                                                         NOT NULL DEFAULT '0' COMMENT '部门ID',
    `channel_id`    bigint                                                         NOT NULL DEFAULT '0' COMMENT '接入通道ID',
    `protocol_id`   bigint                                                         NOT NULL DEFAULT '0' COMMENT '消息协议ID',
    `name`          varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '名称',
    `channel`       varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '接入通道方式',
    `transport`     varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '传输协议',
    `provider`      varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '接入方式',
    `configuration` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '配置',
    `state`         int                                                            NOT NULL DEFAULT '0' COMMENT '状态（1启动 0停止）',
    `description`   varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci  NOT NULL DEFAULT '' COMMENT '描述',
    `create_user`   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '创建人',
    `update_user`   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '更新人',
    `create_time`   timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `del_state`     tinyint                                                        NOT NULL DEFAULT '0' COMMENT '删除状态',
    PRIMARY KEY (`id`),
    KEY `idx_id_deptId` (`dept_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='设备网关表';

-- Create syntax for TABLE 'iot_product'
CREATE TABLE `iot_product`
(
    `id`                 bigint                                                         NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `dept_id`            bigint                                                         NOT NULL DEFAULT '0' COMMENT '部门ID',
    `name`               varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '名称',
    `photo`              varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci  NOT NULL DEFAULT '' COMMENT '图片地址',
    `metadata`           varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '物模型',
    `transport_protocol` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '传输协议',
    `message_protocol`   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '消息协议',
    `store_policy`       varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '存储策略',
    `device_type`        varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '设备类型',
    `describe`           varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci  NOT NULL DEFAULT '' COMMENT '描述',
    `state`              tinyint                                                        NOT NULL DEFAULT '0' COMMENT '状态',
    `create_user`        varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '创建人',
    `update_user`        varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '更新人',
    `create_time`        timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`        timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `del_state`          tinyint                                                        NOT NULL DEFAULT '0' COMMENT '删除状态',
    PRIMARY KEY (`id`),
    KEY `idx_id_deptId` (`dept_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  ROW_FORMAT = DYNAMIC COMMENT ='设备产品表';

-- Create syntax for TABLE 'iot_device'
CREATE TABLE `iot_device`
(
    `id`          bigint                                                         NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `dept_id`     bigint                                                         NOT NULL DEFAULT '0' COMMENT '部门ID',
    `product_id`  bigint                                                         NOT NULL DEFAULT '0' COMMENT '产品id',
    `name`        varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '名称',
    `metadata`    varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '物模型',
    `describe`    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci  NOT NULL DEFAULT '' COMMENT '描述',
    `state`       tinyint                                                        NOT NULL DEFAULT '0' COMMENT '状态',
    `create_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '创建人',
    `update_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci   NOT NULL DEFAULT '' COMMENT '更新人',
    `create_time` timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` timestamp                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `del_state`   tinyint                                                        NOT NULL DEFAULT '0' COMMENT '删除状态',
    PRIMARY KEY (`id`),
    KEY `idx_id_deptId` (`dept_id`) USING BTREE,
    KEY `idx_id_productId` (`product_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci COMMENT ='设备实例表';