INSERT INTO `sys_menu` (`id`, `pid`, `name`, `title`, `icon`, `condition`, `remark`, `menu_type`, `menu_sort`,
                        `is_hide`, `path`, `component`, `is_link`, `is_iframe`, `is_keep_alive`, `redirect`, `is_affix`,
                        `link_url`, `create_user`, `update_user`, `create_time`, `update_time`, `del_state`)
VALUES (67, 0, 'api/v1/iot', '物联网管理', 'ele-Cpu', '', '', 0, 2, 0, '/iot', 'iot', 0, 0, 1, '', 0, '', '1', '1',
        '2022-10-18 15:25:27', '2023-01-09 17:01:48', 0),
       (68, 67, 'api/v1/iot/map', '地理位置', 'iconfont icon-ditu', '', '', 1, 1, 0, '/iot/map', 'iot/location/index',
        0, 0, 1, '', 0, '', '1', '1', '2022-10-18 15:33:26', '2022-10-18 15:36:02', 0),
       (69, 71, 'api/v1/iot/network/list', '网络组件', 'iconfont icon-zujian', '', '', 1, 1, 0, '/iot/access/network',
        'iot/network/index', 0, 0, 1, '', 0, '', '1', '1', '2022-10-18 15:40:53', '2023-01-14 14:21:26', 0),
       (70, 71, 'api/v1/iot/gateway/list', '设备网关', 'iconfont icon-quanxian', '', '', 1, 2, 0, '/iot/access/gateway',
        'iot/gateway/index', 0, 0, 1, '', 0, '', '1', '1', '2022-10-18 15:43:36', '2023-01-14 14:21:32', 0),
       (71, 67, 'api/v1/iot/access', '设备接入', 'iconfont icon-siweidaotu', '', '', 1, 2, 0, '/iot/access',
        'iot/access', 0, 0, 1, '', 0, '', '1', '1', '2022-11-16 16:03:43', '2023-01-14 14:21:19', 0),
       (72, 67, 'api/v1/iot/device', '设备管理', 'ele-Connection', '', '', 1, 3, 0, '/iot/device', 'iot/device', 0, 0,
        1, '', 0, '', '1', '1', '2023-01-14 14:15:21', '2023-01-14 14:35:55', 0),
       (73, 72, 'api/v1/iot/product/list', '产品管理', 'iconfont icon-fuwenbenkuang', '', '', 1, 1, 0,
        '/iot/device/product', 'iot/product/index', 0, 0, 1, '', 0, '', '1', '1', '2023-01-14 14:36:28',
        '2023-01-14 14:45:28', 0),
       (74, 72, 'api/v1/iot/device/list', '设备管理', 'fa fa-steam', '', '', 1, 2, 0, '/iot/device/equipment',
        'iot/device/index', 0, 0, 1, '', 0, '', '1', '1', '2023-01-14 14:37:50', '2023-01-14 14:46:49', 0);