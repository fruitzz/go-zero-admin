# 开发环境
# goctl安装 俩种方式
go install github.com/zeromicro/go-zero/tools/goctl@latest
brew install goctl
# protobuf protoc-gen-go protoc-gen-go-grpc 安装 俩种方式
goctl env check -i -f --verbose
brew install protobuf protoc-gen-go protoc-gen-go-grpc


# proto导入生成pb文件
protoc model.proto --go_out=.
# 生成多service模块
cd apps/xxx/rpc
goctl rpc protoc xxx.proto --go_out=./ --go-grpc_out=./ --zrpc_out=./ --multiple=true


# grpc测试
# 安装
go install github.com/fullstorydev/grpcurl/cmd/grpcurl@latest
go install github.com/fullstorydev/grpcui/cmd/grpcui@latest
brew install grpcurl grpcui
# 启动
grpcui -plaintext 127.0.0.1:9000